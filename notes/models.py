from django.db import models

# Create your models here.


class Notes(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    text = models.TextField()


class SPYNotes(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    text = models.TextField()


class QQQNotes(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    text = models.TextField()


class IWMNotes(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    text = models.TextField()


class MSFTNotes(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
