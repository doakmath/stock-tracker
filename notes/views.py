from django.shortcuts import render
from .models import Notes, QQQNotes, IWMNotes, SPYNotes, MSFTNotes
from .common.model_encoder import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse

# Create your views here.


# _________________________________MODEL ENCODERS________________________

class NotesEncoder(ModelEncoder):
    model = Notes
    properties = [
        "id",
        "timestamp",
        "text"
    ]

class QQQNotesEncoder(ModelEncoder):
    model = QQQNotes
    properties = [
        "id",
        "timestamp",
        "text"
    ]

class SPYNotesEncoder(ModelEncoder):
    model = SPYNotes
    properties = [
        "id",
        "timestamp",
        "text"
    ]

class IWMNotesEncoder(ModelEncoder):
    model = IWMNotes
    properties = [
        "id",
        "timestamp",
        "text"
    ]

class MSFTNotesEncoder(ModelEncoder):
    model = MSFTNotes
    properties = [
        "id",
        "timestamp",
        "text"
    ]


# _______________________________NOTES______________________________


@require_http_methods(["POST"])
def create_note(request):
    try:
        content = json.loads(request.body)
        print("CONTENT", content)
        if Notes.objects.filter(text=content['text']).exists():
            print("ITEMS ALREADY EXIST BASED ON TEXT INPUT")
            return JsonResponse({"message": "Already exists"})
        note = Notes.objects.create(**content)
        return JsonResponse(
            {"note": note},
            encoder=NotesEncoder
            )
    except Exception as e:
        print("Error:", str(e))
        return JsonResponse({'status': 'error'})


@require_http_methods(["GET"])
def get_all_notes(request):
    if request.method == "GET":
        note = Notes.objects.all()
        return JsonResponse(
            {"note": note},
            encoder=NotesEncoder
        )


@require_http_methods(["DELETE"])
def delete_note(request, pk):
    if request.method == "DELETE":
        note = Notes.objects.get(id=pk)
        note.delete()
        return JsonResponse(
            note,
            encoder=NotesEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def delete_all_notes(request):
    if request.method == "DELETE":
        note = Notes.objects.all()
        note.delete()
        return JsonResponse(
            note,
            encoder=NotesEncoder,
            safe=False
        )



# ____________________________________________QQQ______________________________________


@require_http_methods(["POST"])
def create_QQQnote(request):
    try:
        content = json.loads(request.body)
        print("CONTENT", content)
        if QQQNotes.objects.filter(text=content['text']).exists():
            print("ITEMS ALREADY EXIST BASED ON TEXT INPUT")
            return JsonResponse({"message": "Already exists"})
        qqq = QQQNotes.objects.create(**content)
        return JsonResponse(
            {"QQQ": qqq},
            encoder=QQQNotesEncoder
            )
    except Exception as e:
        print("Error:", str(e))
        return JsonResponse({'status': 'error'})


@require_http_methods(["GET"])
def get_all_QQQnotes(request):
    if request.method == "GET":
        qqq = QQQNotes.objects.all()
        return JsonResponse(
            {"QQQ": qqq},
            encoder=QQQNotesEncoder
        )


@require_http_methods(["DELETE"])
def delete_QQQnote(request, pk):
    if request.method == "DELETE":
        qqq = QQQNotes.objects.get(id=pk)
        qqq.delete()
        return JsonResponse(
            qqq,
            encoder=QQQNotesEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def delete_all_QQQnotes(request):
    if request.method == "DELETE":
        qqq = QQQNotes.objects.all()
        qqq.delete()
        return JsonResponse(
            qqq,
            encoder=QQQNotesEncoder,
            safe=False
        )



# ___________________________________SPY________________________________________


@require_http_methods(["POST"])
def create_SPYnote(request):
    try:
        content = json.loads(request.body)
        print("CONTENT", content)
        if SPYNotes.objects.filter(text=content['text']).exists():
            print("ITEMS ALREADY EXIST BASED ON ID")
            return JsonResponse({"message": "Already exists"})
        spy = SPYNotes.objects.create(**content)
        return JsonResponse(
            {"SPY": spy},
            encoder=SPYNotesEncoder
            )
    except Exception as e:
        print("Error:", str(e))
        return JsonResponse({'status': 'error'})


@require_http_methods(["GET"])
def get_all_SPYnotes(request):
    if request.method == "GET":
        spy = SPYNotes.objects.all()
        return JsonResponse(
            {"SPY": spy},
            encoder=SPYNotesEncoder
        )


@require_http_methods(["DELETE"])
def delete_SPYnote(request, pk):
    if request.method == "DELETE":
        spy = SPYNotes.objects.get(id=pk)
        spy.delete()
        return JsonResponse(
            spy,
            encoder=SPYNotesEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def delete_all_SPYnotes(request):
    if request.method == "DELETE":
        spy = SPYNotes.objects.all()
        spy.delete()
        return JsonResponse(
            spy,
            encoder=SPYNotesEncoder,
            safe=False
        )



    # __________________________________IWM________________________________________


@require_http_methods(["POST"])
def create_IWMnote(request):
    try:
        content = json.loads(request.body)
        print("CONTENT", content)
        if IWMNotes.objects.filter(text=content['text']).exists():
            print("ITEMS ALREADY EXIST BASED ON ID")
            return JsonResponse({"message": "Already exists"})
        iwm = IWMNotes.objects.create(**content)
        return JsonResponse(
            {"IWM": iwm},
            encoder=IWMNotesEncoder
            )
    except Exception as e:
        print("Error:", str(e))
        return JsonResponse({'status': 'error'})


@require_http_methods(["GET"])
def get_all_IWMnotes(request):
    if request.method == "GET":
        iwm = IWMNotes.objects.all()
        return JsonResponse(
            {"IWM": iwm},
            encoder=IWMNotesEncoder
        )


@require_http_methods(["DELETE"])
def delete_IWMnote(request, pk):
    if request.method == "DELETE":
        iwm = IWMNotes.objects.get(id=pk)
        iwm.delete()
        return JsonResponse(
            iwm,
            encoder=IWMNotesEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def delete_all_IWMnotes(request):
    if request.method == "DELETE":
        iwm = IWMNotes.objects.all()
        iwm.delete()
        return JsonResponse(
            iwm,
            encoder=IWMNotesEncoder,
            safe=False
        )



    # __________________________________MSFT__________________________________________


@require_http_methods(["POST"])
def create_MSFTnote(request):
    try:
        content = json.loads(request.body)
        print("CONTENT", content)
        if MSFTNotes.objects.filter(text=content['text']).exists():
            print("ITEMS ALREADY EXIST BASED ON ID")
            return JsonResponse({"message": "Already exists"})
        msft = MSFTNotes.objects.create(**content)
        return JsonResponse(
            {"MSFT": msft},
            encoder=MSFTNotesEncoder
            )
    except Exception as e:
        print("Error:", str(e))
        return JsonResponse({'status': 'error'})


@require_http_methods(["GET"])
def get_all_MSFTnotes(request):
    if request.method == "GET":
        msft = MSFTNotes.objects.all()
        return JsonResponse(
            {"MSFT": msft},
            encoder=MSFTNotesEncoder
        )


@require_http_methods(["DELETE"])
def delete_MSFTnote(request, pk):
    if request.method == "DELETE":
        msft = MSFTNotes.objects.get(id=pk)
        msft.delete()
        return JsonResponse(
            msft,
            encoder=MSFTNotesEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def delete_all_MSFTnotes(request):
    if request.method == "DELETE":
        msft = MSFTNotes.objects.all()
        msft.delete()
        return JsonResponse(
            msft,
            encoder=MSFTNotesEncoder,
            safe=False
        )
