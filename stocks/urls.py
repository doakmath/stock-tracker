from django.urls import path
from .views import (
    create_tsi,
    get_all_tsi,
    delete_tsi,
    delete_all_tsi,
    create_SPY,
    get_all_SPY,
    delete_SPY,
    delete_all_SPY,
    create_QQQ,
    get_all_QQQ,
    delete_QQQ,
    delete_all_QQQ,
    create_IWM,
    get_all_IWM,
    delete_IWM,
    delete_all_IWM
)

urlpatterns = [
    path("createTsi/", create_tsi, name="create_tsi"),
    path("getTsi/", get_all_tsi, name="get_all_tsi"),
    path("deleteTsi/<int:pk>", delete_tsi, name="delete_tsi"),
    path("deleteTsi/", delete_all_tsi, name="delete_all_tsi"),
    path("createSPY/", create_SPY, name="create_SPY"),
    path("getSPY/", get_all_SPY, name="get_all_SPY"),
    path("deleteSPY/<int:pk>", delete_SPY, name="delete_SPY"),
    path("deleteAllSPY/", delete_all_SPY, name="delete_all_SPY"),
    path("createQQQ/", create_QQQ, name="create_QQQ"),
    path("getQQQ/", get_all_QQQ, name="get_all_QQQ"),
    path("deleteQQQ/<int:pk>", delete_QQQ, name="delete_QQQ"),
    path("deleteAllQQQ/", delete_all_QQQ, name="delete_all_QQQ"),
    path("createIWM/", create_IWM, name="create_IWM"),
    path("getIWM/", get_all_IWM, name="get_all_IWM"),
    path("deleteIWM/<int:pk>", delete_IWM, name="delete_IWM"),
    path("deleteAllIWM/", delete_all_IWM, name="delete_all_IWM")
]
