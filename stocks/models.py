from django.db import models

# Create your models here.


class TSI(models.Model):
    timestamp = models.CharField(max_length=200, null=True)
    open = models.CharField(max_length=200, null=True)
    high = models.CharField(max_length=200, null=True)
    low = models.CharField(max_length=200, null=True)
    close = models.CharField(max_length=200, null=True)
    volume = models.CharField(max_length=200, null=True)


class QQQ(models.Model):
    timestamp = models.CharField(max_length=200, null=True)
    open = models.CharField(max_length=200, null=True)
    high = models.CharField(max_length=200, null=True)
    low = models.CharField(max_length=200, null=True)
    close = models.CharField(max_length=200, null=True)
    volume = models.CharField(max_length=200, null=True)


class SPY(models.Model):
    timestamp = models.CharField(max_length=200, null=True)
    open = models.CharField(max_length=200, null=True)
    high = models.CharField(max_length=200, null=True)
    low = models.CharField(max_length=200, null=True)
    close = models.CharField(max_length=200, null=True)
    volume = models.CharField(max_length=200, null=True)


class IWM(models.Model):
    timestamp = models.CharField(max_length=200, null=True)
    open = models.CharField(max_length=200, null=True)
    high = models.CharField(max_length=200, null=True)
    low = models.CharField(max_length=200, null=True)
    close = models.CharField(max_length=200, null=True)
    volume = models.CharField(max_length=200, null=True)

