from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from .models import TSI, SPY, QQQ, IWM
from .common.modelencoder import ModelEncoder
from django.core import serializers


# Create your views here.



# ----------------------------STOCK ENCODERS---------------------------

class TSIEncoder(ModelEncoder):
    model = TSI
    properties = [
        "id",
        "timestamp",
        "open",
        "high",
        "low",
        "close",
        "volume"
    ]

class SPYEncoder(ModelEncoder):
    model = SPY
    properties = [
        "id",
        "timestamp",
        "open",
        "high",
        "low",
        "close",
        "volume"
    ]


class QQQEncoder(ModelEncoder):
    model = QQQ
    properties = [
        "id",
        "timestamp",
        "open",
        "high",
        "low",
        "close",
        "volume"
    ]

class IWMEncoder(ModelEncoder):
    model = IWM
    properties = [
        "id",
        "timestamp",
        "open",
        "high",
        "low",
        "close",
        "volume"
    ]


# -------------------------------------------------------MSFT(TSI)-------------------------


@require_http_methods(["POST"])
def create_tsi(request):
    try:
        content = json.loads(request.body)
        print("CONTENT", content)
        if TSI.objects.filter(timestamp=content['timestamp']).exists():
            print("ITEMS ALREADY EXIST BASED ON TIMESTAMP")
            return JsonResponse({"message": "Already exists"})
        tsi = TSI.objects.create(**content)
        return JsonResponse(
            {"tsi": tsi},
            encoder=TSIEncoder
            )
    except Exception as e:
        print("Error:", str(e))
        return JsonResponse({'status': 'error'})


@require_http_methods(["GET"])
def get_all_tsi(request):
    if request.method == "GET":
        tsi = TSI.objects.all()
        return JsonResponse(
            {"tsi": tsi},
            encoder=TSIEncoder
        )


@require_http_methods(["DELETE"])
def delete_tsi(request, pk):
    if request.method == "DELETE":
        tsi = TSI.objects.get(id=pk)
        tsi.delete()
        return JsonResponse(
            tsi,
            encoder=TSIEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def delete_all_tsi(request):
    if request.method == "DELETE":
        tsi = TSI.objects.all()
        tsi.delete()
        return JsonResponse(
            tsi,
            encoder=TSIEncoder,
            safe=False
        )


# -------------------------------------------SPY--------------------------------

@require_http_methods(["POST"])
def create_SPY(request):
    try:
        content = json.loads(request.body)
        print("CONTENT", content)
        if SPY.objects.filter(timestamp=content['timestamp']).exists():
            print("ITEMS ALREADY EXIST BASED ON TIMESTAMP")
            return JsonResponse({"message": "Already exists"})
        spy = SPY.objects.create(**content)
        return JsonResponse(
            {"SPY": spy},
            encoder=SPYEncoder
            )
    except Exception as e:
        print("Error:", str(e))
        return JsonResponse({'status': 'error'})


@require_http_methods(["GET"])
def get_all_SPY(request):
    if request.method == "GET":
        spy = SPY.objects.all()
        return JsonResponse(
            {"SPY": spy},
            encoder=SPYEncoder
        )


@require_http_methods(["DELETE"])
def delete_SPY(request, pk):
    if request.method == "DELETE":
        spy = SPY.objects.get(id=pk)
        spy.delete()
        return JsonResponse(
            spy,
            encoder=SPYEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def delete_all_SPY(request):
    if request.method == "DELETE":
        spy = SPY.objects.all()
        spy.delete()
        return JsonResponse(
            spy,
            encoder=SPYEncoder,
            safe=False
        )



    # ---------------------------------------------QQQ---------------------------------------


@require_http_methods(["POST"])
def create_QQQ(request):
    try:
        content = json.loads(request.body)
        print("CONTENT", content)
        if QQQ.objects.filter(timestamp=content['timestamp']).exists():
            print("ITEMS ALREADY EXIST BASED ON TIMESTAMP")
            return JsonResponse({"message": "Already exists"})
        qqq = QQQ.objects.create(**content)
        return JsonResponse(
            {"QQQ": qqq},
            encoder=QQQEncoder
            )
    except Exception as e:
        print("Error:", str(e))
        return JsonResponse({'status': 'error'})


@require_http_methods(["GET"])
def get_all_QQQ(request):
    if request.method == "GET":
        qqq = QQQ.objects.all()
        return JsonResponse(
            {"QQQ": qqq},
            encoder=QQQEncoder
        )


@require_http_methods(["DELETE"])
def delete_QQQ(request, pk):
    if request.method == "DELETE":
        qqq = QQQ.objects.get(id=pk)
        qqq.delete()
        return JsonResponse(
            qqq,
            encoder=QQQEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def delete_all_QQQ(request):
    if request.method == "DELETE":
        qqq = QQQ.objects.all()
        qqq.delete()
        return JsonResponse(
            qqq,
            encoder=QQQEncoder,
            safe=False
        )



    # -------------------------------------IWM---------------------------------------------


@require_http_methods(["POST"])
def create_IWM(request):
    try:
        content = json.loads(request.body)
        print("CONTENT", content)
        if IWM.objects.filter(timestamp=content['timestamp']).exists():
            print("ITEMS ALREADY EXIST BASED ON TIMESTAMP")
            return JsonResponse({"message": "Already exists"})
        iwm = IWM.objects.create(**content)
        return JsonResponse(
            {"IWM": iwm},
            encoder=IWMEncoder
            )
    except Exception as e:
        print("Error:", str(e))
        return JsonResponse({'status': 'error'})


@require_http_methods(["GET"])
def get_all_IWM(request):
    if request.method == "GET":
        iwm = IWM.objects.all()
        return JsonResponse(
            {"IWM": iwm},
            encoder=IWMEncoder
        )


@require_http_methods(["DELETE"])
def delete_IWM(request, pk):
    if request.method == "DELETE":
        iwm = IWM.objects.get(id=pk)
        iwm.delete()
        return JsonResponse(
            iwm,
            encoder=IWMEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def delete_all_IWM(request):
    if request.method == "DELETE":
        iwm = IWM.objects.all()
        iwm.delete()
        return JsonResponse(
            iwm,
            encoder=IWMEncoder,
            safe=False
        )
