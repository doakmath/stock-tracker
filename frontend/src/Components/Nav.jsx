import React from 'react';
import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-dark bg-dark fixed-top">
      <div className="container-fluid">
        {/* <NavLink className="navbar-brand" to="/">Welcome!</NavLink>
        <NavLink to="/circles">Color Game/Circles</NavLink>
        <NavLink to="/maslow">Maslow's Hierarchy of Needs</NavLink>
        <NavLink to="/pictures">PICTURES</NavLink>
        <NavLink to="/picturehouse">all the pictures</NavLink>
        <NavLink to="/dogs">DOGGIES!</NavLink>
        <NavLink to="/dogfavs">Favorite Doggies</NavLink>
        <NavLink to="/doghouse">DogHouse</NavLink>
        <NavLink to="/nasa">NASA Daily Pic</NavLink>
        <NavLink to="/nasahouse">NASA Previous Pics</NavLink>
        <NavLink to="/zen">MOMENT OF ZEN</NavLink>
        <NavLink to="/TSITable">TIme Series Intradaily Table</NavLink>
        <NavLink to="/TSIGraphs">TIme Series Intradaily Graphs</NavLink>
        <NavLink to="/TSIGraphInteractive">Interactive Graphs</NavLink>
        <NavLink to="TSIDynamicGraph">Dynamic Graph</NavLink>
        <NavLink to="/Calendar">Calendar</NavLink>
        <NavLink to="/CalendarReact">React-Calendar</NavLink> */}
        <button className="navbar-toggler ms-auto" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDarkNavbar" aria-controls="offcanvasDarkNavbar" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="offcanvas offcanvas-end bg-dark text-white" tabIndex="-1" id="offcanvasDarkNavbar" aria-labelledby="offcanvasDarkNavbarLabel">
          <div className="offcanvas-header">
            <h5 className="offcanvas-title" id="offcanvasDarkNavbarLabel">API Calls</h5>
            <button type="button" className="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
          </div>
          <div className="offcanvas-body">
            <ul className="navbar-nav justify-content-end flex-grow-1 pe-3">
              <li className="nav-item dropdown">
                <span className="nav-link dropdown-toggle" id="servicesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Stock Info: Alpha Vantage API</span>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="servicesDropdown">

                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/MultiViewPage">MultiViewPage</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/TSIGraphs">Time Series Intradaily Graph</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/TSITable">Time Series Intradaily Table</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/TSIGraphInteractive">Time Series Intradaily Interactive Graph</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/TSIDynamicGraph">Time Series Intradaily Dynamic Graph</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/Compare3Stocks">Compare Line Graph: SPY QQQ IWM</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/CandleStickGraph">CandleStick Graph</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/CandleStick2">CandleStick2</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/StackedBarChart">Stacked Bar Charts: SPY QQQ IWM</NavLink>
                  </li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <span className="nav-link dropdown-toggle" id="salesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Notes</span>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="salesDropdown">
                  <li className="nav-item">
                  <NavLink to="/AllNotesPage">All Notes Page</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/picturehouse">X</NavLink>
                  </li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <span className="nav-link dropdown-toggle" id="salesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Calendars</span>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="salesDropdown">
                  <li className="nav-item">
                  <NavLink to="/CalendarReact">React-Calendar</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/Calendar">Plain Calendar</NavLink>
                  </li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <span className="nav-link dropdown-toggle" id="servicesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Doggies</span>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="servicesDropdown">
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/DoggieGame">Wesley's Favorite Doggie Game!</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/dogfavs">Favorite Doggie</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/doghouse">DogHouse</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/dogs">DOGGIES! the first version</NavLink>
                  </li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <span className="nav-link dropdown-toggle" id="inventoryDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">NASA</span>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="inventoryDropdown">
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/nasa">NASA Daily Pic</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/nasahouse">NASA Previous Daily Pics</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/NasaCarousel">NASA Carousel</NavLink>
                  </li>
                </ul>
                <li className="nav-item dropdown">
                  <span className="nav-link dropdown-toggle" id="salesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Moments of Zen</span>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="salesDropdown">
                <li className="nav-item">
                  <NavLink to="/zen">Moment of Zen</NavLink>
                  </li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <span className="nav-link dropdown-toggle" id="salesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Pretty Pictures</span>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="salesDropdown">
                  <li className="nav-item">
                  <NavLink to="/pictures">Pretty Pictures</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/picturehouse">All the pretty pictures</NavLink>
                  </li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <span className="nav-link dropdown-toggle" id="salesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Extra Fun CSS Stuff!</span>
                  <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="salesDropdown">
                    <li className="nav-item">
                      <NavLink to="/circles">Color Game/Circles</NavLink>
                    </li>
                    <li className="nav-item">
                      <NavLink to="/CSSFactory">CSSFactory</NavLink>
                  </li>
                  <li className="nav-item">
                      <NavLink to="/Maslow">Dr Maslow</NavLink>
                  </li>
                </ul>
              </li>
              </li>

            </ul>
          </div>
        </div>
      </div>
    </nav>
)
}

export default Nav;
