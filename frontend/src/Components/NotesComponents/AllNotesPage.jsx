import IWMNotes from "./IWMNotes"
import SPYNotes from "./SPYNotes"
import QQQNotes from "./QQQNotes"
import MSFTNotes from "./MSFTNotes"
import Notes from "./Notes"

function AllNotesPage () {

    return (
        <>
        <div style={{marginBottom:'0%'}}>
        </div>
        <div>
            {/* <Notes /> */}
            <QQQNotes />
            <IWMNotes />
            <SPYNotes />
            <MSFTNotes />
        </div>
        </>
    )
}

export default AllNotesPage
