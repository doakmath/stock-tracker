import { useState, useEffect } from "react"

function ViewAllNotes () {

    const [notes, setNotes] = useState([])
    const [qqqNotes, setQQQNotes] = useState([])
    const [spyNotes, setSPYNotes] = useState([])
    const [iwmNotes, setIWMNotes] = useState([])
    const [msftNotes, setMSFTNotes] = useState([])

    const [noteText, setNoteText] = useState('')

    const [createSwitch, setCreateSwitch] = useState(false)
    const [viewSwitch, setViewSwitch] = useState(false)


    useEffect(() => {
        fetchQQQNotesData()
        fetchSPYNotesData()
        fetchIWMNotesData()
        fetchMSFTNotesData()
        fetchNotesData()
    }, []);


    // _____________________________________________fetch requests for data___________________________________

    const fetchNotesData = async () => {
        const url = 'http://localhost:8000/notes/getAllNotes/';
        try {
            const response = await fetch(url);
            const result = await response.json();
            console.log("result", result)
            setNotes(result["note"]);
        } catch (error) {
            console.error(error);
        }
    }

    const fetchQQQNotesData = async () => {
        const url = 'http://localhost:8000/notes/getAllQQQNotes/';
        try {
            const response = await fetch(url);
            const result = await response.json();
            console.log("result", result)
            setQQQNotes(result["QQQ"]);
        } catch (error) {
            console.error(error);
        }
    }

    const fetchSPYNotesData = async () => {
        const url = 'http://localhost:8000/notes/getAllSPYNotes/';
        try {
            const response = await fetch(url);
            const result = await response.json();
            console.log("result", result)
            setSPYNotes(result["SPY"]);
        } catch (error) {
            console.error(error);
        }
    }

    const fetchIWMNotesData = async () => {
        const url = 'http://localhost:8000/notes/getAllIWMNotes/';
        try {
            const response = await fetch(url);
            const result = await response.json();
            console.log("result", result)
            setIWMNotes(result["IWM"]);
        } catch (error) {
            console.error(error);
        }
    }

    const fetchMSFTNotesData = async () => {
        const url = 'http://localhost:8000/notes/getAllMSFTNotes/';
        try {
            const response = await fetch(url);
            const result = await response.json();
            console.log("result", result)
            setMSFTNotes(result["MSFT"]);
        } catch (error) {
            console.error(error);
        }
    }

    async function saveNote(text) {
        console.log("text-------", text)
        const url = "http://localhost:8000/notes/createNote/"
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({text: text})
        }
        const response = await fetch(url, options)
        if (!response.ok) {
            throw new Error('Failed to submit Note data')
        }
    }


    const handleNoteSubmit = async (event) => {
        event.preventDefault()
        await saveNote(noteText)
        setNoteText('')
        alert("Message saved.")
    }

    const handleDelete = async (id) => {
        const deleteUrl = `http://localhost:8000/notes/deleteNote/${id}`
        await fetch(deleteUrl, {method: 'delete'})
        fetchNotesData()
    }

    const handleDeleteAll = async () => {
        const deleteUrl = `http://localhost:8000/notes/deleteAllNotes/`
        await fetch(deleteUrl, {method: 'delete'})
        fetchNotesData()
    }

    const handleCreateSwitch = () => {
        if (createSwitch === false){
        setCreateSwitch(true)
        }
        if (createSwitch === true) {
        setCreateSwitch(false)
        }
    }

    const handleViewSwitch = () => {
        if (viewSwitch === false){
        setViewSwitch(true)
        }
        if (viewSwitch === true) {
        setViewSwitch(false)
        }
    }




    console.log("Notes", notes)
    console.log("QQQ notes", qqqNotes)
    console.log("SPY notes", spyNotes)
    console.log("IWM notes", iwmNotes)
    console.log("MSFT notes", msftNotes)



    return (
        <>
        <div style={{marginBottom:'10%'}}>
        </div>
        <div>
            <button onClick={handleCreateSwitch}>Create a Note</button>
        </div>
        <div>
            <button onClick={handleViewSwitch}>View All Notes</button>
        </div>
        {/* <div>
            <button className='bg-danger' onClick={handleDeleteAll}>Delete ALL Notes</button>
        </div> */}

        {createSwitch === true &&
            <div>
                <div className='d-flex justify-content-center'>
                    <h1>Note: </h1>
                    <form onSubmit={handleNoteSubmit}>
                        <label>
                        <textarea
                            style={{ height: '75px', width: '300px' }}
                            value={noteText}
                            onChange={(e) => setNoteText(e.target.value)}
                            placeholder="Enter your Note here."
                        />
                        </label>
                        <button type="submit">Save</button>
                    </form>
                </div>
            </div>
        }


        {viewSwitch === true &&
            <div className='d-flex justify-content-center'>
                {notes &&
                    <div>
                        <table>
                            <thead>
                                <th>General Notes</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Note</th>
                            </thead>
                            <tbody>
                                {notes.map(note => {
                                    return(
                                        <tr key={note.id}>
                                            <td><button onClick={()=>{handleDelete(note.id)}}>Delete</button></td>
                                            <td>{new Date(note.timestamp).toLocaleDateString()}</td>
                                            <td>{new Date(note.timestamp).toLocaleTimeString()}</td>
                                            <td>{note.text}</td>
                                        </tr>
                                    )
                                })}

                            </tbody>
                        </table>
                    </div>
                }
            </div>
        }
        </>
    )
}

export default ViewAllNotes
