import { useState, useEffect } from "react"

function QQQNotes () {

    const [qqqNotes, setQQQNotes] = useState([])
    const [qqqNoteText, setQQQNoteText] = useState('')

    const [createQQQSwitch, setCreateQQQSwitch] = useState(false)
    const [viewQQQSwitch, setViewQQQSwitch] = useState(false)

    useEffect(() => {
        fetchQQQNotesData()
        }, [])

    const fetchQQQNotesData = async () => {
        const url = 'http://localhost:8000/notes/getAllQQQNotes/';
        try {
            const response = await fetch(url);
            const result = await response.json();
            // console.log("result", result)
            setQQQNotes(result["QQQ"]);
        } catch (error) {
            console.error(error);
        }
    }

    async function saveQQQNote(text) {
        console.log("text-------", text)
        const url = "http://localhost:8000/notes/createQQQNote/"
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({text: text})
        }
        const response = await fetch(url, options)
        if (!response.ok) {
            throw new Error('Failed to submit Note data')
        }
    }

    const handleQQQNoteSubmit = async (event) => {
        event.preventDefault()
        await saveQQQNote(qqqNoteText)
        setQQQNoteText('')
        alert("Message saved.")
    }

    const handleDelete = async (id) => {
        const deleteUrl = `http://localhost:8000/notes/deleteQQQNote/${id}`
        await fetch(deleteUrl, {method: 'delete'})
        fetchQQQNotesData()
    }

    const handleDeleteAll = async () => {
        const deleteUrl = `http://localhost:8000/notes/deleteAllQQQNotes/`
        await fetch(deleteUrl, {method: 'delete'})
        fetchQQQNotesData()
    }

    const handleCreateQQQSwitch = () => {
        if (createQQQSwitch === false){
        setCreateQQQSwitch(true)
        }
        if (createQQQSwitch === true) {
        setCreateQQQSwitch(false)
        }
    }

    const handleViewQQQSwitch = () => {
        if (viewQQQSwitch === false){
        setViewQQQSwitch(true)
        }
        if (viewQQQSwitch === true) {
        setViewQQQSwitch(false)
        }
    }

    const buttonStyle = {
        // backgroundColor: 'gray',
        borderRadius: '25%'
        }

    // console.log("QQQ notes", qqqNotes)

    return (

        <>


        {/* <div style={{marginBottom:'10%'}}>
        </div> */}
        <div>
            <button onClick={handleViewQQQSwitch}>QQQ Notes</button>
        </div>
        {viewQQQSwitch === true &&
            <div>
                <button onClick={handleCreateQQQSwitch}>Create a QQQ Note</button>
            </div>
        }
        {/* <div>
            <button className='bg-danger' onClick={handleDeleteAll}>Delete ALL Notes</button>
        </div> */}


        {createQQQSwitch === true && viewQQQSwitch === true &&
            <div>
                <div className='d-flex justify-content-center'>
                    <h1>QQQ Note: </h1>
                    <form onSubmit={handleQQQNoteSubmit}>
                        <label>
                        <textarea
                            style={{ height: '75px', width: '300px' }}
                            value={qqqNoteText}
                            onChange={(e) => setQQQNoteText(e.target.value)}
                            placeholder="Enter your Note here."
                        />
                        </label>
                        <button type="submit">Save QQQ Note</button>
                    </form>
                </div>
            </div>
        }


        {viewQQQSwitch === true &&
            <div className='d-flex justify-content-center'>
                {qqqNotes &&
                    <div>
                        <table>
                            <thead>
                                <th>QQQ Notes</th>
                                <th>Date</th>
                                <th>Note</th>
                                <th>Time</th>
                            </thead>
                            <tbody>
                                {qqqNotes.map(qqqNote => {
                                    return(
                                        <tr key={qqqNote.id}>
                                            <td><button onClick={()=>{handleDelete(qqqNote.id)}}>Delete</button></td>
                                            <td>{new Date(qqqNote.timestamp).toLocaleDateString()}</td>
                                            <td>{qqqNote.text}</td>
                                            <td>{new Date(qqqNote.timestamp).toLocaleTimeString()}</td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                }
            </div>
        }



        </>

    )
}

export default QQQNotes
