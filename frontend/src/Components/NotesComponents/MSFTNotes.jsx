import { useState, useEffect } from "react"

function MSFTNotes () {

    const [msftNotes, setMSFTNotes] = useState([])
    const [msftNoteText, setMSFTNoteText] = useState('')

    const [createMSFTSwitch, setCreateMSFTSwitch] = useState(false)
    const [viewMSFTSwitch, setViewMSFTSwitch] = useState(false)

    useEffect(() => {
        fetchMSFTNotesData()
        }, [])

    const fetchMSFTNotesData = async () => {
        const url = 'http://localhost:8000/notes/getAllMSFTNotes/';
        try {
            const response = await fetch(url);
            const result = await response.json();
            console.log("result", result)
            setMSFTNotes(result["MSFT"]);
        } catch (error) {
            console.error(error);
        }
    }

    async function saveMSFTNote(text) {
        console.log("text-------", text)
        const url = "http://localhost:8000/notes/createMSFTNote/"
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({text: text})
        }
        const response = await fetch(url, options)
        if (!response.ok) {
            throw new Error('Failed to submit Note data')
        }
    }

    const handleMSFTNoteSubmit = async (event) => {
        event.preventDefault()
        await saveMSFTNote(msftNoteText)
        setMSFTNoteText('')
        alert("Message saved.")
    }

    const handleDelete = async (id) => {
        const deleteUrl = `http://localhost:8000/notes/deleteMSFTNote/${id}`
        await fetch(deleteUrl, {method: 'delete'})
        fetchMSFTNotesData()
    }

    const handleDeleteAll = async () => {
        const deleteUrl = `http://localhost:8000/notes/deleteAllMSFTNotes/`
        await fetch(deleteUrl, {method: 'delete'})
        fetchMSFTNotesData()
    }

    const handleCreateMSFTSwitch = () => {
        if (createMSFTSwitch === false){
        setCreateMSFTSwitch(true)
        }
        if (createMSFTSwitch === true) {
        setCreateMSFTSwitch(false)
        }
    }

    const handleViewMSFTSwitch = () => {
        if (viewMSFTSwitch === false){
        setViewMSFTSwitch(true)
        }
        if (viewMSFTSwitch === true) {
        setViewMSFTSwitch(false)
        }
    }

    // console.log("MSFT notes", msftNotes)

    return (

        <>


        {/* <div style={{marginBottom:'10%'}}>
        </div> */}
        <div>
            <button onClick={handleViewMSFTSwitch}>MSFT Notes</button>
        </div>
        {viewMSFTSwitch === true &&
            <div>
                <button onClick={handleCreateMSFTSwitch}>Create a MSFT Note</button>
            </div>
        }
        {/* <div>
            <button className='bg-danger' onClick={handleDeleteAll}>Delete ALL Notes</button>
        </div> */}


        {createMSFTSwitch === true && viewMSFTSwitch === true &&
            <div>
                <div className='d-flex justify-content-center'>
                    <h1>MSFT Note: </h1>
                    <form onSubmit={handleMSFTNoteSubmit}>
                        <label>
                        <textarea
                            style={{ height: '75px', width: '300px' }}
                            value={msftNoteText}
                            onChange={(e) => setMSFTNoteText(e.target.value)}
                            placeholder="Enter your Note here."
                        />
                        </label>
                        <button type="submit">Save MSFT Note</button>
                    </form>
                </div>
            </div>
        }


        {viewMSFTSwitch === true &&
            <div className='d-flex justify-content-center'>
                {msftNotes &&
                    <div>
                        <table>
                            <thead>
                                <th>MSFT Notes</th>
                                <th>Date</th>
                                <th>Note</th>
                                <th>Time</th>
                            </thead>
                            <tbody>
                                {msftNotes.map(msftNote => {
                                    return(
                                        <tr key={msftNote.id}>
                                            <td><button onClick={()=>{handleDelete(msftNote.id)}}>Delete</button></td>
                                            <td>{new Date(msftNote.timestamp).toLocaleDateString()}</td>
                                            <td>{msftNote.text}</td>
                                            <td>{new Date(msftNote.timestamp).toLocaleTimeString()}</td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                }
            </div>
        }


        </>

    )
}

export default MSFTNotes
