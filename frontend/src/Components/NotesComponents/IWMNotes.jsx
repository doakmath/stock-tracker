import { useState, useEffect } from "react"

function IWMNotes () {

    const [iwmNotes, setIWMNotes] = useState([])
    const [iwmNoteText, setIWMNoteText] = useState('')

    const [createIWMSwitch, setCreateIWMSwitch] = useState(false)
    const [viewIWMSwitch, setViewIWMSwitch] = useState(false)

    useEffect(() => {
        fetchIWMNotesData()
        }, [])

    const fetchIWMNotesData = async () => {
        const url = 'http://localhost:8000/notes/getAllIWMNotes/';
        try {
            const response = await fetch(url);
            const result = await response.json();
            console.log("result", result)
            setIWMNotes(result["IWM"]);
        } catch (error) {
            console.error(error);
        }
    }

    async function saveIWMNote(text) {
        console.log("text-------", text)
        const url = "http://localhost:8000/notes/createIWMNote/"
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({text: text})
        }
        const response = await fetch(url, options)
        if (!response.ok) {
            throw new Error('Failed to submit Note data')
        }
    }

    const handleIWMNoteSubmit = async (event) => {
        event.preventDefault()
        await saveIWMNote(iwmNoteText)
        setIWMNoteText('')
        alert("Message saved.")
    }

    const handleDelete = async (id) => {
        const deleteUrl = `http://localhost:8000/notes/deleteIWMNote/${id}`
        await fetch(deleteUrl, {method: 'delete'})
        fetchIWMNotesData()
    }

    const handleDeleteAll = async () => {
        const deleteUrl = `http://localhost:8000/notes/deleteAllIWMNotes/`
        await fetch(deleteUrl, {method: 'delete'})
        fetchIWMNotesData()
    }

    const handleCreateIWMSwitch = () => {
        if (createIWMSwitch === false){
        setCreateIWMSwitch(true)
        }
        if (createIWMSwitch === true) {
        setCreateIWMSwitch(false)
        }
    }

    const handleViewIWMSwitch = () => {
        if (viewIWMSwitch === false){
        setViewIWMSwitch(true)
        }
        if (viewIWMSwitch === true) {
        setViewIWMSwitch(false)
        }
    }

    // console.log("IWM notes", iwmNotes)

    return (

        <>


        {/* <div style={{marginBottom:'10%'}}>
        </div> */}
        <div>
            <button onClick={handleViewIWMSwitch}>IWM Notes</button>
        </div>
        {viewIWMSwitch === true &&
            <div>
                <button onClick={handleCreateIWMSwitch}>Create a IWM Note</button>
            </div>
        }
        {/* <div>
            <button className='bg-danger' onClick={handleDeleteAll}>Delete ALL Notes</button>
        </div> */}


        {createIWMSwitch === true && viewIWMSwitch === true &&
            <div>
                <div className='d-flex justify-content-center'>
                    <h1>IWM Note: </h1>
                    <form onSubmit={handleIWMNoteSubmit}>
                        <label>
                        <textarea
                            style={{ height: '75px', width: '300px' }}
                            value={iwmNoteText}
                            onChange={(e) => setIWMNoteText(e.target.value)}
                            placeholder="Enter your Note here."
                        />
                        </label>
                        <button type="submit">Save IWM Note</button>
                    </form>
                </div>
            </div>
        }


        {viewIWMSwitch === true &&
            <div className='d-flex justify-content-center'>
                {iwmNotes &&
                    <div>
                        <table>
                            <thead>
                                <th>IWM Notes</th>
                                <th>Date</th>
                                <th>Note</th>
                                <th>Time</th>
                            </thead>
                            <tbody>
                                {iwmNotes.map(iwmNote => {
                                    return(
                                        <tr key={iwmNote.id}>
                                            <td><button onClick={()=>{handleDelete(iwmNote.id)}}>Delete</button></td>
                                            <td>{new Date(iwmNote.timestamp).toLocaleDateString()}</td>
                                            <td>{iwmNote.text}</td>
                                            <td>{new Date(iwmNote.timestamp).toLocaleTimeString()}</td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                }
            </div>
        }


        </>

    )
}

export default IWMNotes
