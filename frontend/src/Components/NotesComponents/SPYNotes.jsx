import { useState, useEffect } from "react"

function SPYNotes () {

    const [spyNotes, setSPYNotes] = useState([])
    const [spyNoteText, setSPYNoteText] = useState('')

    const [createSPYSwitch, setCreateSPYSwitch] = useState(false)
    const [viewSPYSwitch, setViewSPYSwitch] = useState(false)

    useEffect(() => {
        fetchSPYNotesData()
        }, [])

    const fetchSPYNotesData = async () => {
        const url = 'http://localhost:8000/notes/getAllSPYNotes/';
        try {
            const response = await fetch(url);
            const result = await response.json();
            console.log("result", result)
            setSPYNotes(result["SPY"]);
        } catch (error) {
            console.error(error);
        }
    }

    async function saveSPYNote(text) {
        console.log("text-------", text)
        const url = "http://localhost:8000/notes/createSPYNote/"
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({text: text})
        }
        const response = await fetch(url, options)
        if (!response.ok) {
            throw new Error('Failed to submit SPYNote data')
        }
    }

    const handleSPYNoteSubmit = async (event) => {
        event.preventDefault()
        await saveSPYNote(spyNoteText)
        setSPYNoteText('')
        alert("Message saved.")
    }

    const handleDelete = async (id) => {
        const deleteUrl = `http://localhost:8000/notes/deleteSPYNote/${id}`
        await fetch(deleteUrl, {method: 'delete'})
        fetchSPYNotesData()
    }

    const handleDeleteAll = async () => {
        const deleteUrl = `http://localhost:8000/notes/deleteAllSPYNotes/`
        await fetch(deleteUrl, {method: 'delete'})
        fetchSPYNotesData()
    }

    const handleCreateSPYSwitch = () => {
        if (createSPYSwitch === false){
        setCreateSPYSwitch(true)
        }
        if (createSPYSwitch === true) {
        setCreateSPYSwitch(false)
        }
    }

    const handleViewSPYSwitch = () => {
        if (viewSPYSwitch === false){
        setViewSPYSwitch(true)
        }
        if (viewSPYSwitch === true) {
        setViewSPYSwitch(false)
        }
    }

    // console.log("SPY notes", spyNotes)

    return (

        <>


        {/* <div style={{marginBottom:'10%'}}>
        </div> */}
        <div>
            <button onClick={handleViewSPYSwitch}>SPY Notes</button>
        </div>
        {viewSPYSwitch === true &&
            <div>
                <button onClick={handleCreateSPYSwitch}>Create a SPY Note</button>
            </div>
        }
        {/* <div>
            <button className='bg-danger' onClick={handleDeleteAll}>Delete ALL Notes</button>
        </div> */}


        {createSPYSwitch === true && viewSPYSwitch === true &&
            <div>
                <div className='d-flex justify-content-center'>
                    <h1>SPY Note: </h1>
                    <form onSubmit={handleSPYNoteSubmit}>
                        <label>
                        <textarea
                            style={{ height: '75px', width: '300px' }}
                            value={spyNoteText}
                            onChange={(e) => setSPYNoteText(e.target.value)}
                            placeholder="Enter your Note here."
                        />
                        </label>
                        <button type="submit">Save SPY Note</button>
                    </form>
                </div>
            </div>
        }


        {viewSPYSwitch === true &&
            <div className='d-flex justify-content-center'>
                {spyNotes &&
                    <div>
                        <table>
                            <thead>
                                <th>SPY Notes</th>
                                <th>Date</th>
                                <th>Note</th>
                                <th>Time</th>
                            </thead>
                            <tbody>
                                {spyNotes.map(spyNote => {
                                    return(
                                        <tr key={spyNote.id}>
                                            <td><button onClick={()=>{handleDelete(spyNote.id)}}>Delete</button></td>
                                            <td>{new Date(spyNote.timestamp).toLocaleDateString()}</td>
                                            <td>{spyNote.text}</td>
                                            <td>{new Date(spyNote.timestamp).toLocaleTimeString()}</td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                }
            </div>
        }


        </>

    )
}

export default SPYNotes
