import { useState, useEffect } from "react"

function Notes () {

    const [notes, setNotes] = useState([])
    const [noteText, setNoteText] = useState('')

    const [createSwitch, setCreateSwitch] = useState(false)
    const [viewSwitch, setViewSwitch] = useState(false)

    useEffect(() => {
        fetchNotesData()
        }, [])

    const fetchNotesData = async () => {
        const url = 'http://localhost:8000/notes/getAllNotes/';
        try {
            const response = await fetch(url);
            const result = await response.json();
            console.log("result", result)
            setNotes(result["note"]);
        } catch (error) {
            console.error(error);
        }
    }

    async function saveNote(text) {
        console.log("text-------", text)
        const url = "http://localhost:8000/notes/createNote/"
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({text: text})
        }
        const response = await fetch(url, options)
        if (!response.ok) {
            throw new Error('Failed to submit Note data')
        }
    }

    const handleNoteSubmit = async (event) => {
        event.preventDefault()
        await saveNote(noteText)
        setNoteText('')
        alert("Message saved.")
    }

    const handleDelete = async (id) => {
        const deleteUrl = `http://localhost:8000/notes/deleteNote/${id}`
        await fetch(deleteUrl, {method: 'delete'})
        fetchNotesData()
    }

    const handleDeleteAll = async () => {
        const deleteUrl = `http://localhost:8000/notes/deleteAllNotes/`
        await fetch(deleteUrl, {method: 'delete'})
        fetchNotesData()
    }

    const handleCreateSwitch = () => {
        if (createSwitch === false){
        setCreateSwitch(true)
        }
        if (createSwitch === true) {
        setCreateSwitch(false)
        }
    }

    const handleViewSwitch = () => {
        if (viewSwitch === false){
        setViewSwitch(true)
        }
        if (viewSwitch === true) {
        setViewSwitch(false)
        }
    }

    console.log(" notes", notes)

    return (

        <>


        {/* <div style={{marginBottom:'10%'}}>
        </div> */}
        <div>
            <button onClick={handleViewSwitch}>General  Notes</button>
        </div>
        {viewSwitch === true &&
            <div>
                <button onClick={handleCreateSwitch}>Create a General Note</button>
            </div>
        }
        {/* <div>
            <button className='bg-danger' onClick={handleDeleteAll}>Delete ALL Notes</button>
        </div> */}


        {createSwitch === true &&
            <div>
                <div className='d-flex justify-content-center'>
                    <h1> Note: </h1>
                    <form onSubmit={handleNoteSubmit}>
                        <label>
                        <textarea
                            style={{ height: '75px', width: '300px' }}
                            value={noteText}
                            onChange={(e) => setNoteText(e.target.value)}
                            placeholder="Enter your Note here."
                        />
                        </label>
                        <button type="submit">Save  Note</button>
                    </form>
                </div>
            </div>
        }


        {viewSwitch === true &&
            <div className='d-flex justify-content-center'>
                {notes &&
                    <div>
                        <table>
                            <thead>
                                <th> Notes</th>
                                <th>Date</th>
                                <th>Note</th>
                                <th>Time</th>
                            </thead>
                            <tbody>
                                {notes.map(note => {
                                    return(
                                        <tr key={note.id}>
                                            <td><button onClick={()=>{handleDelete(note.id)}}>Delete</button></td>
                                            <td>{new Date(note.timestamp).toLocaleDateString()}</td>
                                            <td>{note.text}</td>
                                            <td>{new Date(note.timestamp).toLocaleTimeString()}</td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                }
            </div>
        }


        </>

    )
}

export default Notes
