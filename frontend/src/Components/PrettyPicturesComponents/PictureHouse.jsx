import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function PictureHouse () {

    const [prettypic, setPrettypic] = useState([])


    const fetchData = async () => {
        const url = "http://localhost:8000/pictures/listPrettypic"
        const response = await fetch(url);
        try {
            if (response.ok) {
            const data = await response.json()
            setPrettypic(data.prettypic)
            }
        } catch (error) {
            console.error("Error retrieving fetchData-nasaList", error)
        }
    }

    useEffect(() => {
        fetchData()
    }, []);


    async function deletePrettyPic(id) {
        const url = `http://localhost:8000/pictures/deletePrettypic/${id}`
        const fetchConfig = {method: "DELETE"}
        const response = await fetch(url, fetchConfig)
        try {
            if (response.ok) {
                fetchData()
            }
        } catch (error) {
            console.error("deletion error, perhaps it doesn't exist", error)
        }
    }

    async function deleteAllPrettyPics() {
        const url = `http://localhost:8000/pictures/deleteAllPrettypics`
        const fetchConfig = {method: "DELETE"}
        const response = await fetch(url, fetchConfig)
        try {
            if (response.ok) {
                fetchData()
            }
        } catch (error) {
            console.error("deletion error, perhaps it doesn't exist", error)
        }
    }

console.log("prettypic list", prettypic)

    return (
        <>
        <div style={{marginBottom:'5%'}}>
            {/* <Link to="/"><button>HOME</button></Link>
            <Link to="/pictures"><button>PICTURES</button></Link>
            <Link to="/picturehouse"><button>all the pictures</button></Link>
            <Link to="/dogs"><button>DOGGIES!</button></Link>
            <Link to="/dogfavs"><button>Favorite Doggies</button></Link>
            <Link to="/doghouse"><button>DogHouse</button></Link>
            <Link to="/nasa"><button>NASA Daily Pic</button></Link>
            <Link to="/nasahouse"><button>NASA Previous Pics</button></Link>
            <Link to="/zen"><button>MOMENT OF ZEN</button></Link> */}
        </div>
        <div>
            <h1>Copyright Free Beautiful Images from: free images API</h1>
        </div>
        {/* <button onClick={() => deleteAllPrettyPics(prettypic)}>Delete ALL</button> */}
        <div className="picture-container" style={{ display: "flex", flexWrap: "wrap" }}>
            {prettypic.map(pic => (
                <div key={pic.id} className="picture-card" style={{ margin: "10px", border: "1px solid black", padding: "10px" }}>
                    <img src={pic.image} alt="Pretty Pic" style={{ width: "200px", height: "200px" }} />
                    <div className="picture-details">
                        <button onClick={() => deletePrettyPic(pic.id)}>Delete</button>
                    </div>
                </div>
            ))}
        </div>
        </>
    )
}

export default PictureHouse
