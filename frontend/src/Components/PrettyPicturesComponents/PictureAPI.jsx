import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Carousel from 'react-bootstrap/Carousel'
import './PictureAPI.css'

function PictureAPI () {


    const [pictureList, setPictureList] = useState([]);
    const [pic1, setPic1] = useState('')
    const [pic2, setPic2] = useState('')
    const [pic3, setPic3] = useState('')

    const getPictures = async () => {
        const url = 'https://free-images-api.p.rapidapi.com/images/wallpaper';
        const options = {
            method: 'GET',
            headers: {
                'X-RapidAPI-Key': 'bf0635dbcemshde30513e0b8df0cp129e7djsn9f83ff38a9b3',
                'X-RapidAPI-Host': 'free-images-api.p.rapidapi.com'
            }
        }
        try {
            const response = await fetch(url, options);
            const result = await response.json()
            const pictureList = result.results.map(picture => picture)
            setPictureList(pictureList)
            setPic1(pictureList[0].image)
            setPic2(pictureList[1].image)
            setPic3(pictureList[2].image)
        } catch (error) {
            console.error(error);
        }
    }

    useEffect(() => {
        getPictures()
    }, []) // Empty dependency array ensures useEffect runs only once after initial render



    async function savePictures (pictures) {
        for (const picture of pictures) {
          await savePicture(picture)
        }
        alert("Save Successful!")
      }

    async function savePicture(picture) {
        try {
            const url = 'http://localhost:8000/pictures/createPrettypic'
            const pictureObject = {
                image: picture.image,
                by: picture.by,
                download: picture.download,
                source: picture.source,
                diffrentSizes: picture.diffrentSizes,
                pic_id: picture.id
              }
            const fetchOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(pictureObject)
            }
            const response = await fetch(url, fetchOptions)
            if (!response.ok) {
                throw new Error('Failed to submit data')
            }
            console.log('Pictures submitted successfully:', picture)
        } catch (error) {
            console.error('Error submitting pictures:', error)
        }
    }


    console.log("pictureList", pictureList)


    return (
        <>
        <div>
            <Link to="/"><button>HOME</button></Link>
            <Link to="/pictures"><button>PICTURES</button></Link>
            <Link to="/picturehouse"><button>all the pictures</button></Link>
            <Link to="/dogs"><button>DOGGIES!</button></Link>
            <Link to="/dogfavs"><button>Favorite Doggies</button></Link>
            <Link to="/doghouse"><button>DogHouse</button></Link>
            <Link to="/nasa"><button>NASA Daily Pic</button></Link>
            <Link to="/nasahouse"><button>NASA Previous Pics</button></Link>
            <Link to="/zen"><button>MOMENT OF ZEN</button></Link>
        </div>
        <div>
            <h1>`</h1>
        </div>
        <div className="carousel-container">
    <Carousel width="100">
        <Carousel.Item>
            <img
                className="d-block w-100"
                height="250" width="250"
                src={pic1}
                alt="First slide"
            />
        </Carousel.Item>
        <Carousel.Item>
            <img
                className="d-block w-100"
                height="250" width="250"
                src={pic2}
                alt="First slide"
            />
        </Carousel.Item>
        <Carousel.Item>
            <img
                className="d-block w-100"
                height="250" width="250"
                src={pic3}
                alt="First slide"
            />
        </Carousel.Item>
    </Carousel>
</div>
        <div>
            <button
                onClick = {() => savePictures(pictureList)}
                style={{
                padding: '15px 30px 30px', // Add more padding on the bottom
                fontSize: '32px', // Keep button size
                backgroundColor: 'lightblue', // Keep button color
                color: 'darkpink', // Keep text color
                border: 'none', // Remove border
                borderRadius: '50px', // Very round edges
                fontFamily: 'Arial, sans-serif', // Keep font family
                fontWeight: 'bold', // Keep font weight
                textTransform: 'uppercase' // Keep text transformation
                }}
            >
                Click to SAVE ALL!!!
            </button>
        </div>
        <div>
            <table className="table table-striped m-3">
                <thead>
                    <tr>
                        <th>PICTURES!!!</th>
                        <th>Click to DOWNLOAD</th>
                        <th>Picture Made Possible By:</th>
                    </tr>
                </thead>
                <tbody>
                    {pictureList.map((picture) => (
                        <tr key={picture.id}>
                            <td><img src={picture.image} alt="random" height="200"></img></td>
                            <td><a href={picture.download} target="_blank" rel="noopener noreferrer">{picture.download}</a></td>
                            <td>{picture.by}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
        </>
    )
}
export default PictureAPI;
