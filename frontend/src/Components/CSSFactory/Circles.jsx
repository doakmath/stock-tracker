import React, { useState, useEffect } from 'react';

const Circles = () => {
  const [messageVisible, setMessageVisible] = useState(false);
  const [messageWord, setMessageWord] = useState('');
  const [fontColorIndex, setFontColorIndex] = useState(0);
  const [clickedCircleColor, setClickedCircleColor] = useState('');
  const [value, setValue] = useState('')
  const fontColors = ['#ff0000', '#ffa500', '#ffff00', '#008000', '#0000ff', '#800080'];
  const [reloadCount, setReloadCount] = useState(0)

  const showRandomWord = (color) => {
    setMessageWord(getRandomWord());
    setMessageVisible(true);
    setFontColorIndex(prevIndex => (prevIndex + 1) % fontColors.length);
    setClickedCircleColor(color);
  };

  const getRandomWord = () => {
    const words = ['red', 'orange', 'green', 'yellow', 'blue', 'purple'];
    const randomIndex = Math.floor(Math.random() * words.length);
    return words[randomIndex];
  };

  const generateRandomStyle = () => {
    const circleColor = generateRandomColor();
    return {
      backgroundColor: circleColor,
      width: generateRandomNumber(80, 200) + 'px',
      height: generateRandomNumber(80, 200) + 'px',
      borderRadius: '50%',
      position: 'absolute',
      top: generateRandomNumber(0, window.innerHeight) + 'px',
      left: generateRandomNumber(0, window.innerWidth) + 'px',
      animation: 'moveUpDown 3s linear infinite',
      cursor: 'pointer',
    };
  };

  const generateRandomNumber = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min);
  };

  const generateRandomColor = () => {
    const r = generateRandomNumber(0, 255);
    const g = generateRandomNumber(0, 255);
    const b = generateRandomNumber(0, 255);
    const alpha = Math.random().toFixed(1);
    return `rgba(${r}, ${g}, ${b}, ${alpha})`;
  };

  const messageStyle = {
    fontSize: '8rem',
    textAlign: 'center',
    color: fontColors[fontColorIndex],
    margin: '20px 0',
  };

  const handleClick = (e) => {
    const value = e.target
    setValue(value)
  }

  useEffect(() => {
    const intervalId = setInterval(() => {
      // Update state to trigger re-render
      setReloadCount(prevCount => prevCount + 1);
      showRandomWord()
    }, 1000); // Reload every 3 seconds

    return () => clearInterval(intervalId); // Cleanup on unmount
  }, []); // Empty dependency array ensures effect runs only once

  console.log("Value", value)


  return (
    <>
      {messageVisible && <div style={messageStyle}>{messageWord}</div>}
      <div className="circles-wrapper d-flex flex-wrap" style={{ width: '100vw', position: 'relative', minHeight: '100vh' }}>
        {Array.from({ length: 26 }, (_, index) => (
          <div key={index} className="circle" style={generateRandomStyle()} onClick={() => showRandomWord(clickedCircleColor)} onChange={(e) => handleClick(e) }></div>
        ))}
      </div>
      <div style={{ backgroundColor: 'rgba(173, 216, 230, 0.5)', minWidth: '100%', minHeight: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center', position: 'relative', marginTop: '-700px'}}>
      <div>
        <div>
          <h1>How to Play:</h1>
          <h4>Try to say the COLOR of the displayed word.  Avoid saying the word itself (unless it happens to ALSO be the color.)</h4>
        </div>
        <div>
          <h2>Stroop Effect</h2>
        </div>
        <div>

          <h5>
            Why it's funky: Your brain gets tangled up because reading words is automatic, but naming colors takes effort.
          </h5>
          <h5>
            Who found it: John Stroop back in 1935, spotting this wacky mind trick.
          </h5>
        </div>
      </div>
      <div>
      <h5>
        A mind mess-up where you try to say the color of a word, but the word itself throws you off.
      </h5>
      </div>
      <div>
        Psychological Explanation:

        The Stroop Effect demonstrates the interference in reaction time when the brain processes conflicting information. Specifically, it illustrates the automatic processing of reading words versus the effortful processing required to name the color of the ink. When the color word and the ink color match (congruent condition), the task is relatively easy because reading the word happens automatically and quickly. However, when the ink color and the word differ (incongruent condition), there is interference between the two processes, resulting in slower reaction times and often errors.
      <div>
        Effect:

        The Stroop Effect demonstrates the cognitive phenomenon of interference, where the processing of one aspect of a stimulus interferes with the processing of another aspect. It highlights the complexity of cognitive processes involved in tasks like selective attention, cognitive control, and response inhibition.
      </div>
        Psychologists:

        The Stroop Effect was first described by John Ridley Stroop in his 1935 study "Studies of interference in serial verbal reactions." Stroop found that participants had difficulty naming the ink color when it differed from the color word written. Since then, the Stroop Effect has become a widely studied phenomenon in psychology, with numerous variations and applications in fields such as cognitive psychology, neuropsychology, and experimental psychology.
      </div>
    </div>
    </>
  );
};

export default Circles;
