import React from 'react'

const Maslow = () => {

  const trapezoidStyle = {
    height: 0,
    borderLeft: '60px solid transparent',
    borderRight: '60px solid transparent'
  }

  const bottomTrapezoidStyle = {
    ...trapezoidStyle,
    borderBottom: '120px solid #87ceeb',
    width: '600px',
    marginBottom: '-20px'
  }

  const topTriangleStyle = {
    height: 0,
    borderLeft: '170px solid transparent',
    borderRight: '170px solid transparent',
    borderBottom: '310px solid #87ceeb',
    position: 'relative'
  }

  const containerStyle = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh'
  }

  const selfActualizationStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    color: 'black',
    fontSize: '24px'
  }


  return (
    <>
    <div style={{marginBottom: '10%'}}>
    </div>
    <div style={containerStyle}>
        <div>
            <h1>
                Maslow's Hierarchy of Needs:
            </h1>
            <h4 className= 'd-flex justify-content-center'>
                Every humans most basic path towards:
            </h4>
        </div>
        <div><h2>Self Actualization</h2></div>
      <div style={{ ...topTriangleStyle, borderBottomColor: '#4682B4' }}></div>
      <div style={{ ...bottomTrapezoidStyle, width: '460px', borderBottomColor: 'green' }}><h4 className= 'd-flex justify-content-center'>Self Esteem</h4><p className= 'd-flex justify-content-center'></p></div>

      <div style={{ ...bottomTrapezoidStyle, width: '560px', borderBottomColor: 'yellow' }}><h3 className= 'd-flex justify-content-center'>Love and Belonging</h3></div>

      <div style={{ ...bottomTrapezoidStyle, width: '660px', borderBottomColor: 'orange' }}> <h2 className= 'd-flex justify-content-center'>Safety and Security</h2></div>

      <div style={{ ...bottomTrapezoidStyle, width: '760px', borderBottomColor: 'red' }}><h1 className= 'd-flex justify-content-center'>Physiological Needs</h1></div>

    </div>
    </>
  )
}

export default Maslow
