import { BarChart, Bar, XAxis, YAxis, Tooltip, ErrorBar } from 'recharts';
import { useState, useEffect } from 'react';

function CandleStickGraph() {
  const [volumeData, setVolumeData] = useState([]);

  const xHighData = volumeData.map(item => item.high);
  const xHighMin = Math.min(...xHighData);
  const xHighMax = Math.max(...xHighData);

  const yHighData = volumeData.map(item => item.high);
  const yHighMin = Math.min(...yHighData);
  const yHighMax = Math.max(...yHighData);


  // const fetchData = async () => {
  //   const url = "http://localhost:8000/stocks/getTsi/"
  //   const response = await fetch(url)
  //   try {
  //     if (response.ok) {
  //       const listData = await response.json()
  //       setVolumeData(listData['tsi'])
  //     }
  //   } catch (error) {
  //       console.log("error!", error)
  //   }
  // }

  const fetchData = async () => {
    const url =
      'https://alpha-vantage.p.rapidapi.com/query?interval=5min&function=TIME_SERIES_INTRADAY&symbol=MSFT&datatype=json&output_size=compact';
    const options = {
      method: 'GET',
      headers: {
        'X-RapidAPI-Key': 'bf0635dbcemshde30513e0b8df0cp129e7djsn9f83ff38a9b3',
        'X-RapidAPI-Host': 'alpha-vantage.p.rapidapi.com'
      }
    };
    try {
      const response = await fetch(url, options);
      const result = await response.json();
      alert('You successfully made an API call!');
      console.log('RESULT', result);
      const volumeData = Object.entries(result['Time Series (5min)']).map(([timestamp, data]) => ({
        timestamp,
        open: parseFloat(data['1. open']),
        high: parseFloat(data['2. high']),
        low: parseFloat(data['3. low']),
        close: parseFloat(data['4. close']),
        volume: parseFloat(data['5. volume'])
      }));
      setVolumeData(volumeData);
    } catch (error) {
      console.error(error);
    }
  };

//   useEffect(() => {
//     fetchData();
//   }, []);

  const handleApiCall = () => {
    fetchData()
  }

  const highestValue = Math.max(...volumeData.map(data => data.high));

  // XXXXX Uncommented the commented out code
  // const data = [
  //     {name: 'Week 1', max: 4000, min: 1400, error: [200, 1000]},
  //     {name: 'Week 2', max: 4000, min: 3400, errorNegative: [1000, 200]},
  //     {name: 'Week 3', max: 3200, min: 2400, errorNegative: [1000, 200]},
  //     {name: 'Week 4', max: 1000, min: 400, error: [200, 1000]},
  //     {name: 'Week 5', max: 2000, min: 1100, error: [200, 1000]},
  //     {name: 'Week 6', max: 3000, min: 400, error: [-200, 2000]},
  //     {name: 'Week 7', max: 500, min: -1000, error: [200, 2000]},
  // ];

  const processedData = volumeData.map(data => ({
    timestamp: data.timestamp,
    high: data.high,
    low: data.low,
    open: data.open,
    close: data.close,
    volume: data.volume,
    errorLow: data.low, // Create error low and high values
    errorHigh: data.high
    }));

  console.log("---------volumeData-----------", volumeData)

  return (
    <>
      <div style={{ marginBottom: '6%' }}></div>
      <div>
        <h1>candlestick for SPY</h1>
      </div>
      <div>
        <button onClick={()=>{ handleApiCall() }}>Click for Todays Info</button>
      </div>
      <BarChart
        width={600}
        height={300}
        data={processedData}
        margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
        >
        <XAxis xAxisId={0} dataKey="timestamp" hide/>
        <XAxis xAxisId={1} dataKey="timestamp"/>
        <YAxis domain={[yHighMin, yHighMax]}/>
        <Tooltip content={(props) => {
            if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
            const { payload } = props;
            const timestamp = new Date(payload[0].payload.timestamp);
            const formattedTimestamp = timestamp.toLocaleString();
            const openValue = payload[0].payload.open;
            const highValue = payload[0].payload.high;
            const lowValue = payload[0].payload.low;
            const closeValue = payload[0].payload.close;
            const volumeValue = payload[0].payload.volume;
            return (
                <div className="custom-tooltip" style={{ color: 'blue', padding: '5px' }}>
                    <p>{`Time: ${new Date(formattedTimestamp).toLocaleTimeString()}`}</p>
                    <p>{`High: ${highValue}`}</p>
                    <p>{`Open: ${openValue}`}</p>
                    <p>{`Close: ${closeValue}`}</p>
                    <p>{`Low: ${lowValue}`}</p>
                    {/* <p>{`Volume: ${volumeValue}`}</p> */}
                </div>
            );
        }} />
        <Bar barSize={60} dataKey="close" fill="blue" />
        <Bar barSize={30} dataKey="open" fill="yellow" >
        <ErrorBar dataKey="low" width={4} strokeWidth={2} stroke="red" />
        <ErrorBar dataKey="high" width={4} strokeWidth={2} stroke="black" />
        </Bar>
     </BarChart>

    </>
  );
}

export default CandleStickGraph;
