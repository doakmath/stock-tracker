// import React from 'react';
import { useEffect, useState } from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';


function Compare3Stocks () {

    const [opacity, setOpacity] = useState({})
    const [spyVolumeData, setSPYVolumeData] = useState([])
    const [qqqVolumeData, setQQQVolumeData] =useState([])
    const [iwmVolumeData, setIWMVolumeData] = useState([])
    // const [combinedVolumeData, setCombinedVolumeData] = useState([])

    // const spyHighData = combinedVolumeData[0]?.SPY?.map(item => item.high) || [];
    // const xHighMin = Math.min(...spyHighData);
    // const xHighMax = Math.max(...spyHighData);

    // const yHighData = combinedVolumeData.flatMap(stock => Object.values(stock)[0].map(item => item.high));
    // const yHighMin = Math.min(...yHighData);
    // const yHighMax = Math.max(...yHighData);

    const handleMouseEnter = (o) => {
    const state = {
        opacity: {
            QQQ: 1,
            SPY: 1,
            IWM: 1
        },
        };
        const { dataKey } = o;
        const { opacity } = state;
        setOpacity({ ...opacity, [dataKey]: 0.5 })
    };


    const handleMouseLeave = (o) => {
    const state = {
        opacity: {
            QQQ: 1,
            SPY: 1,
            IWM: 1,
        },
        };
        const { dataKey } = o;
        const { opacity } = state;
        setOpacity({ ...opacity, [dataKey]: 0.5 })
        }

    const fetchSpyData = async () => {
        const url = 'https://alpha-vantage.p.rapidapi.com/query?interval=5min&function=TIME_SERIES_INTRADAY&symbol=SPY&datatype=json&output_size=compact';
        const options = {
            method: 'GET',
            headers: {
                'X-RapidAPI-Key': 'bf0635dbcemshde30513e0b8df0cp129e7djsn9f83ff38a9b3',
                'X-RapidAPI-Host': 'alpha-vantage.p.rapidapi.com'
            }
        }
        try {
            const response = await fetch(url, options);
            const result = await response.json();
            // alert("You successfully made an API call!")
            console.log("RESULT SPY", result)
            const volumeData = Object.entries(result["Time Series (5min)"]).map(([timestamp, data]) => ({
                timestamp,
                open: parseFloat(data["1. open"]),
                high: parseFloat(data["2. high"]),
                low: parseFloat(data["3. low"]),
                close: parseFloat(data["4. close"]),
                volume: parseFloat(data["5. volume"])
            }))
            setSPYVolumeData(volumeData)
        } catch (error) {
            console.error(error)
        }
    }


    const fetchQQQData = async () => {
        const url = 'https://alpha-vantage.p.rapidapi.com/query?interval=5min&function=TIME_SERIES_INTRADAY&symbol=QQQ&datatype=json&output_size=compact';
        const options = {
            method: 'GET',
            headers: {
                'X-RapidAPI-Key': 'bf0635dbcemshde30513e0b8df0cp129e7djsn9f83ff38a9b3',
                'X-RapidAPI-Host': 'alpha-vantage.p.rapidapi.com'
            }
        }
        try {
            const response = await fetch(url, options);
            const result = await response.json();
            // alert("You successfully made an API call!")
            console.log("RESULT QQQ", result)
            const volumeData = Object.entries(result["Time Series (5min)"]).map(([timestamp, data]) => ({
                timestamp,
                open: parseFloat(data["1. open"]),
                high: parseFloat(data["2. high"]),
                low: parseFloat(data["3. low"]),
                close: parseFloat(data["4. close"]),
                volume: parseFloat(data["5. volume"])
            }))
            setQQQVolumeData(volumeData)
        } catch (error) {
            console.error(error)
        }
    }

    const fetchIWMData = async () => {
        const url = 'https://alpha-vantage.p.rapidapi.com/query?interval=5min&function=TIME_SERIES_INTRADAY&symbol=IWM&datatype=json&output_size=compact';
        const options = {
            method: 'GET',
            headers: {
                'X-RapidAPI-Key': 'bf0635dbcemshde30513e0b8df0cp129e7djsn9f83ff38a9b3',
                'X-RapidAPI-Host': 'alpha-vantage.p.rapidapi.com'
            }
        }
        try {
            const response = await fetch(url, options);
            const result = await response.json();
            // alert("You successfully made an API call!")
            console.log("RESULT IWM", result)
            const volumeData = Object.entries(result["Time Series (5min)"]).map(([timestamp, data]) => ({
                timestamp,
                open: parseFloat(data["1. open"]),
                high: parseFloat(data["2. high"]),
                low: parseFloat(data["3. low"]),
                close: parseFloat(data["4. close"]),
                volume: parseFloat(data["5. volume"])
            }))
            setIWMVolumeData(volumeData)
        } catch (error) {
            console.error(error)
        }
    }


    async function saveSPYData () {
        for (const items of data[0]) {
          await saveSPYDataOneAtATime(items)
        }
      }

    async function saveQQQData () {
      for (const items of data[1]) {
        await saveQQQDataOneAtATime(items)
      }
    }

    async function saveIWMData () {
        for (const items of data[2]) {
            await saveIWMDataOneAtATime(items)
        }
    }

    async function saveSPYDataOneAtATime(items) {
        const url = "http://localhost:8000/stocks/createSPY/"
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(items)
        }
        const response = await fetch(url, options)
        if (!response.ok) {
            throw new Error('Failed to submit SPY data')
        }
    }

    async function saveQQQDataOneAtATime(items) {
        const url = "http://localhost:8000/stocks/createQQQ/"
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(items)
        }
        const response = await fetch(url, options)
        if (!response.ok) {
            throw new Error('Failed to submit SPY data')
        }
    }

    async function saveIWMDataOneAtATime(items) {
        const url = "http://localhost:8000/stocks/createIWM/"
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(items)
        }
        const response = await fetch(url, options)
        if (!response.ok) {
            throw new Error('Failed to submit SPY data')
        }
    }

    // const mergeData = () => {
    //     const mergedData = {
    //         SPY: [],
    //         QQQ: [],
    //         IWM: []
    //     };

    //     spyVolumeData.forEach(spyData => {
    //         mergedData.SPY.push({
    //             open: spyData.open,
    //             close: spyData.close,
    //             high: spyData.high,
    //             low: spyData.low,
    //             timestamp: spyData.timestamp,
    //             volume: spyData.volume
    //         });
    //     });

    //     qqqVolumeData.forEach(qqqData => {
    //         mergedData.QQQ.push({
    //             open: qqqData.open,
    //             close: qqqData.close,
    //             high: qqqData.high,
    //             low: qqqData.low,
    //             timestamp: qqqData.timestamp,
    //             volume: qqqData.volume
    //         });
    //     });

    //     iwmVolumeData.forEach(iwmData => {
    //         mergedData.IWM.push({
    //             open: iwmData.open,
    //             close: iwmData.close,
    //             high: iwmData.high,
    //             low: iwmData.low,
    //             timestamp: iwmData.timestamp,
    //             volume: iwmData.volume
    //         });
    //     });

    //     // Convert mergedData to an array of objects
    //     const combinedVolumeData = Object.entries(mergedData).map(([symbol, data]) => ({ [symbol]: data }));

    //     setCombinedVolumeData(combinedVolumeData);
    // };

    const data = [
        spyVolumeData,
        qqqVolumeData,
        iwmVolumeData
    ]




    // useEffect(()=>{fetchData()}, [])
    const handleApiCall = () => {
        fetchSpyData()
        fetchQQQData()
        fetchIWMData()
    }

    // const handleMerge = () => {
    //     mergeData()
    // }

    const purpleDotStyle = {
        width: '10px',
        height: '10px',
        backgroundColor: 'purple',
        borderRadius: '50%',
      };

      const blueDotStyle = {
        width: '10px',
        height: '10px',
        backgroundColor: 'blue',
        borderRadius: '50%',
      };

      const redDotStyle = {
        width: '10px',
        height: '10px',
        backgroundColor: 'red',
        borderRadius: '50%',
      };


console.log("-----SPYvolumeData--------", spyVolumeData)
console.log("-----QQQvolumeData--------", qqqVolumeData)
console.log("-----IWMvolumeData--------", iwmVolumeData)
// console.log("-------combinedVolumeData-------", combinedVolumeData)
console.log("-----data--------", data)

    return (
        <>
        <div style={{marginBottom: '10%'}}>
        </div>
        <div className='d-flex justify-content-center'>
            <h1>Comparison: QQQ SPY IWM</h1>
        </div>
        <div className='d-flex justify-content-center'>
        <div style={purpleDotStyle}></div>
        <div><p>SPY</p></div>
        <div style={blueDotStyle}></div>
        <div><p>QQQ</p></div>
        <div style={redDotStyle}></div>
        <div><p>IWM</p></div>
        </div>
        <div className='d-flex justify-content-center'>
            <button onClick={() => handleApiCall()}>Click for Todays Info</button>
        </div>
        <div className='d-flex justify-content-center'>
        <div>
            <button onClick={() => {saveSPYData()}}>Click to save todays SPY Info</button>
        </div>
        <div>
            <button onClick={() => {saveQQQData()}}>Click to save todays QQQ Info</button>
        </div>
        <div>
            <button onClick={() => {saveIWMData()}}>Click to save todays IWM Info</button>
        </div>
        <div>
            {/* <button onClick={() => {handleMerge()}}>Compile</button> */}
        </div>
        </div>
        <div style={{ width: '100%' }}>
    <ResponsiveContainer width="100%" height={300}>
        <LineChart
            width={600}
            height={300}
            data={data}
            margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 5,
            }}
        >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis domain={[0,100]}allowDataOverflow = {false} />
            <YAxis />
            <Tooltip content={(props) => {
                if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
                const { payload } = props;
                const timestamp = new Date(payload[0].payload.timestamp); // Parse timestamp
                const formattedTimestamp = timestamp.toLocaleString(); // Format timestamp
                const openValue = payload[0].payload.open; // Get the open value
                const highValue = payload[0].payload.high; // Get the high value
                const lowValue = payload[0].payload.low; // Get the low value
                const closeValue = payload[0].payload.close; // Get the close value
                const volumeValue = payload[0].payload.volume; // Get the volume value
                return (
                    <div className="custom-tooltip" style={{ color: 'purple', padding: '5px' }}>
                        <p>SPY</p>
                        <p>{`Timestamp: ${formattedTimestamp}`}</p>
                        <p>{`High: ${highValue}`}</p>
                        <p>{`Open: ${openValue}`}</p>
                        <p>{`Close: ${closeValue}`}</p>
                        <p>{`Low: ${lowValue}`}</p>
                        {/* <p>{`Volume: ${volumeValue}`}</p> */}
                    </div>
                );
            }} />
            <Legend onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave} />

            <Line
                type="monotone"
                data={data[0]} // Set data directly
                dataKey='high' // Specify the data key
                strokeOpacity={opacity.SPY}
                stroke="purple"
                activeDot={{ r: 8 }}
            />

            <Line
                type="monotone"
                data={data[1]} // Set data directly
                dataKey='high' // Specify the data key
                strokeOpacity={opacity.QQQ}
                stroke="blue"
            />

            <Line
                type="monotone"
                data={data[2]} // Set data directly
                dataKey={'high'} // Specify the data key
                strokeOpacity={opacity.IWM}
                stroke="red"
            />



        </LineChart>
    </ResponsiveContainer>
    <p className="notes">Hover the Legend for the line you want REMOVED (for comparison of the other two)!</p>
</div>

        </>
    );

}


export default Compare3Stocks






// const data = [
    //     {
    //       name: 'Page A',
    //       QQQ: 4000,
    //       SPY: 2400,
    //       IWM: 2400,
    //     },
    //     {
    //       name: 'Page B',
    //       QQQ: 3000,
    //       SPY: 1398,
    //       IWM: 2210,
    //     },
    //     {
    //       name: 'Page C',
    //       QQQ: 2000,
    //       SPY: 9800,
    //       IWM: 2290,
    //     },
    //     {
    //       name: 'Page D',
    //       QQQ: 2780,
    //       SPY: 3908,
    //       IWM: 2000,
    //     },
    //     {
    //       name: 'Page E',
    //       QQQ: 1890,
    //       SPY: 4800,
    //       IWM: 2181,
    //     },
    //     {
    //       name: 'Page F',
    //       QQQ: 2390,
    //       SPY: 3800,
    //       IWM: 2500,
    //     },
    //     {
    //       name: 'Page G',
    //       QQQ: 3490,
    //       SPY: 4300,
    //       IWM: 2100,
    //     },
    //   ];
