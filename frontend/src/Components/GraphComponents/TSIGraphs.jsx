import { useState } from 'react'
import { Bar, BarChart, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';



function TSIGraphs () {

    // const initialValue = [0,1,2,3,1,2,2,3,4,5,6,7,7,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,3,4,5,6,7,8,9,0,9,8,7,6,5,4,3,2,1,2,3,4,5,6,7,8,9,0,9,8,7,6,5,4,3,2,1,2,3,4,5,6,7,8,9,0,9,8,7,6,5,4,3,2,1,2,3,4,5,6,7,8,9,0,9,8,7,6,5,4,3,2,1,2,3,4,5,6,7,8,9,0,9,8,7,6,5,4,3,2,1]
    const [volumeData, setVolumeData] = useState([]);
    const [date, setDate] = useState('')

    const xOpenData = volumeData.map(item => item.open);
    const yOpenData = volumeData.map(item => item.open);
    const xOpenMin = Math.min(...xOpenData);
    const xOpenMax = Math.max(...xOpenData);
    const yOpenMin = Math.min(...yOpenData);
    const yOpenMax = Math.max(...yOpenData);

    const xHighData = volumeData.map(item => item.high);
    const yHighData = volumeData.map(item => item.high);
    const xHighMin = Math.min(...xHighData);
    const xHighMax = Math.max(...xHighData);
    const yHighMin = Math.min(...yHighData);
    const yHighMax = Math.max(...yHighData);

    const xLowData = volumeData.map(item => item.low);
    const yLowData = volumeData.map(item => item.low);
    const xLowMin = Math.min(...xLowData);
    const xLowMax = Math.max(...xLowData);
    const yLowMin = Math.min(...yLowData);
    const yLowMax = Math.max(...yLowData);

    const xCloseData = volumeData.map(item => item.close);
    const yCloseData = volumeData.map(item => item.open);
    const xCloseMin = Math.min(...xCloseData);
    const xCloseMax = Math.max(...xCloseData);
    const yCloseMin = Math.min(...yCloseData);
    const yCloseMax = Math.max(...yCloseData);

    const xVolumeData = volumeData.map(item => item.volume);
    const yVolumeData = volumeData.map(item => item.volume);
    const xVolumeMin = Math.min(...xVolumeData);
    const xVolumeMax = Math.max(...xVolumeData);
    const yVolumeMin = Math.min(...yVolumeData);
    const yVolumeMax = Math.max(...yVolumeData);



    async function saveData (volumeData) {
        console.log("volumeData-------", volumeData)
        for (const items of volumeData) {
          await saveDataOneAtATime(items)
        }
        alert("Save Successful!")
      }

    async function saveDataOneAtATime(items) {
        console.log("items-------", items)
        const url = "http://localhost:8000/stocks/createTsi/"
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(items)
        }
        const response = await fetch(url, options)
        if (!response.ok) {
            throw new Error('Failed to submit tsi data')
        }
    }


    const fetchData = async () => {
        const url = 'https://alpha-vantage.p.rapidapi.com/query?interval=5min&function=TIME_SERIES_INTRADAY&symbol=MSFT&datatype=json&output_size=compact';
        const options = {
            method: 'GET',
            headers: {
                'X-RapidAPI-Key': 'bf0635dbcemshde30513e0b8df0cp129e7djsn9f83ff38a9b3',
                'X-RapidAPI-Host': 'alpha-vantage.p.rapidapi.com'
            }
        }
        try {
            const response = await fetch(url, options);
            const result = await response.json();
            alert("You successfully made an API call!")
            console.log("RESULT", result)
            const volumeData = Object.entries(result["Time Series (5min)"]).map(([timestamp, data]) => ({
                timestamp,
                open: parseFloat(data["1. open"]),
                high: parseFloat(data["2. high"]),
                low: parseFloat(data["3. low"]),
                close: parseFloat(data["4. close"]),
                volume: parseFloat(data["5. volume"])
            }))
            setVolumeData(volumeData)
        } catch (error) {
            console.error(error)
        }
    }

    async function handleAPICall () {
        fetchData()
    }

    const handleDate = (event) => {
        const value = event.target.value
        setDate(value)
    }

    console.log("volume DATA", volumeData)

    return(
        <>
        <div style={{ paddingTop: '200px' }}></div>
        <div className="d-flex justify-content-center align-items-center" style={{ height: '10vh', marginBottom: '120px' }}>
            <div className="w-75 text-center">
                <h1 className="display-1">Time Series IntraDaily Graphs and Table for MSFT (Microsoft corp)</h1>
            </div>
        </div>
        <div>
            <button onClick={() => {handleAPICall()}}>Click to retrieve todays Info</button>
        </div>
        <div>
            <button onClick={() => {saveData(volumeData)}}>Click to save todays Info</button>
        </div>


        <div className="row">
            <div className="col-md-6">
                <div className="card">
                    <h2 className="card-header text-success">Open</h2>
                    <div className="card-body" style={{ backgroundColor: 'rgba(10, 25, 15, 0.5)' }}>
                    <ResponsiveContainer width="100%" height={250}>
                        <LineChart
                            width={600}
                            height={300}
                            data={volumeData}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="100 1" stroke="#888" />
                            <XAxis domain={[xOpenMin, xOpenMax]} />
                            <YAxis domain={[yOpenMin, yOpenMax]}/>
                            <Tooltip content={(props) => {
                                if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
                                const { payload } = props;
                                const timestamp = new Date(payload[0].payload.timestamp); // Parse timestamp
                                const formattedTimestamp = timestamp.toLocaleString(); // Format timestamp
                                const openValue = payload[0].payload.open; // Get the open value
                                const highValue = payload[0].payload.high; // Get the high value
                                const lowValue = payload[0].payload.low; // Get the low value
                                const closeValue = payload[0].payload.close; // Get the close value
                                const volumeValue = payload[0].payload.volume; // Get the volume value
                                return (
                                    <div className="custom-tooltip" style={{ color: 'green', padding: '5px' }}>
                                        <p>{`Timestamp: ${formattedTimestamp}`}</p>
                                        <p>{`Open: ${openValue}`}</p>
                                        <p>{`High: ${highValue}`}</p>
                                        <p>{`Low: ${lowValue}`}</p>
                                        <p>{`Close: ${closeValue}`}</p>
                                        <p>{`Volume: ${volumeValue}`}</p>
                                    </div>
                                );
                            }} />
                            <Legend content={<div style={{ color: 'green' }}>Open: Starting price of MSFT stock within the time period.</div>} />
                            <Line type="monotone" dataKey="open" stroke="green" activeDot={{ r: 8 }} />
                        </LineChart>
                        </ResponsiveContainer>
                    </div>
                </div>
            </div>


            <div className="col-md-6">
                <div className="card">
                    <h2 className="card-header text-danger">Close</h2>
                    <div className="card-body" style={{ backgroundColor: 'rgba(10, 25, 15, 0.5)' }}>
                    <ResponsiveContainer width="100%" height={250}>
                        <LineChart
                            width={600}
                            height={300}
                            data={volumeData}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="100 1" stroke="#888"/>
                            <XAxis domain={[xCloseMin, xCloseMax]} />
                            <YAxis domain={[yCloseMin, yCloseMax]}/>
                            <Tooltip content={(props) => {
                                if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
                                const { payload } = props;
                                const timestamp = new Date(payload[0].payload.timestamp); // Parse timestamp
                                const formattedTimestamp = timestamp.toLocaleString(); // Format timestamp
                                const openValue = payload[0].payload.open; // Get the open value
                                const highValue = payload[0].payload.high; // Get the high value
                                const lowValue = payload[0].payload.low; // Get the low value
                                const closeValue = payload[0].payload.close; // Get the close value
                                const volumeValue = payload[0].payload.volume; // Get the volume value
                                return (
                                    <div className="custom-tooltip" style={{ color: 'red', padding: '5px' }}>
                                        <p>{`Timestamp: ${formattedTimestamp}`}</p>
                                        <p>{`Open: ${openValue}`}</p>
                                        <p>{`High: ${highValue}`}</p>
                                        <p>{`Low: ${lowValue}`}</p>
                                        <p>{`Close: ${closeValue}`}</p>
                                        <p>{`Volume: ${volumeValue}`}</p>
                                    </div>
                                );
                            }} />
                            <Legend content={<div style={{ color: 'red' }}>Close: Final price of MSFT stock within the time period.</div>} />
                            <Line type="monotone" dataKey="close" stroke="red" />
                        </LineChart>
                        </ResponsiveContainer>
                    </div>
                </div>
            </div>

            <div className="row">
            <div className="col-md-6">
                <div className="card">
                    <h2 className="card-header text-primary">Low</h2>
                    <div className="card-body" style={{ backgroundColor: 'rgba(10, 25, 15, 0.5)' }}>
                    <ResponsiveContainer width="100%" height={250}>
                        <LineChart
                            width={600}
                            height={300}
                            data={volumeData}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="100 1" stroke="#888"/>
                            <XAxis domain={[xLowMin, xLowMax]} />
                            <YAxis domain={[yLowMin, yLowMax]}/>
                            <Tooltip content={(props) => {
                                if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
                                const { payload } = props;
                                const timestamp = new Date(payload[0].payload.timestamp); // Parse timestamp
                                const formattedTimestamp = timestamp.toLocaleString(); // Format timestamp
                                const openValue = payload[0].payload.open; // Get the open value
                                const highValue = payload[0].payload.high; // Get the high value
                                const lowValue = payload[0].payload.low; // Get the low value
                                const closeValue = payload[0].payload.close; // Get the close value
                                const volumeValue = payload[0].payload.volume; // Get the volume value
                                return (
                                    <div className="custom-tooltip" style={{ color: 'blue', padding: '5px' }}>
                                        <p>{`Timestamp: ${formattedTimestamp}`}</p>
                                        <p>{`Open: ${openValue}`}</p>
                                        <p>{`High: ${highValue}`}</p>
                                        <p>{`Low: ${lowValue}`}</p>
                                        <p>{`Close: ${closeValue}`}</p>
                                        <p>{`Volume: ${volumeValue}`}</p>
                                    </div>
                                );
                            }} />
                            <Legend content={<div style={{ color: 'blue' }}>Low: Minimum price of MSFT stock within the time period.</div>} />
                            <Line type="monotone" dataKey="low" stroke="blue" activeDot={{ r: 8 }} />
                        </LineChart>
                        </ResponsiveContainer>
                    </div>
                </div>
            </div>


                <div className="col-md-6">
                    <div className="card">
                        <h2 className="card-header">High</h2>
                        <div className="card-body" style={{ backgroundColor: 'rgba(10, 25, 15, 0.5)' }}>
                        <ResponsiveContainer width="100%" height={250}>
                            <LineChart
                                width={600}
                                height={300}
                                data={volumeData}
                                margin={{
                                    top: 5,
                                    right: 30,
                                    left: 20,
                                    bottom: 5,
                                }}
                            >
                                <CartesianGrid strokeDasharray="100 1" stroke="#888"/>
                                <XAxis domain={[xHighMin, xHighMax]} />
                                <YAxis domain={[yHighMin, yHighMax]} />
                                <Tooltip content={(props) => {
                                    if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
                                    const { payload } = props;
                                    const timestamp = new Date(payload[0].payload.timestamp); // Parse timestamp
                                    const formattedTimestamp = timestamp.toLocaleString(); // Format timestamp
                                    const openValue = payload[0].payload.open; // Get the open value
                                    const highValue = payload[0].payload.high; // Get the high value
                                    const lowValue = payload[0].payload.low; // Get the low value
                                    const closeValue = payload[0].payload.close; // Get the close value
                                    const volumeValue = payload[0].payload.volume; // Get the volume value
                                    return (
                                        <div className="custom-tooltip" style={{ color: 'black', padding: '5px' }}>
                                            <p>{`Timestamp: ${formattedTimestamp}`}</p>
                                            <p>{`Open: ${openValue}`}</p>
                                            <p>{`High: ${highValue}`}</p>
                                            <p>{`Low: ${lowValue}`}</p>
                                            <p>{`Close: ${closeValue}`}</p>
                                            <p>{`Volume: ${volumeValue}`}</p>
                                        </div>
                                    );
                                }} />
                                <Legend content={<div style={{ color: 'black' }}>High: Maximum price of MSFT stock within the time period.</div>} />
                                <Line type="monotone" dataKey="high" stroke="black" activeDot={{ r: 8 }} />
                            </LineChart>
                            </ResponsiveContainer>
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <div>
            <h1 style={{ color: 'teal' }}>
                Volume
            </h1>
        </div>
        <div className="card-body" style={{ backgroundColor: 'rgba(10, 25, 15, 0.5)', marginBottom: "100px" }}>
        <ResponsiveContainer width="100%" height={250}>
            <BarChart
                width={1300}
                height={300}
                data={volumeData}
                margin={{
                    top: 5,
                    right: 30,
                    left: 20,
                    bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="100 1" stroke="#888"/>
                <XAxis domain={[xVolumeMin, xVolumeMax]} />
                <YAxis domain={[yVolumeMin, yVolumeMax]}/>
                <Tooltip content={(props) => {
                                if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
                                const { payload } = props;
                                const timestamp = new Date(payload[0].payload.timestamp); // Parse timestamp
                                const formattedTimestamp = timestamp.toLocaleString(); // Format timestamp
                                const openValue = payload[0].payload.open; // Get the open value
                                const highValue = payload[0].payload.high; // Get the high value
                                const lowValue = payload[0].payload.low; // Get the low value
                                const closeValue = payload[0].payload.close; // Get the close value
                                const volumeValue = payload[0].payload.volume; // Get the volume value
                                return (
                                    <div className="custom-tooltip" style={{ color: 'teal', padding: '5px' }}>
                                        <p>{`Timestamp: ${formattedTimestamp}`}</p>
                                        <p>{`Open: ${openValue}`}</p>
                                        <p>{`High: ${highValue}`}</p>
                                        <p>{`Low: ${lowValue}`}</p>
                                        <p>{`Close: ${closeValue}`}</p>
                                        <p>{`Volume: ${volumeValue}`}</p>
                                    </div>
                                );
                            }} />
                <Legend content={<div style={{ color: 'teal' }}>Volume: Total number of MSFT shares traded within the period.</div>} />
                <Bar dataKey="volume" fill="teal" />
            </BarChart>
            </ResponsiveContainer>
        </div>
        <div className="container d-flex flex-column align-items-center justify-content">
            <h4>Search by time:</h4>
            <input onChange={handleDate}/>
            <p>HH:MM</p>
        </div>
        <div className="container d-flex flex-column align-items-center justify-content" style={{ height: '100vh' }}>
            <div>
                <h1 style={{ color: 'black' }}>Table of <span style={{ color: 'teal' }}>VOLUME</span> Data for MSFT</h1>
                <table className="container d-flex flex-column align-items-center justify-content">
                    <thead>
                        <tr>
                            <th >TimeStamp</th>
                            <th style={{ color: 'teal' }}>Volume</th>
                        </tr>
                    </thead>
                    <tbody>
                        {volumeData.filter(volume => date === '' || volume.timestamp.includes(date)).map(volume => (
                            <tr key={volume.timestamp}>
                            <td className="container d-flex flex-column align-items-center justify-content">
                                {new Date(volume.timestamp).toLocaleString('en-US', {
                                    year: 'numeric',
                                    month: 'short',
                                    day: '2-digit',
                                    hour: '2-digit',
                                    minute: '2-digit',
                                    second: '2-digit',
                                    hour12: false
                                })}
                            </td>
                            <td style={{ color: 'teal' }}>{volume.volume}</td>
                        </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
        </>
    )
}

export default TSIGraphs
