import React, { useState, useEffect } from "react";
import { Card, CardContent, CardHeader, Divider } from "@mui/material";
import Chart from "react-apexcharts";
import { ResponsiveContainer, Tooltip } from "recharts";
import ViewAllNotes from '../NotesComponents/ViewAllNotes'
import IWMNotes from "../NotesComponents/IWMNotes";
import SPYNotes from "../NotesComponents/SPYNotes";
import QQQNotes from "../NotesComponents/QQQNotes";
import MSFTNotes from "../NotesComponents/MSFTNotes";
import Notes from "../NotesComponents/Notes";

function MultiViewPage () {


    const fetchData = async () => {
        fetchIWMData()
        fetchQQQData()
        fetchSPYData()
        fetchTsiData()
    }

    useEffect(() => {
        fetchData();
    }, []);

    const [qqqData, setQQQData] = useState([]);
    const [tsiData, setTSIData] = useState([]);
    const [spyData, setSPYData] = useState([]);
    const [iwmData, setIWMData] = useState([]);
    const [options, setOptions] = useState({
        xaxis: {
            type: "datetime",
        },
        tooltip: {
            x: {
                format: 'dd MMM yyyy'
            },
            y: {
                formatter: function(value) {
                    return '$' + value.toFixed(2);
                }
            }
        },
    });


    const fetchQQQData = async () => {
        const urlQQQ = "http://localhost:8000/stocks/getQQQ/";
        try {
            const response = await fetch(urlQQQ);
            if (response.ok) {
                const result = await response.json();
                const parsedData = result['QQQ'].map((data) => ({
                    timestamp: data.timestamp,
                    open: parseFloat(data["open"]),
                    high: parseFloat(data["high"]),
                    low: parseFloat(data["low"]),
                    close: parseFloat(data["close"]),
                    volume: parseFloat(data["volume"])
                })).reverse();
                setQQQData(parsedData);

                // Extract timestamps and update options state
                const timestamps = await extractTimestamps(parsedData);
                setOptions(prevOptions => ({
                    ...prevOptions,
                    xaxis: {
                        ...prevOptions.xaxis,
                        categories: timestamps
                    }
                }));
            }
        } catch (error) {
            console.log("error!", error);
        }
    };

    const fetchTsiData = async () => {
        const urlTsi = "http://localhost:8000/stocks/getTsi/";
        try {
            const response = await fetch(urlTsi);
            if (response.ok) {
                const result = await response.json();
                const parsedData = result['tsi'].map((data) => ({
                    timestamp: data.timestamp,
                    open: parseFloat(data["open"]),
                    high: parseFloat(data["high"]),
                    low: parseFloat(data["low"]),
                    close: parseFloat(data["close"]),
                    volume: parseFloat(data["volume"])
                })).reverse();
                setTSIData(parsedData);

                // Extract timestamps and update options state
                // const timestamps = await extractTimestamps(parsedData);
                // setOptions(prevOptions => ({
                //     ...prevOptions,
                //     xaxis: {
                //         ...prevOptions.xaxis,
                //         categories: timestamps
                //     }
                // })
                // );
            }
        } catch (error) {
            console.log("error!", error);
        }
    };

    const fetchSPYData = async () => {
        const urlSPY = "http://localhost:8000/stocks/getSPY/";
        try {
            const response = await fetch(urlSPY);
            if (response.ok) {
                const result = await response.json();
                const parsedData = result['SPY'].map((data) => ({
                    timestamp: data.timestamp,
                    open: parseFloat(data["open"]),
                    high: parseFloat(data["high"]),
                    low: parseFloat(data["low"]),
                    close: parseFloat(data["close"]),
                    volume: parseFloat(data["volume"])
                })).reverse();
                setSPYData(parsedData);

                // Extract timestamps and update options state
                // const timestamps = await extractTimestamps(parsedData);
                // setOptions(prevOptions => ({
                //     ...prevOptions,
                //     xaxis: {
                //         ...prevOptions.xaxis,
                //         categories: timestamps
                //     }
                // }));
            }
        } catch (error) {
            console.log("error!", error);
        }
    };

    const fetchIWMData = async () => {
        const urlIWM = "http://localhost:8000/stocks/getIWM/";
        try {
            const response = await fetch(urlIWM);
            if (response.ok) {
                const result = await response.json();
                const parsedData = result['IWM'].map((data) => ({
                    timestamp: data.timestamp,
                    open: parseFloat(data["open"]),
                    high: parseFloat(data["high"]),
                    low: parseFloat(data["low"]),
                    close: parseFloat(data["close"]),
                    volume: parseFloat(data["volume"])
                })).reverse();
                setIWMData(parsedData);

                // Extract timestamps and update options state
                // const timestamps = await extractTimestamps(parsedData);
                // setOptions(prevOptions => ({
                //     ...prevOptions,
                //     xaxis: {
                //         ...prevOptions.xaxis,
                //         categories: timestamps
                //     }
                // }));
            }
        } catch (error) {
            console.log("error!", error);
        }
    };

    const handleGetInfo = async () => {
        await fetchQQQData();
        await fetchTsiData();
        await fetchSPYData();
        await fetchIWMData();
    };

    const extractTimestamps = async (data) => {
        const timestamps = data.map(item => item.timestamp);
        return timestamps;
    };




    return (
        <>
            <div style={{ marginBottom: "1%" }}></div>
            {/* <div>
                <button onClick={handleGetInfo}>Click for Info</button>
            </div> */}
            <div>
            <div>
                <h3 style={{marginBottom:'2%'}} className="d-flex justify-content-center">All Data</h3>
            </div>
            {options && <div>
                <Card>
                    <CardHeader title="QQQ" />
                    <Divider />
                    <CardContent>
                        <ResponsiveContainer width="100%" height={200}>
                            <Chart
                                options={options}
                                series={[{ data: qqqData.map(item => ({ x: new Date(item.timestamp).getTime(), y: [item.open, item.high, item.low, item.close] })) }]}
                                type="candlestick"
                                width={500}
                                height={200}
                            />
                        </ResponsiveContainer>
                    </CardContent>
                </Card>
                </div>
            }
            </div>
            <div>
                <QQQNotes />
            </div>

            <div>
            {options && <div>
                <Card>
                    <CardHeader title="SPY" />
                    <Divider />
                    <CardContent>
                        <ResponsiveContainer width="100%" height={200}>
                            <Chart
                                options={options}
                                series={[{ data: spyData.map(item => ({ x: new Date(item.timestamp).getTime(), y: [item.open, item.high, item.low, item.close] })) }]}
                                type="candlestick"
                                width={500}
                                height={200}
                            />
                        </ResponsiveContainer>
                    </CardContent>
                </Card>
            </div>}
            </div>
            <div>
                <SPYNotes />
            </div>

            <div>
            {options && <div>
                <Card>
                    <CardHeader title="IWM" />
                    <Divider />
                    <CardContent>
                        <ResponsiveContainer width="100%" height={200}>
                            <Chart
                                options={options}
                                series={[{ data: iwmData.map(item => ({ x: new Date(item.timestamp).getTime(), y: [item.open, item.high, item.low, item.close] })) }]}
                                type="candlestick"
                                width={500}
                                height={200}
                            />
                        </ResponsiveContainer>
                    </CardContent>
                </Card>
            </div>}
            </div>
            <div>
                <IWMNotes />
            </div>

            <div>
            {options && <div>
                <Card>
                    <CardHeader title="MSFT (Microsoft corp)" />
                    <Divider />
                    <CardContent>
                        <ResponsiveContainer width="100%" height={200}>
                            <Chart
                                options={options}
                                series={[{ data: tsiData.map(item => ({ x: new Date(item.timestamp).getTime(), y: [item.open, item.high, item.low, item.close] })) }]}
                                type="candlestick"
                                width={500}
                                height={200}
                            />
                        </ResponsiveContainer>
                    </CardContent>
                </Card>
            </div>}
            </div>
            <div>
                <MSFTNotes />
            </div>

        </>
    );
}


export default MultiViewPage
