import { useState, useEffect } from 'react'
import { Bar, BarChart, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';



function TSIGraphInteractive (dataChoice) {

    // const initialValue = [0,1,2,3,1,2,2,3,4,5,6,7,7,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,3,4,5,6,7,8,9,0,9,8,7,6,5,4,3,2,1,2,3,4,5,6,7,8,9,0,9,8,7,6,5,4,3,2,1,2,3,4,5,6,7,8,9,0,9,8,7,6,5,4,3,2,1,2,3,4,5,6,7,8,9,0,9,8,7,6,5,4,3,2,1,2,3,4,5,6,7,8,9,0,9,8,7,6,5,4,3,2,1]
    const [volumeData, setVolumeData] = useState([]);
    const [date, setDate] = useState('')
    const [graphClick, setGraphClick] = useState({timestamp: null, openValue: null , highValue: null, lowValue: null, closeValue: null, volumeValue: null})
    const [firstClickData, setFirstClickData] = useState(null);
    const [secondClickData, setSecondClickData] = useState(null);
    const [selectedPoints, setSelectedPoints] = useState([])

    const xOpenData = volumeData.map(item => item.open);
    const yOpenData = volumeData.map(item => item.open);
    const xOpenMin = Math.min(...xOpenData);
    const xOpenMax = Math.max(...xOpenData);
    const yOpenMin = Math.min(...yOpenData);
    const yOpenMax = Math.max(...yOpenData);

    const xHighData = volumeData.map(item => item.high);
    const yHighData = volumeData.map(item => item.high);
    const xHighMin = Math.min(...xHighData);
    const xHighMax = Math.max(...xHighData);
    const yHighMin = Math.min(...yHighData);
    const yHighMax = Math.max(...yHighData);

    const xLowData = volumeData.map(item => item.low);
    const yLowData = volumeData.map(item => item.low);
    const xLowMin = Math.min(...xLowData);
    const xLowMax = Math.max(...xLowData);
    const yLowMin = Math.min(...yLowData);
    const yLowMax = Math.max(...yLowData);

    const xCloseData = volumeData.map(item => item.close);
    const yCloseData = volumeData.map(item => item.open);
    const xCloseMin = Math.min(...xCloseData);
    const xCloseMax = Math.max(...xCloseData);
    const yCloseMin = Math.min(...yCloseData);
    const yCloseMax = Math.max(...yCloseData);

    const xVolumeData = volumeData.map(item => item.volume);
    const yVolumeData = volumeData.map(item => item.volume);
    const xVolumeMin = Math.min(...xVolumeData);
    const xVolumeMax = Math.max(...xVolumeData);
    const yVolumeMin = Math.min(...yVolumeData);
    const yVolumeMax = Math.max(...yVolumeData);

    // Calculate the domain for XAxis based on selected points
    const xDomain = selectedPoints.length === 2
        ? [selectedPoints[0].timestamp, selectedPoints[1].timestamp]
        : [0, volumeData.length - 1];



    const separateGet = () => {
        if (dataChoice.dataChoice.startsWith('get')) {
            let remaining = dataChoice.dataChoice.slice('get'.length).trim();
            return remaining;
        } else {
            return null;
        }
        }


    const fetchData = async () => {
        const url = `http://localhost:8000/stocks/${dataChoice.dataChoice}/`;
        try {
            const response = await fetch(url);
            if (response.ok) {
                const result = await response.json();
                let dataSelection = separateGet()
                    if (dataSelection === 'Tsi'){
                    dataSelection = 'tsi'
                    }

                const parsedData = result[dataSelection].map((data) => ({
                    timestamp: data.timestamp,
                    open: parseFloat(data["open"]),
                    high: parseFloat(data["high"]),
                    low: parseFloat(data["low"]),
                    close: parseFloat(data["close"]),
                    volume: parseFloat(data["volume"])
                }));
                setVolumeData(parsedData);

                // Extract timestamps and update options state
                // const timestamps = await extractTimestamps(parsedData);
                // setOptions(prevOptions => ({
                //     ...prevOptions,
                //     xaxis: {
                //         ...prevOptions.xaxis,
                //         categories: timestamps
                //     }
                // }));
            }
        } catch (error) {
            console.log("error!", error);
        }
    };

    useEffect(()=>{fetchData()},[dataChoice])

    async function handleAPICall () {
        fetchData()
    }

    const handleDate = (event) => {
        const value = event.target.value
        setDate(value)
    }

    const handleReset = () => {
        setFirstClickData(null)
        setSecondClickData(null)
    }

    const handleGraphClick = (timestamp, openValue, highValue, lowValue, closeValue, volumeValue) => {
        setGraphClick({ timestamp, openValue, highValue, lowValue, closeValue, volumeValue })
        if (firstClickData === null) {
            setFirstClickData({ timestamp, openValue, highValue, lowValue, closeValue, volumeValue })
        } else {
            setSecondClickData({ timestamp, openValue, highValue, lowValue, closeValue, volumeValue })
        }
        if (selectedPoints.length < 2) {
            setSelectedPoints([...selectedPoints, { timestamp, openValue, highValue, lowValue, closeValue, volumeValue }]);
        } else {
            setSelectedPoints([{ timestamp, openValue, highValue, lowValue, closeValue, volumeValue }]);
        }
    }

    console.log("volume DATA", volumeData)
    console.log("GRAPH CLICK--------------------:", graphClick)
    console.log("first click", firstClickData)
    console.log("second click", secondClickData)

    return(
        <>
        {/* <div style={{ paddingTop: '200px' }}></div> */}
        <div className="d-flex justify-content-center align-items-center" style={{ marginBottom: '1%'}}>
        </div>
        {/* <div>
            <button onClick={() => {handleAPICall()}}>Click to retrieve todays Info</button>
        </div> */}
        <div>
            <h3 style={{marginBottom:'2%'}} className="d-flex justify-content-center">All Data {(dataChoice && separateGet())}</h3>
        </div>

        <div className="row" style={{marginBottom:"2%"}}>
            <div className="col-md-6">
                <div className="card">
                    <h2 className="card-header text-success">Open</h2>
                    <div className="card-body" style={{ backgroundColor: 'rgba(10, 25, 15, 0.5)' }}>
                    <ResponsiveContainer width="100%" height={250}>
                        <LineChart
                            width={600}
                            height={300}
                            data={volumeData}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                            onClick={(event) => {
                                const timestamp = event.activePayload[0].payload.timestamp
                                const openValue = event.activePayload[0].payload.open
                                const highValue = event.activePayload[0].payload.high
                                const lowValue = event.activePayload[0].payload.low
                                const closeValue = event.activePayload[0].payload.close
                                const volumeValue = event.activePayload[0].payload.volume
                                handleGraphClick( timestamp, openValue, highValue, lowValue, closeValue, volumeValue )
                             }}
                        >
                            <CartesianGrid strokeDasharray="100 1" stroke="#888" />
                            <XAxis domain={[xDomain]} />
                            <YAxis domain={[yOpenMin, yOpenMax]}/>
                            <Tooltip content={(props) => {
                                if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
                                const { payload } = props;
                                const timestamp = new Date(payload[0].payload.timestamp);
                                const formattedTimestamp = timestamp.toLocaleString();
                                const openValue = payload[0].payload.open;
                                const highValue = payload[0].payload.high;
                                const lowValue = payload[0].payload.low;
                                const closeValue = payload[0].payload.close;
                                const volumeValue = payload[0].payload.volume;
                                return (
                                    <div className="custom-tooltip" style={{ color: 'green', padding: '5px' }}>
                                            <p>{`${formattedTimestamp}`}</p>
                                            <p>{`Open: ${openValue}`}</p>
                                            {/* <p>{`High: ${highValue}`}</p>
                                            <p>{`Low: ${lowValue}`}</p>
                                            <p>{`Close: ${closeValue}`}</p>
                                            <p>{`Volume: ${volumeValue}`}</p> */}
                                    </div>
                                );
                            }} />
                            <Legend content={<div style={{ color: 'green' }}>Open: Starting price of MSFT stock within the time period.</div>} />
                            <Line type="monotone" dataKey="open" stroke="green" activeDot={{ r: 8 }} />
                        </LineChart>
                    </ResponsiveContainer>
                    </div>


                    <div style={{ display: 'flex' }}>
                        <div style={{ marginRight: '1%' }}>
                            <table>
                                <thead>
                                    <th><button onClick={()=>{handleReset()}}>Reset</button></th>
                                    <th>Time</th>
                                    <th>Open</th>
                                    <th>High</th>
                                    <th>Low</th>
                                    <th>Close</th>
                                    <th>Volume</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        {firstClickData && <th>HOLD</th>}
                                        {firstClickData && <td>{new Date(firstClickData.timestamp).toLocaleTimeString()}</td>}
                                        {firstClickData && <td>{firstClickData.openValue}</td>}
                                        {firstClickData && <td>{firstClickData.highValue}</td>}
                                        {firstClickData && <td>{firstClickData.lowValue}</td>}
                                        {firstClickData && <td>{firstClickData.closeValue}</td>}
                                        {firstClickData && <td>{firstClickData.volume}</td>}
                                    </tr>
                                    <tr>
                                        {secondClickData && <th>COMPARISON</th>}
                                        {secondClickData && <td style={{color:'red'}}>{new Date(secondClickData.timestamp).toLocaleTimeString()}</td>}
                                        {secondClickData && <td>{secondClickData.openValue}</td>}
                                        {secondClickData && <td>{secondClickData.highValue}</td>}
                                        {secondClickData && <td>{secondClickData.lowValue}</td>}
                                        {secondClickData && <td>{secondClickData.closeValue}</td>}
                                        {secondClickData && <td>{secondClickData.volume}</td>}
                                    </tr>
                                    <tr>
                                        <td>DIFFERENCE</td>
                                        <td>N/A</td>
                                        {/* <td>{secondClickData && (Math.abs((new Date(firstClickData.timestamp).getTime()) - (new Date(secondClickData.timestamp).getTime() ) ) / (1000*60)) }</td> */}
                                        <td>{secondClickData && ((firstClickData.openValue - secondClickData.openValue) * -1).toFixed(3)}</td>
                                        <td>{secondClickData && ((firstClickData.highValue - secondClickData.highValue) * -1).toFixed(3)}</td>
                                        <td>{secondClickData && ((firstClickData.lowValue - secondClickData.lowValue) * -1).toFixed(3)}</td>
                                        <td>{secondClickData && ((firstClickData.closeValue - secondClickData.closeValue)* -1).toFixed(3)}</td>
                                        <td>{secondClickData && ((firstClickData.volumeValue - secondClickData.volumeValue) * -1).toFixed(1)}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>



            <div className="col-md-6">
                <div className="card">
                    <h2 className="card-header text-danger">Close</h2>
                    <div className="card-body" style={{ backgroundColor: 'rgba(10, 25, 15, 0.5)' }}>
                    <ResponsiveContainer width="100%" height={250}>
                        <LineChart
                            width={600}
                            height={300}
                            data={volumeData}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                            onClick={(event) => {
                                const timestamp = event.activePayload[0].payload.timestamp
                                const openValue = event.activePayload[0].payload.open
                                const highValue = event.activePayload[0].payload.high
                                const lowValue = event.activePayload[0].payload.low
                                const closeValue = event.activePayload[0].payload.close
                                const volumeValue = event.activePayload[0].payload.volume
                                handleGraphClick( timestamp, openValue, highValue, lowValue, closeValue, volumeValue )
                             }}
                        >
                            <CartesianGrid strokeDasharray="100 1" stroke="#888"/>
                            <XAxis domain={[xCloseMin, xCloseMax]} />
                            <YAxis domain={[yCloseMin, yCloseMax]}/>
                            <Tooltip content={(props) => {
                                if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
                                const { payload } = props;
                                const timestamp = new Date(payload[0].payload.timestamp); // Parse timestamp
                                const formattedTimestamp = timestamp.toLocaleString(); // Format timestamp
                                const openValue = payload[0].payload.open; // Get the open value
                                const highValue = payload[0].payload.high; // Get the high value
                                const lowValue = payload[0].payload.low; // Get the low value
                                const closeValue = payload[0].payload.close; // Get the close value
                                const volumeValue = payload[0].payload.volume; // Get the volume value
                                return (
                                    <div className="custom-tooltip" style={{ color: 'red', padding: '5px' }}>
                                        <p>{`${formattedTimestamp}`}</p>
                                        {/* <p>{`Open: ${openValue}`}</p>
                                        <p>{`High: ${highValue}`}</p>
                                        <p>{`Low: ${lowValue}`}</p> */}
                                        <p>{`Close: ${closeValue}`}</p>
                                        {/* <p>{`Volume: ${volumeValue}`}</p> */}
                                    </div>
                                );
                            }} />
                            <Legend content={<div style={{ color: 'red' }}>Close: Final price of MSFT stock within the time period.</div>} />
                            <Line type="monotone" dataKey="close" stroke="red" />
                        </LineChart>
                    </ResponsiveContainer>
                    </div>


                    <div style={{ display: 'flex' }}>
                        <div style={{ marginRight: '1%' }}>
                            <table>
                                <thead>
                                    <th><button onClick={()=>{handleReset()}}>Reset</button></th>
                                    <th>Time</th>
                                    <th>Open</th>
                                    <th>High</th>
                                    <th>Low</th>
                                    <th>Close</th>
                                    <th>Volume</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        {firstClickData && <th>HOLD</th>}
                                        {firstClickData && <td>{new Date(firstClickData.timestamp).toLocaleTimeString()}</td>}
                                        {firstClickData && <td>{firstClickData.openValue}</td>}
                                        {firstClickData && <td>{firstClickData.highValue}</td>}
                                        {firstClickData && <td>{firstClickData.lowValue}</td>}
                                        {firstClickData && <td>{firstClickData.closeValue}</td>}
                                        {firstClickData && <td>{firstClickData.volume}</td>}
                                    </tr>
                                    <tr>
                                        {secondClickData && <th>COMPARISON</th>}
                                        {secondClickData && <td style={{color:'red'}}>{new Date(secondClickData.timestamp).toLocaleTimeString()}</td>}
                                        {secondClickData && <td>{secondClickData.openValue}</td>}
                                        {secondClickData && <td>{secondClickData.highValue}</td>}
                                        {secondClickData && <td>{secondClickData.lowValue}</td>}
                                        {secondClickData && <td>{secondClickData.closeValue}</td>}
                                        {secondClickData && <td>{secondClickData.volume}</td>}
                                    </tr>
                                    <tr>
                                        <td>DIFFERENCE</td>
                                        <td>N/A</td>
                                        <td>{secondClickData && ((firstClickData.openValue - secondClickData.openValue) * -1).toFixed(3)}</td>
                                        <td>{secondClickData && ((firstClickData.highValue - secondClickData.highValue) * -1).toFixed(3)}</td>
                                        <td>{secondClickData && ((firstClickData.lowValue - secondClickData.lowValue) * -1).toFixed(3)}</td>
                                        <td>{secondClickData && ((firstClickData.closeValue - secondClickData.closeValue)* -1).toFixed(3)}</td>
                                        <td>{secondClickData && ((firstClickData.volumeValue - secondClickData.volumeValue) * -1).toFixed(1)}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div className="row">
            <div className="col-md-6">
                <div className="card">
                    <h2 className="card-header text-primary">Low</h2>
                    <div className="card-body" style={{ backgroundColor: 'rgba(10, 25, 15, 0.5)' }}>
                    <ResponsiveContainer width="100%" height={250}>
                        <LineChart
                            width={600}
                            height={300}
                            data={volumeData}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                            onClick={(event) => {
                                const timestamp = event.activePayload[0].payload.timestamp
                                const openValue = event.activePayload[0].payload.open
                                const highValue = event.activePayload[0].payload.high
                                const lowValue = event.activePayload[0].payload.low
                                const closeValue = event.activePayload[0].payload.close
                                const volumeValue = event.activePayload[0].payload.volume
                                handleGraphClick( timestamp, openValue, highValue, lowValue, closeValue, volumeValue )
                             }}
                        >
                            <CartesianGrid strokeDasharray="100 1" stroke="#888"/>
                            <XAxis domain={[xLowMin, xLowMax]} />
                            <YAxis domain={[yLowMin, yLowMax]}/>
                            <Tooltip content={(props) => {
                                if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
                                const { payload } = props;
                                const timestamp = new Date(payload[0].payload.timestamp); // Parse timestamp
                                const formattedTimestamp = timestamp.toLocaleString(); // Format timestamp
                                const openValue = payload[0].payload.open; // Get the open value
                                const highValue = payload[0].payload.high; // Get the high value
                                const lowValue = payload[0].payload.low; // Get the low value
                                const closeValue = payload[0].payload.close; // Get the close value
                                const volumeValue = payload[0].payload.volume; // Get the volume value
                                return (
                                    <div className="custom-tooltip" style={{ color: 'blue', padding: '5px' }}>
                                        <p>{`${formattedTimestamp}`}</p>
                                        {/* <p>{`Open: ${openValue}`}</p>
                                        <p>{`High: ${highValue}`}</p> */}
                                        <p>{`Low: ${lowValue}`}</p>
                                        {/* <p>{`Close: ${closeValue}`}</p>
                                        <p>{`Volume: ${volumeValue}`}</p> */}
                                    </div>
                                );
                            }} />
                            <Legend content={<div style={{ color: 'blue' }}>Low: Minimum price of MSFT stock within the time period.</div>} />
                            <Line type="monotone" dataKey="low" stroke="blue" activeDot={{ r: 8 }} />
                        </LineChart>
                    </ResponsiveContainer>
                    </div>
                    <div style={{ display: 'flex' }}>
                        <div style={{ marginRight: '1%' }}>
                            <table>
                                <thead>
                                    <th><button onClick={()=>{handleReset()}}>Reset</button></th>
                                    <th>Time</th>
                                    <th>Open</th>
                                    <th>High</th>
                                    <th>Low</th>
                                    <th>Close</th>
                                    <th>Volume</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        {firstClickData && <th>HOLD</th>}
                                        {firstClickData && <td>{new Date(firstClickData.timestamp).toLocaleTimeString()}</td>}
                                        {firstClickData && <td>{firstClickData.openValue}</td>}
                                        {firstClickData && <td>{firstClickData.highValue}</td>}
                                        {firstClickData && <td>{firstClickData.lowValue}</td>}
                                        {firstClickData && <td>{firstClickData.closeValue}</td>}
                                        {firstClickData && <td>{firstClickData.volume}</td>}
                                    </tr>
                                    <tr>
                                        {secondClickData && <th>COMPARISON</th>}
                                        {secondClickData && <td style={{color:'red'}}>{new Date(secondClickData.timestamp).toLocaleTimeString()}</td>}
                                        {secondClickData && <td>{secondClickData.openValue}</td>}
                                        {secondClickData && <td>{secondClickData.highValue}</td>}
                                        {secondClickData && <td>{secondClickData.lowValue}</td>}
                                        {secondClickData && <td>{secondClickData.closeValue}</td>}
                                        {secondClickData && <td>{secondClickData.volume}</td>}
                                    </tr>
                                    <tr>
                                        <td>DIFFERENCE</td>
                                        <td>N/A</td>
                                        <td>{secondClickData && ((firstClickData.openValue - secondClickData.openValue) * -1).toFixed(3)}</td>
                                        <td>{secondClickData && ((firstClickData.highValue - secondClickData.highValue) * -1).toFixed(3)}</td>
                                        <td>{secondClickData && ((firstClickData.lowValue - secondClickData.lowValue) * -1).toFixed(3)}</td>
                                        <td>{secondClickData && ((firstClickData.closeValue - secondClickData.closeValue)* -1).toFixed(3)}</td>
                                        <td>{secondClickData && ((firstClickData.volumeValue - secondClickData.volumeValue) * -1).toFixed(1)}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>



        <div className="col-md-6">
                <div className="card">
                    <h2 className="card-header">High</h2>
                    <div className="card-body" style={{ backgroundColor: 'rgba(10, 25, 15, 0.5)' }}>
                    <ResponsiveContainer width="100%" height={250}>
                        <LineChart
                            width={600}
                            height={300}
                            data={volumeData}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                            onClick={(event) => {
                                const timestamp = event.activePayload[0].payload.timestamp
                                const openValue = event.activePayload[0].payload.open
                                const highValue = event.activePayload[0].payload.high
                                const lowValue = event.activePayload[0].payload.low
                                const closeValue = event.activePayload[0].payload.close
                                const volumeValue = event.activePayload[0].payload.volume
                                handleGraphClick( timestamp, openValue, highValue, lowValue, closeValue, volumeValue )
                             }}
                        >
                            <CartesianGrid strokeDasharray="100 1" stroke="#888"/>
                            <XAxis domain={[xHighMin, xHighMax]} />
                            <YAxis domain={[yHighMin, yHighMax]} />
                            <Tooltip content={(props) => {
                                if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
                                const { payload } = props;
                                const timestamp = new Date(payload[0].payload.timestamp); // Parse timestamp
                                const formattedTimestamp = timestamp.toLocaleString(); // Format timestamp
                                const openValue = payload[0].payload.open; // Get the open value
                                const highValue = payload[0].payload.high; // Get the high value
                                const lowValue = payload[0].payload.low; // Get the low value
                                const closeValue = payload[0].payload.close; // Get the close value
                                const volumeValue = payload[0].payload.volume; // Get the volume value
                                return (
                                    <div className="custom-tooltip" style={{ color: 'black', padding: '5px' }}>
                                        <p>{`${formattedTimestamp}`}</p>
                                        {/* <p>{`Open: ${openValue}`}</p> */}
                                        <p>{`High: ${highValue}`}</p>
                                        {/* <p>{`Low: ${lowValue}`}</p>
                                        <p>{`Close: ${closeValue}`}</p>
                                        <p>{`Volume: ${volumeValue}`}</p> */}
                                    </div>
                                );
                            }} />
                            <Legend content={<div style={{ color: 'black' }}>High: Maximum price of MSFT stock within the time period.</div>} />
                            <Line type="monotone" dataKey="high" stroke="black" activeDot={{ r: 8 }} />
                        </LineChart>
                    </ResponsiveContainer>
                    </div>


                    <div style={{ display: 'd-flex justify-content-center' }}>
                        <div style={{ marginRight: '1%' }}>
                            <table>
                                <thead>
                                    <th><button onClick={()=>{handleReset()}}>Reset</button></th>
                                    <th>Time</th>
                                    <th>Open</th>
                                    <th>High</th>
                                    <th>Low</th>
                                    <th>Close</th>
                                    <th>Volume</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        {firstClickData && <th>HOLD</th>}
                                        {firstClickData && <td>{new Date(firstClickData.timestamp).toLocaleTimeString()}</td>}
                                        {firstClickData && <td>{firstClickData.openValue}</td>}
                                        {firstClickData && <td>{firstClickData.highValue}</td>}
                                        {firstClickData && <td>{firstClickData.lowValue}</td>}
                                        {firstClickData && <td>{firstClickData.closeValue}</td>}
                                        {firstClickData && <td>{firstClickData.volume}</td>}
                                    </tr>
                                    <tr>
                                        {secondClickData && <th>COMPARISON</th>}
                                        {secondClickData && <td style={{color:'red'}}>{new Date(secondClickData.timestamp).toLocaleTimeString()}</td>}
                                        {secondClickData && <td>{secondClickData.openValue}</td>}
                                        {secondClickData && <td>{secondClickData.highValue}</td>}
                                        {secondClickData && <td>{secondClickData.lowValue}</td>}
                                        {secondClickData && <td>{secondClickData.closeValue}</td>}
                                        {secondClickData && <td>{secondClickData.volume}</td>}
                                    </tr>
                                    <tr>
                                        <td>DIFFERENCE</td>
                                        <td>N/A</td>
                                        <td>{secondClickData && ((firstClickData.openValue - secondClickData.openValue) * -1).toFixed(3)}</td>
                                        <td>{secondClickData && ((firstClickData.highValue - secondClickData.highValue) * -1).toFixed(3)}</td>
                                        <td>{secondClickData && ((firstClickData.lowValue - secondClickData.lowValue) * -1).toFixed(3)}</td>
                                        <td>{secondClickData && ((firstClickData.closeValue - secondClickData.closeValue)* -1).toFixed(3)}</td>
                                        <td>{secondClickData && ((firstClickData.volumeValue - secondClickData.volumeValue) * -1).toFixed(1)}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div>
            <h1 style={{ color: 'teal' }}>
                Volume
            </h1>
        </div>
        <div className="card-body" style={{ backgroundColor: 'rgba(10, 25, 25, 0.9)', marginBottom: "3%" }}>
        <ResponsiveContainer width="100%" height={250}>
            <BarChart
                width={1300}
                height={300}
                data={volumeData}
                margin={{
                    top: 5,
                    right: 30,
                    left: 20,
                    bottom: 5,
                }}
                onClick={(event) => {
                    const timestamp = event.activePayload[0].payload.timestamp
                    const openValue = event.activePayload[0].payload.open
                    const highValue = event.activePayload[0].payload.high
                    const lowValue = event.activePayload[0].payload.low
                    const closeValue = event.activePayload[0].payload.close
                    const volumeValue = event.activePayload[0].payload.volume
                    handleGraphClick( timestamp, openValue, highValue, lowValue, closeValue, volumeValue )
                 }}
            >
                <CartesianGrid strokeDasharray="100 1" stroke="#888"/>
                <XAxis domain={[xVolumeMin, xVolumeMax]} />
                <YAxis domain={[yVolumeMin, yVolumeMax]}/>
                <Tooltip content={(props) => {
                                if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
                                const { payload } = props;
                                const timestamp = new Date(payload[0].payload.timestamp); // Parse timestamp
                                const formattedTimestamp = timestamp.toLocaleString(); // Format timestamp
                                const openValue = payload[0].payload.open; // Get the open value
                                const highValue = payload[0].payload.high; // Get the high value
                                const lowValue = payload[0].payload.low; // Get the low value
                                const closeValue = payload[0].payload.close; // Get the close value
                                const volumeValue = payload[0].payload.volume; // Get the volume value
                                return (
                                    <div className="custom-tooltip" style={{ color: 'teal', padding: '5px' }}>
                                        <p>{`${formattedTimestamp}`}</p>
                                        {/* <p>{`Open: ${openValue}`}</p>
                                        <p>{`High: ${highValue}`}</p>
                                        <p>{`Low: ${lowValue}`}</p>
                                        <p>{`Close: ${closeValue}`}</p> */}
                                        <p>{`Volume: ${volumeValue}`}</p>
                                    </div>
                                );
                            }} />
                <Legend content={<div style={{ color: 'teal' }}>Volume: Total number of MSFT shares traded within the period.</div>} />
                <Bar dataKey="volume" fill="yellow" />
            </BarChart>
        </ResponsiveContainer>
        </div>


        <div style={{ display: 'flex' }}>
            <div style={{ marginRight: '1%' }}>
                <table>
                    <thead>
                        <th><button onClick={()=>{handleReset()}}>Reset</button></th>
                        <th>Time</th>
                        <th>Open</th>
                        <th>High</th>
                        <th>Low</th>
                        <th>Close</th>
                        <th>Volume</th>
                    </thead>
                    <tbody>
                        <tr>
                            {firstClickData && <th>HOLD</th>}
                            {firstClickData && <td style={{color:'red'}}>{new Date(firstClickData.timestamp).toLocaleTimeString()}</td>}
                            {firstClickData && <td>{firstClickData.openValue}</td>}
                            {firstClickData && <td>{firstClickData.highValue}</td>}
                            {firstClickData && <td>{firstClickData.lowValue}</td>}
                            {firstClickData && <td>{firstClickData.closeValue}</td>}
                            {firstClickData && <td>{firstClickData.volume}</td>}
                        </tr>
                        <tr>
                            {secondClickData && <th>COMPARISON</th>}
                            {secondClickData && <td style={{color:'red'}}>{new Date(secondClickData.timestamp).toLocaleTimeString()}</td>}
                            {secondClickData && <td>{secondClickData.openValue}</td>}
                            {secondClickData && <td>{secondClickData.highValue}</td>}
                            {secondClickData && <td>{secondClickData.lowValue}</td>}
                            {secondClickData && <td>{secondClickData.closeValue}</td>}
                            {secondClickData && <td>{secondClickData.volume}</td>}
                        </tr>
                        <tr>
                            <td>DIFFERENCE</td>
                            <td>N/A</td>
                            <td>{secondClickData && ((firstClickData.openValue - secondClickData.openValue) * -1).toFixed(3)}</td>
                            <td>{secondClickData && ((firstClickData.highValue - secondClickData.highValue) * -1).toFixed(3)}</td>
                            <td>{secondClickData && ((firstClickData.lowValue - secondClickData.lowValue) * -1).toFixed(3)}</td>
                            <td>{secondClickData && ((firstClickData.closeValue - secondClickData.closeValue)* -1).toFixed(3)}</td>
                            <td>{secondClickData && ((firstClickData.volumeValue - secondClickData.volumeValue) * -1).toFixed(1)}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>



        <div className="container d-flex flex-column align-items-center justify-content">
            <h4>Search by time:</h4>
            <input onChange={handleDate}/>
            <p>HH:MM</p>
        </div>
        <div className="container d-flex flex-column align-items-center justify-content" style={{ height: '100vh' }}>
            <div>
                <h1 style={{ color: 'black' }}>Table of <span style={{ color: 'teal' }}>VOLUME</span> Data for MSFT</h1>
                <table className="container d-flex flex-column align-items-center justify-content">
                    <thead>
                        <tr>
                            <th >TimeStamp</th>
                            <th style={{ color: 'teal' }}>Volume</th>
                        </tr>
                    </thead>
                    <tbody>
                        {volumeData.filter(volume => date === '' || volume.timestamp.includes(date)).map(volume => (
                            <tr key={volume.timestamp}>
                            <td className="container d-flex flex-column align-items-center justify-content">
                                {new Date(volume.timestamp).toLocaleString('en-US', {
                                    year: 'numeric',
                                    month: 'short',
                                    day: '2-digit',
                                    hour: '2-digit',
                                    minute: '2-digit',
                                    second: '2-digit',
                                    hour12: false
                                })}
                            </td>
                            <td style={{ color: 'teal' }}>{volume.volume}</td>
                        </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
        </>
    )
}

export default TSIGraphInteractive
