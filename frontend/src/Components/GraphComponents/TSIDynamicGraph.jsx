import { useState, useEffect } from 'react'
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';
// import { parseScale } from 'recharts/types/util/ChartUtils';



function TSIDynamicGraph (dataChoice) {

    // const initialValue = [0,1,2,3,1,2,2,3,4,5,6,7,7,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,3,4,5,6,7,8,9,0,9,8,7,6,5,4,3,2,1,2,3,4,5,6,7,8,9,0,9,8,7,6,5,4,3,2,1,2,3,4,5,6,7,8,9,0,9,8,7,6,5,4,3,2,1,2,3,4,5,6,7,8,9,0,9,8,7,6,5,4,3,2,1,2,3,4,5,6,7,8,9,0,9,8,7,6,5,4,3,2,1]
    const [volumeData, setVolumeData] = useState([]);
    const [date, setDate] = useState('')
    const [graphClick, setGraphClick] = useState({index: null, timestamp: null, openValue: null , highValue: null, lowValue: null, closeValue: null, volumeValue: null})
    const [firstClickData, setFirstClickData] = useState(null);
    const [secondClickData, setSecondClickData] = useState(null);
    const [selectedPoints, setSelectedPoints] = useState([])
    const [graphClick2, setGraphClick2] = useState({index: null, timestamp: null, openValue: null , highValue: null, lowValue: null, closeValue: null, volumeValue: null})
    const [firstClickData2, setFirstClickData2] = useState(null);
    const [secondClickData2, setSecondClickData2] = useState(null);
    const [selectedPoints2, setSelectedPoints2] = useState([])
    const [secondGraphData, setSecondGraphData] =useState([])


    const xOpenData = volumeData.map(item => item.open);
    const yOpenData = volumeData.map(item => item.open);
    const xOpenMin = Math.min(...xOpenData);
    const xOpenMax = Math.max(...xOpenData);
    const yOpenMin = Math.min(...yOpenData);
    const yOpenMax = Math.max(...yOpenData);

    const xHighData = volumeData.map(item => item.high);
    const yHighData = volumeData.map(item => item.high);
    const xHighMin = Math.min(...xHighData);
    const xHighMax = Math.max(...xHighData);
    const yHighMin = Math.min(...yHighData);
    const yHighMax = Math.max(...yHighData);

    const xLowData = volumeData.map(item => item.low);
    const yLowData = volumeData.map(item => item.low);
    const xLowMin = Math.min(...xLowData);
    const xLowMax = Math.max(...xLowData);
    const yLowMin = Math.min(...yLowData);
    const yLowMax = Math.max(...yLowData);

    const xCloseData = volumeData.map(item => item.close);
    const yCloseData = volumeData.map(item => item.open);
    const xCloseMin = Math.min(...xCloseData);
    const xCloseMax = Math.max(...xCloseData);
    const yCloseMin = Math.min(...yCloseData);
    const yCloseMax = Math.max(...yCloseData);

    const xVolumeData = volumeData.map(item => item.volume);
    const yVolumeData = volumeData.map(item => item.volume);
    const xVolumeMin = Math.min(...xVolumeData);
    const xVolumeMax = Math.max(...xVolumeData);
    const yVolumeMin = Math.min(...yVolumeData);
    const yVolumeMax = Math.max(...yVolumeData);

    // Calculate the domain for XAxis based on selected points
    let xDomain = selectedPoints.length === 2
      ? [selectedPoints[0].timestamp, selectedPoints[1].timestamp]
      : [0, volumeData.length - 1];

    let yDomain = selectedPoints.length === 2
      ? [selectedPoints[0].timestamp, selectedPoints[1].timestamp]
      : [0, volumeData.length - 1];

    // Filter the volumeData based on selected points
    const filteredData = volumeData.filter(item => {
        if (selectedPoints.length === 2) {
            return item.timestamp >= selectedPoints[0].timestamp && item.timestamp <= selectedPoints[1].timestamp;
        }
        return true;
    });



    const separateGet = () => {
        if (dataChoice.dataChoice.startsWith('get')) {
            let remaining = dataChoice.dataChoice.slice('get'.length).trim();
            return remaining;
        } else {
            return null;
        }
        }


    const fetchData = async () => {
        const url = `http://localhost:8000/stocks/${dataChoice.dataChoice}/`;
        try {
            const response = await fetch(url);
            if (response.ok) {
                const result = await response.json();
                let dataSelection = separateGet()
                    if (dataSelection === 'Tsi'){
                    dataSelection = 'tsi'
                    }

                const parsedData = result[dataSelection]
                .filter(data => new Date(data.timestamp).toLocaleDateString() === new Date(dataChoice.selectedDate).toLocaleDateString())
                    .map((data) => ({
                    timestamp: data.timestamp,
                    open: parseFloat(data["open"]),
                    high: parseFloat(data["high"]),
                    low: parseFloat(data["low"]),
                    close: parseFloat(data["close"]),
                    volume: parseFloat(data["volume"])
                })).reverse()

                setVolumeData(parsedData);

                // Extract timestamps and update options state
                // const timestamps = await extractTimestamps(parsedData);
                // setOptions(prevOptions => ({
                //     ...prevOptions,
                //     xaxis: {
                //         ...prevOptions.xaxis,
                //         categories: timestamps
                //     }
                // }));
            }
        } catch (error) {
            console.log("error!", error);
        }
    };

    useEffect(()=>{fetchData()},[dataChoice])


    async function handleAPICall () {
        fetchData()
    }

    const handleDate = (event) => {
        const value = event.target.value
        setDate(value)
    }

    const handleReset = () => {
        setFirstClickData(null)
        setSecondClickData(null)
    }

    const handleReset2 = () => {
        setFirstClickData2(null)
        setSecondClickData2(null)
    }

    const thirdClick = () => {
        handleReset()
        setSelectedPoints([xDomain, yDomain])
    }



    const handleGraphClick = (index, timestamp, openValue, highValue, lowValue, closeValue, volumeValue) => {
        setGraphClick({index, timestamp, openValue, highValue, lowValue, closeValue, volumeValue })
        // clickCounter += 1
        if (firstClickData === null && secondClickData === null) {
            setFirstClickData({index, timestamp, openValue, highValue, lowValue, closeValue, volumeValue })
        } else if (firstClickData !== null && secondClickData === null) {
            setSecondClickData({ index, timestamp, openValue, highValue, lowValue, closeValue, volumeValue })
        } else if (firstClickData && secondClickData) {
            handleReset()
            setFirstClickData({index, timestamp, openValue, highValue, lowValue, closeValue, volumeValue })
        }
        if (selectedPoints.length < 2) {
            setSelectedPoints([...selectedPoints, { index, timestamp, openValue, highValue, lowValue, closeValue, volumeValue }]);
        } else {
            setSelectedPoints([{ index, timestamp, openValue, highValue, lowValue, closeValue, volumeValue }]);
        }
    }

    const handleGraphClick2 = (index, timestamp, openValue, highValue, lowValue, closeValue, volumeValue) => {
        setGraphClick2({index, timestamp, openValue, highValue, lowValue, closeValue, volumeValue })
        // clickCounter += 1
        if (firstClickData2 === null && secondClickData2 === null) {
            setFirstClickData2({index, timestamp, openValue, highValue, lowValue, closeValue, volumeValue })
        } else if (firstClickData2 !== null && secondClickData2 === null) {
            setSecondClickData2({ index, timestamp, openValue, highValue, lowValue, closeValue, volumeValue })
        } else if (firstClickData2 && secondClickData2) {
            handleReset2()
            setFirstClickData2({index, timestamp, openValue, highValue, lowValue, closeValue, volumeValue })
        }
        if (selectedPoints2.length < 2) {
            setSelectedPoints2([...selectedPoints2, { index, timestamp, openValue, highValue, lowValue, closeValue, volumeValue }]);
        } else {
            setSelectedPoints2([{ index, timestamp, openValue, highValue, lowValue, closeValue, volumeValue }]);
        }
    }

    // console.log("volume DATA", volumeData)
    // console.log("GRAPH CLICK--------------------:", graphClick)
    console.log("first click", firstClickData)
    console.log("dataChoice.selectedDate", dataChoice.selectedDate)



    return (
        <>
        <div style={{ marginBottom: '1%' }}></div>
            {/* <div className="d-flex justify-content-center" style={{ height: '10vh', marginBottom: '20px' }}>
                <h1 className="display-4">Dynamic Graphs</h1>
                {volumeData && <h2>{new Date(volumeData[0]['timestamp']).toLocaleDateString()}</h2>}
            </div> */}
            {/* <div className="mb-3">
                    <button className="btn btn-primary" onClick={() => { handleAPICall() }}>Click to retrieve today's Info</button>
            </div> */}
            <div>
                <h4 className='d-flex justify-content-center'>{(dataChoice && separateGet())} {(dataChoice.selectedDate).toLocaleDateString()}</h4>
            </div>
            <div style={{marginBottom:'1%'}}>
                <div>
                    <div className="col-md-4"></div>
                    <div>
                        <div className="card flex-grow-1 mx-2">
                            {/* <h2 className="card-header text-success">QQQ High Blue/Low Green</h2> */}
                            <div className='d-flex justify-content-center'>
                            <div className="card-body" style={{ backgroundColor: 'rgba(10, 25, 15, 0.6)' }}>
                            <ResponsiveContainer width="100%" height={200}>
                            <LineChart
                                  width={600}
                                  height={200}
                                  data={volumeData}
                                  margin={{
                                      top: 5,
                                      right: 30,
                                      left: 20,
                                      bottom: 5,
                                  }}
                                  onClick={(event) => {
                                    console.log("-----------activePayload-----------", event.activePayload)
                                    const index = event.activePayload[0].payload.index
                                      const timestamp = event.activePayload[0].payload.timestamp
                                      const openValue = event.activePayload[0].payload.open
                                      const highValue = event.activePayload[0].payload.high
                                      const lowValue = event.activePayload[0].payload.low
                                      const closeValue = event.activePayload[0].payload.close
                                      const volumeValue = event.activePayload[0].payload.volume
                                      handleGraphClick(index, timestamp, openValue, highValue, lowValue, closeValue, volumeValue)
                                  }}
                              >
                                  <CartesianGrid strokeDasharray="100 1" stroke="#888" />
                                  <XAxis domain={[xHighMin, xHighMax]} />
                                  <YAxis domain={[yHighMin, yHighMax]} />
                                  <Tooltip content={(props) => {
                                      if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
                                      const { payload } = props;
                                      const timestamp = new Date(payload[0].payload.timestamp);
                                      const formattedTimestamp = timestamp.toLocaleString();
                                      const openValue = payload[0].payload.open;
                                      const highValue = payload[0].payload.high;
                                      const lowValue = payload[0].payload.low;
                                      const closeValue = payload[0].payload.close;
                                      const volumeValue = payload[0].payload.volume;
                                      return (
                                          <div className="custom-tooltip" style={{ color: 'blue', padding: '5px' }}>
                                              <p>{`Time: ${new Date(formattedTimestamp).toLocaleTimeString()}`}</p>
                                              {/* <p>{`Open: ${openValue}`}</p> */}
                                              {/* <p>{`High: ${highValue}`}</p>
                                              <p>{`Low: ${lowValue}`}</p>
                                              <p>{`Close: ${closeValue}`}</p> */}
                                              {/* <p>{`Volume: ${volumeValue}`}</p> */}
                                          </div>
                                      );
                                  }} />
                                  <Legend content={<div style={{ color: 'green' }}>Total Range</div>} />
                                  <Line type="monotone" dataKey="high" stroke="green" activeDot={{ r: 8 }} />
                                  <Line type="monotone" dataKey="low" stroke="blue" />
                                  {/* <Line type="monotone" dataKey="open" stroke="red" />
                                  <Line type="monotone" dataKey="close" stroke="yellow" /> */}
                              </LineChart>
                              </ResponsiveContainer>
                            </div>
                        </div>
                        <div className="card flex-grow-1">
                            {/* <h2 className="card-header">Dynamic Selection</h2> */}
                            <div className="card-body" style={{ backgroundColor: 'rgba(10, 25, 15, 0.6)' }}>
                            <ResponsiveContainer width="100%" height={275}>
                            <LineChart
                                  width={600}
                                  height={200}
                                  data={filteredData}
                                  margin={{
                                      top: 5,
                                      right: 30,
                                      left: 20,
                                      bottom: 5,
                                  }}
                                  onClick={(event) => {
                                    console.log("-----------activePayload-----------", event.activePayload)
                                    const index = event.activePayload[0].payload.index
                                      const timestamp = event.activePayload[0].payload.timestamp
                                      const openValue = event.activePayload[0].payload.open
                                      const highValue = event.activePayload[0].payload.high
                                      const lowValue = event.activePayload[0].payload.low
                                      const closeValue = event.activePayload[0].payload.close
                                      const volumeValue = event.activePayload[0].payload.volume
                                      handleGraphClick2(index, timestamp, openValue, highValue, lowValue, closeValue, volumeValue)
                                  }}
                              >
                                <CartesianGrid strokeDasharray="100 1" stroke="#888" />
                                <XAxis dataKey="timestamp"
                                        domain={xDomain}
                                        tickFormatter={(timestamp) => new Date(timestamp).toLocaleTimeString()}
                                        />
                                <YAxis domain={[yDomain]} />
                                <Tooltip content={(props) => {
                                    if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
                                    const { payload } = props;
                                    const timestamp = new Date(payload[0].payload.timestamp);
                                    const formattedTimestamp = timestamp.toLocaleString();
                                    const openValue = payload[0].payload.open;
                                    const highValue = payload[0].payload.high;
                                    const lowValue = payload[0].payload.low;
                                    const closeValue = payload[0].payload.close;
                                    const volumeValue = payload[0].payload.volume;
                                    if (openValue >= closeValue) {
                                    return (
                                        <div className="custom-tooltip" style={{ color: 'black', padding: '5px' }}>
                                            <p>{`Timestamp: ${formattedTimestamp}`}</p>
                                            {/* <p>{`High: ${highValue}`}</p> */}
                                            <p style={{color: 'yellow'}}>{`Open: ${openValue}`}</p>
                                            <p style={{color: 'red'}}>{`Close: ${closeValue}`}</p>
                                            {/* <p>{`Low: ${lowValue}`}</p> */}
                                            {/* <p>{`Volume: ${volumeValue}`}</p> */}
                                        </div>
                                    )} else if (closeValue > openValue) {
                                        return (
                                            <div className="custom-tooltip" style={{ color: 'black', padding: '5px' }}>
                                                <p>{`Timestamp: ${formattedTimestamp}`}</p>
                                                {/* <p>{`High: ${highValue}`}</p> */}
                                                <p style={{color: 'red'}}>{`Close: ${closeValue}`}</p>
                                                <p style={{color: 'yellow'}}>{`Open: ${openValue}`}</p>
                                                {/* <p>{`Low: ${lowValue}`}</p> */}
                                                {/* <p>{`Volume: ${volumeValue}`}</p> */}
                                            </div>
                                        )
                                    }
                                }} />
                                <Legend content={<div style={{ color: 'green' }}>Selected Range View</div>} />
                                <Line type="monotone" dataKey="high" stroke="blue"  />
                                <Line type="monotone" dataKey="low" stroke="blue" />
                                <Line type="monotone" dataKey="open" stroke="yellow" activeDot={{ r: 8 }}/>
                                <Line type="monotone" dataKey="close" stroke="red" />
                                </LineChart>
                            </ResponsiveContainer>
                            </div>
                        </div>
                    </div>


                    <div>
                        <table className="table table-striped">
                            <tbody>
                                <tr>
                                    <th>First Click:</th>
                                    <td>Time: {firstClickData2 && new Date(firstClickData2.timestamp).toLocaleTimeString()}</td>
                                    <td>Open: {firstClickData2 && firstClickData2.openValue}</td>
                                    <td>High: {firstClickData2 && firstClickData2.highValue}</td>
                                    <td>Low: {firstClickData2 && firstClickData2.lowValue}</td>
                                    <td>Close: {firstClickData2 && firstClickData2.closeValue}</td>
                                    <td>Volume: {firstClickData2 && firstClickData2.volumeValue}</td>
                                </tr>
                                <tr>
                                    <th>Second Click:</th>
                                    <td>Time: {secondClickData2 && new Date(secondClickData2.timestamp).toLocaleTimeString()}</td>
                                    <td>Open: {secondClickData2 && secondClickData2.openValue}</td>
                                    <td>High: {secondClickData2 && secondClickData2.highValue}</td>
                                    <td>Low: {secondClickData2 && secondClickData2.lowValue}</td>
                                    <td>Close: {secondClickData2 && secondClickData2.closeValue}</td>
                                    <td>Volume: {secondClickData2 && secondClickData2.volumeValue}</td>
                                </tr>
                                <tr>
                                    <th>Difference:</th>
                                    <td>Time: ---</td>
                                    <td style={{color: 'yellow'}}>Open: {secondClickData2 && ((firstClickData2.openValue - secondClickData2.openValue) * -1).toFixed(3)}</td>
                                    <td>High: {secondClickData2 && ((firstClickData2.highValue - secondClickData2.highValue) * -1).toFixed(3)}</td>
                                    <td>Low: {secondClickData2 && ((firstClickData2.lowValue - secondClickData2.lowValue) * -1).toFixed(3)}</td>
                                    <td>Close: {secondClickData2 && ((firstClickData2.closeValue - secondClickData2.closeValue)* -1).toFixed(3)}</td>
                                    <td>Volume: {secondClickData2 && ((firstClickData2.volumeValue - secondClickData2.volumeValue) * -1).toFixed(3)}</td>
                                </tr>
                            </tbody>
                        </table>
                        <div>
                            <button className="btn btn-secondary" onClick={() => { handleReset() }}>Reset</button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )















//     return (
//       <>
//           {/* <div style={{ paddingTop: '200px' }}></div> */}
//           <div className="d-flex justify-content-center align-items-center" style={{ height: '10vh', marginBottom: '120px' }}>
//               <div className="w-75 text-center">
//                   <h1 className="display-1">Interactive Graphs</h1>
//               </div>
//           </div>
//           <div>
//               <button onClick={() => { handleAPICall() }}>Click to retrieve today's Info</button>
//           </div>
//           <div className='flex-d flex-container'>





//               <div className="row">
//                   <div className="col-md-12">
//                       <div className="card">
//                           <h2 className="card-header text-success">Open</h2>
//                           <div className="card-body" style={{ backgroundColor: 'rgba(10, 25, 15, 0.5)' }}>
//                               <LineChart
//                                   width={600}
//                                   height={200}
//                                   data={volumeData}
//                                   margin={{
//                                       top: 5,
//                                       right: 30,
//                                       left: 20,
//                                       bottom: 5,
//                                   }}
//                                   onClick={(event) => {
//                                     console.log("-----------activePayload-----------", event.activePayload)
//                                     const index = event.activePayload[0].payload.index
//                                       const timestamp = event.activePayload[0].payload.timestamp
//                                       const openValue = event.activePayload[0].payload.open
//                                       const highValue = event.activePayload[0].payload.high
//                                       const lowValue = event.activePayload[0].payload.low
//                                       const closeValue = event.activePayload[0].payload.close
//                                       const volumeValue = event.activePayload[0].payload.volume
//                                       handleGraphClick(index, timestamp, openValue, highValue, lowValue, closeValue, volumeValue)
//                                   }}
//                               >
//                                   <CartesianGrid strokeDasharray="100 1" stroke="#888" />
//                                   <XAxis domain={[xOpenMin, xOpenMax]} />
//                                   <YAxis domain={[yOpenMin, yOpenMax]} />
//                                   <Tooltip content={(props) => {
//                                       if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
//                                       const { payload } = props;
//                                       const timestamp = new Date(payload[0].payload.timestamp);
//                                       const formattedTimestamp = timestamp.toLocaleString();
//                                       const openValue = payload[0].payload.open;
//                                       const highValue = payload[0].payload.high;
//                                       const lowValue = payload[0].payload.low;
//                                       const closeValue = payload[0].payload.close;
//                                       const volumeValue = payload[0].payload.volume;
//                                       return (
//                                           <div className="custom-tooltip" style={{ color: 'blue', padding: '5px' }}>
//                                               <p>{`Timestamp: ${formattedTimestamp}`}</p>
//                                               <p>{`Open: ${openValue}`}</p>
//                                               {/* <p>{`High: ${highValue}`}</p>
//                                               <p>{`Low: ${lowValue}`}</p>
//                                               <p>{`Close: ${closeValue}`}</p> */}
//                                               <p>{`Volume: ${volumeValue}`}</p>
//                                           </div>
//                                       );
//                                   }} />
//                                   <Legend content={<div style={{ color: 'green' }}>Open: Starting price of MSFT stock within the time period.</div>} />
//                                   <Line type="monotone" dataKey="open" stroke="green" activeDot={{ r: 8 }} />
//                               </LineChart>
//                           </div>
//                       </div>
//                   </div>
//               </div>




//               <div style={{ display: 'flex' }}>
//                             <button onClick={()=>{handleReset()}}>Reset</button>
//                             <table>
//                                 <tbody>
//                                     <tr>
//                                         <th>First Click:</th>
//                                         <td>Time: {firstClickData && new Date(firstClickData.timestamp).toLocaleTimeString()}</td>
//                                         <td>Open: {firstClickData && firstClickData.openValue}</td>
//                                         <td>High: {firstClickData && firstClickData.highValue}</td>
//                                         <td>Low: {firstClickData && firstClickData.lowValue}</td>
//                                         <td>Close: {firstClickData && firstClickData.closeValue}</td>
//                                         <td>Volume: {firstClickData && firstClickData.volumeValue}</td>
//                                     </tr>
//                                     <tr>
//                                         <th>Second Clicks:</th>
//                                         <td>Time:  {secondClickData && new Date(secondClickData.timestamp).toLocaleTimeString()}</td>
//                                         <td>Open:{secondClickData && secondClickData.openValue}</td>
//                                         <td>High: {secondClickData && secondClickData.highValue}</td>
//                                         <td>Low: {secondClickData && secondClickData.lowValue}</td>
//                                         <td>Close: {secondClickData && secondClickData.closeValue}</td>
//                                         <td>Volume: {secondClickData && secondClickData.volumeValue}</td>
//                                     </tr>
//                                     <tr>
//                                         <th>Difference:</th>
//                                         <td className="text-danger">Time:  {secondClickData && new Date((new Date(firstClickData.timestamp).toLocaleDateString() - new Date(secondClickData.timestamp).toLocaleDateString())).toLocaleTimeString()}</td>
//                                         <td className="text-danger">Open:{secondClickData && (firstClickData.openValue - secondClickData.openValue)}</td>
//                                         <td className="text-danger">High: {secondClickData && (firstClickData.highValue - secondClickData.highValue)}</td>
//                                         <td className="text-danger">Low: {secondClickData && (firstClickData.lowValue - secondClickData.lowValue)}</td>
//                                         <td className="text-danger">Close: {secondClickData && (firstClickData.closeValue - secondClickData.closeValue)}</td>
//                                         <td className="text-danger">Volume: {secondClickData && (firstClickData.volumeValue - secondClickData.volumeValue)}</td>
//                                     </tr>
//                                 </tbody>
//                             </table>

//                           </div>







//               <div className="row mt-3">
//                   <div className="col-md-12">
//                       <div className="card">
//                           <h2 className="card-header">Open Dynamic Selection</h2>
//                           <div className="card-body" style={{ backgroundColor: 'rgba(10, 25, 15, 0.5)' }}>
//                               <LineChart
//                                   width={400}
//                                   height={150}
//                                   data={filteredData}
//                                   margin={{
//                                       top: 5,
//                                       right: 30,
//                                       left: 20,
//                                       bottom: 5,
//                                   }}
//                                   onClick={(event) => {
//                                       const timestamp = event.activePayload[0].payload.timestamp
//                                       const openValue = event.activePayload[0].payload.open
//                                       const highValue = event.activePayload[0].payload.high
//                                       const lowValue = event.activePayload[0].payload.low
//                                       const closeValue = event.activePayload[0].payload.close
//                                       const volumeValue = event.activePayload[0].payload.volume
//                                       handleGraphClick(timestamp, openValue, highValue, lowValue, closeValue, volumeValue)
//                                   }}
//                               >
//                                   <CartesianGrid strokeDasharray="100 1" stroke="#888" />
//                                   <XAxis domain={[xDomain]} />
//                                   <YAxis domain={[yDomain]} />
//                                   <Tooltip content={(props) => {
//                                       if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
//                                       const { payload } = props;
//                                       const timestamp = new Date(payload[0].payload.timestamp);
//                                       const formattedTimestamp = timestamp.toLocaleString();
//                                       const openValue = payload[0].payload.open;
//                                       const highValue = payload[0].payload.high;
//                                       const lowValue = payload[0].payload.low;
//                                       const closeValue = payload[0].payload.close;
//                                       const volumeValue = payload[0].payload.volume;
//                                       return (
//                                           <div className="custom-tooltip" style={{ color: 'blue', padding: '5px' }}>
//                                               <p>{`Timestamp: ${formattedTimestamp}`}</p>
//                                               <p>{`Open: ${openValue}`}</p>
//                                               {/* <p>{`High: ${highValue}`}</p>
//                                               <p>{`Low: ${lowValue}`}</p>
//                                               <p>{`Close: ${closeValue}`}</p> */}
//                                               <p>{`Volume: ${volumeValue}`}</p>
//                                           </div>
//                                       );
//                                   }} />
//                                   <Legend content={<div style={{ color: 'green' }}>Selected Range View</div>} />
//                                   <Line type="monotone" dataKey="open" stroke="red" activeDot={{ r: 8 }} />
//                               </LineChart>
//                           </div>
//                       </div>
//                   </div>
//               </div>



//           </div>
//       </>
//   )

}

export default TSIDynamicGraph
