import React, { useState, useEffect } from "react";
import { Card, CardContent, CardHeader, Divider } from "@mui/material";
import Chart from "react-apexcharts";
import { ResponsiveContainer, Tooltip } from "recharts";
import QQQNotes from "../NotesComponents/QQQNotes";
import SPYNotes from "../NotesComponents/SPYNotes";
import IWMNotes from "../NotesComponents/IWMNotes";
import MSFTNotes from "../NotesComponents/MSFTNotes";


function CandleStick2(dataChoice) {

    useEffect(() => {
        fetchQQQData();
    }, [dataChoice]);

    const [data, setData] = useState([]);
    const [options, setOptions] = useState({
        xaxis: {
            type: "datetime",
        },
        tooltip: {
            x: {
                format: 'hh:mm MMM dd'
            },
            y: {
                formatter: function(value) {
                    return '$' + value.toFixed(2);
                }
            }
        },
    });

    // console.log("dataChoice", dataChoice)



    const separateGet = () => {
        if (dataChoice.dataChoice.startsWith('get')) {
            let remaining = dataChoice.dataChoice.slice('get'.length).trim();
            return remaining;
        } else {
            return null;
        }
      }

    const fetchQQQData = async () => {
        const url = `http://localhost:8000/stocks/${dataChoice.dataChoice}/`;
        try {
            const response = await fetch(url);
            if (response.ok) {
                const result = await response.json();
                let dataSelection = separateGet()
                    if (dataSelection === 'Tsi'){
                    dataSelection = 'tsi'
                    }

                const parsedData = result[dataSelection].map((data) => ({
                    timestamp: data.timestamp,
                    open: parseFloat(data["open"]),
                    high: parseFloat(data["high"]),
                    low: parseFloat(data["low"]),
                    close: parseFloat(data["close"]),
                    volume: parseFloat(data["volume"])
                })).reverse();
                setData(parsedData);

                // Extract timestamps and update options state
                const timestamps = await extractTimestamps(parsedData);
                setOptions(prevOptions => ({
                    ...prevOptions,
                    xaxis: {
                        ...prevOptions.xaxis,
                        categories: timestamps
                    }
                }));
            }
        } catch (error) {
            console.log("error!", error);
        }
    };

    const handleGetInfo = async () => {
        await fetchQQQData();
    };

    const extractTimestamps = async (data) => {
        const timestamps = data.map(item => item.timestamp);
        return timestamps;
    };

    const filterData = () => {
        const filtered = data.filter(item => new Date(item.timestamp).toLocaleDateString() === new Date(dataChoice.selectedDate).toLocaleDateString())
        return filtered;
    }

// console.log('data', data)


    return (
        <>
            <div style={{ marginBottom: "1%" }}></div>
            <div>
                {/* <button onClick={handleGetInfo}>Click for Info</button> */}
            </div>
            {options && <div>
                {dataChoice.dataChoice === 'getQQQ' &&
                    <QQQNotes />
                }
                {dataChoice.dataChoice === 'getSPY' &&
                    <SPYNotes />
                }
                {dataChoice.dataChoice === 'getIWM' &&
                    <IWMNotes />
                }
                {dataChoice.dataChoice === 'getTsi' &&
                    <MSFTNotes />
                }
                <Card>
                <h3>{` ${separateGet(dataChoice)} ${new Date((dataChoice.selectedDate)).toLocaleDateString()}`}</h3>
                    {/* <CardHeader title={`Single Candlestick  ${separateGet(dataChoice.dataChoice)}`} /> */}
                    <Divider />
                    <CardContent>
                        <ResponsiveContainer width="100%" height={450}>
                            <Chart
                                options={options}
                                series={[{ data: data.filter(item => new Date(item.timestamp).toLocaleDateString() === new Date(dataChoice.selectedDate).toLocaleDateString())
                                    .map(item => ({ x: new Date(item.timestamp).getTime(), y: [item.open, item.high, item.low, item.close] })) }]}
                                type="candlestick"
                                width={500}
                                height={320}
                            />
                        </ResponsiveContainer>
                    </CardContent>
                </Card>
            </div>}
        </>
    );
}

export default CandleStick2;
