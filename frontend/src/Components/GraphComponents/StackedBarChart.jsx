import React, { useState, useEffect } from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import QQQNotes from '../NotesComponents/QQQNotes';
import IWMNotes from '../NotesComponents/IWMNotes';
import SPYNotes from '../NotesComponents/SPYNotes';


function StackedBarChart () {
    const [spyData, setSPYData] = useState([]);
    const [qqqData, setQQQData] = useState([])
    const [iwmData, setIWMData] = useState([])

    useEffect(() => {
        fetchStockData();
    }, []);

    const fetchSPYData = async () => {
        const url = 'http://localhost:8000/stocks/getSPY/';
        try {
            const response = await fetch(url);
            const result = await response.json();
            console.log("result", result);
            setSPYData(result["SPY"]);
        } catch (error) {
            console.error(error);
        }
    };

    const fetchQQQData = async () => {
        const url = 'http://localhost:8000/stocks/getQQQ/';
        try {
            const response = await fetch(url);
            const result = await response.json();
            console.log("result", result);
            setQQQData(result["QQQ"]);
        } catch (error) {
            console.error(error);
        }
    };

    const fetchIWMData = async () => {
        const url = 'http://localhost:8000/stocks/getIWM/';
        try {
            const response = await fetch(url);
            const result = await response.json();
            console.log("result", result);
            setIWMData(result["IWM"]);
        } catch (error) {
            console.error(error);
        }
    };

    const fetchStockData = async () => {
        await fetchSPYData();
        await fetchQQQData();
        await fetchIWMData();
    }



    return (
        <>
            <div style={{marginBottom: '1%'}}></div>
            <div>
                <h3 style={{marginBottom:'1%'}} className="d-flex justify-content-center">All Data</h3>
            </div>
            {/* <div><button onClick={fetchStockData}>Click for Info</button></div> */}
            {spyData !== null && (
            <div className='d-flex justify-content-center'>
                <h4>SPY</h4>
            </div>
            )}

            {spyData && spyData.length > 0 && (
                <div style={{ width: '100%' }}>
                    <ResponsiveContainer width="100%" height={250}>
                        <BarChart
                            width={500}
                            height={300}
                            data={spyData}
                            margin={{
                                top: 20,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="date" />
                            <YAxis />
                            <Tooltip content={(props) => {
                                if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
                                const { payload } = props;
                                const timestamp = new Date(payload[0].payload.timestamp);
                                const formattedTimestamp = timestamp.toLocaleString();
                                const openValue = payload[0].payload.open;
                                const highValue = payload[0].payload.high;
                                const lowValue = payload[0].payload.low;
                                const closeValue = payload[0].payload.close;
                                const volumeValue = payload[0].payload.volume;
                                return (
                                    <div className="custom-tooltip" style={{ color: 'black', padding: '5px' }}>
                                        <p>{`Time: ${new Date(formattedTimestamp).toLocaleString()}`}</p>
                                         <p>{`High: ${highValue}`}</p>
                                         <p>{`Open: ${openValue}`}</p>
                                        <p>{`Close: ${closeValue}`}</p>
                                        <p>{`Low: ${lowValue}`}</p>
                                         <p>{`Volume: ${volumeValue}`}</p>
                                    </div>
                                );
                            }} />
                            <Legend />
                            <Bar dataKey="low" stackId="a" fill="teal" />
                            <Bar dataKey="close" stackId="a" fill="blue" />
                            <Bar dataKey="open" stackId="a" fill="green" />
                            <Bar dataKey="high" stackId="a" fill="yellow" />
                        </BarChart>
                    </ResponsiveContainer>
                    <SPYNotes />
                </div>
            )}

            <div className='d-flex justify-content-center'>
                <h4>QQQ</h4>
            </div>

            {spyData && spyData.length > 0 && (
                <div style={{ width: '100%' }}>
                    <ResponsiveContainer width="100%" height={250}>
                        <BarChart
                            width={500}
                            height={300}
                            data={qqqData}
                            margin={{
                                top: 20,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="date" />
                            <YAxis />
                            <Tooltip content={(props) => {
                                if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
                                const { payload } = props;
                                const timestamp = new Date(payload[0].payload.timestamp);
                                const formattedTimestamp = timestamp.toLocaleString();
                                const openValue = payload[0].payload.open;
                                const highValue = payload[0].payload.high;
                                const lowValue = payload[0].payload.low;
                                const closeValue = payload[0].payload.close;
                                const volumeValue = payload[0].payload.volume;
                                return (
                                    <div className="custom-tooltip" style={{ color: 'black', padding: '5px' }}>
                                        <p>{`Time: ${new Date(formattedTimestamp).toLocaleString()}`}</p>
                                         <p>{`High: ${highValue}`}</p>
                                         <p>{`Open: ${openValue}`}</p>
                                        <p>{`Close: ${closeValue}`}</p>
                                        <p>{`Low: ${lowValue}`}</p>
                                         <p>{`Volume: ${volumeValue}`}</p>
                                    </div>
                                );
                            }} />
                            <Legend />
                            <Bar dataKey="low" stackId="a" fill="teal" />
                            <Bar dataKey="close" stackId="a" fill="blue" />
                            <Bar dataKey="open" stackId="a" fill="green" />
                            <Bar dataKey="high" stackId="a" fill="yellow" />
                        </BarChart>
                    </ResponsiveContainer>
                    <QQQNotes />
                </div>
            )}

            <div className='d-flex justify-content-center'>
                <h4>IWM</h4>
            </div>

            {spyData && spyData.length > 0 && (
                <div style={{ width: '100%' }}>
                    <ResponsiveContainer width="100%" height={250}>
                        <BarChart
                            width={500}
                            height={300}
                            data={iwmData}
                            margin={{
                                top: 20,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="date" />
                            <YAxis />
                            <Tooltip content={(props) => {
                                if (props.payload.length === 0) return null; // Hide tooltip if no data point is active
                                const { payload } = props;
                                const timestamp = new Date(payload[0].payload.timestamp);
                                const formattedTimestamp = timestamp.toLocaleString();
                                const openValue = payload[0].payload.open;
                                const highValue = payload[0].payload.high;
                                const lowValue = payload[0].payload.low;
                                const closeValue = payload[0].payload.close;
                                const volumeValue = payload[0].payload.volume;
                                return (
                                    <div className="custom-tooltip" style={{ color: 'black', padding: '5px' }}>
                                        <p>{`Time: ${new Date(formattedTimestamp).toLocaleString()}`}</p>
                                         <p>{`High: ${highValue}`}</p>
                                         <p>{`Open: ${openValue}`}</p>
                                        <p>{`Close: ${closeValue}`}</p>
                                        <p>{`Low: ${lowValue}`}</p>
                                         <p>{`Volume: ${volumeValue}`}</p>
                                    </div>
                                );
                            }} />
                            <Legend />
                            <Bar dataKey="low" stackId="a" fill="teal" />
                            <Bar dataKey="close" stackId="a" fill="blue" />
                            <Bar dataKey="open" stackId="a" fill="green" />
                            <Bar dataKey="high" stackId="a" fill="yellow" />
                        </BarChart>
                    </ResponsiveContainer>
                    <IWMNotes />
                </div>
            )}

        </>
    );
}

export default StackedBarChart;
