import { useState, useEffect } from 'react'
import '../styles/TSITable.css';

function TSITable () {
    const [allData, setAllData] = useState([]);
    const [date, setDate] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8000/stocks/getTsi/';
        try {
            const response = await fetch(url);
            const result = await response.json();
            console.log("result", result)
            setAllData(result["tsi"]);
        } catch (error) {
            console.error(error);
        }
    }

    useEffect(() => { fetchData() }, []);

    const handleDate = (event) => {
        const value = event.target.value;
        setDate(value);
    }

    const handleDelete = async (id) => {
        const deleteUrl = `http://localhost:8000/stocks/deleteTsi/${id}`
        await fetch(deleteUrl, {method: 'delete'})
        fetchData()
    }

    const handleDeleteAll = async () => {
        const deleteUrl = `http://localhost:8000/stocks/deleteTsi/`
        await fetch(deleteUrl, {method: 'delete'})
        fetchData()
    }

    return (
        <>
        <div>
            <h1>Charts and Tables for MSFT</h1>
        </div>
        <div className="container">
            <div>
                <h1>Table of Data from NASDAQ API call for MSFT</h1>
                <h3>Descending List by 5 minutes from most recent at the top</h3>
                <h4>Search</h4>
                <input onChange={handleDate}/>
                <p>Example: 3/8/2024, 4:55:00 PM</p>
                <div>
                    <table className="table table-striped table-custom m-3">
                        <thead>
                            <tr>
                                <th><button onClick={() => {handleDeleteAll()}}>Delete ALL</button></th>
                                <th>----index----</th>
                                <th className="bg-secondary">----Timestamp----</th>
                                <th className="bg-primary">----Open----</th>
                                <th className="bg-success">----High----</th>
                                <th className="bg-warning">----Low----</th>
                                <th className="bg-info">----Close----</th>
                                <th className="bg-danger">----Volume----</th>
                            </tr>
                        </thead>
                        <tbody>
                            {allData.filter(tsiObj => date === '' || new Date(tsiObj.timestamp).toLocaleString().includes(date)).map(tsiObj => {
                                return (
                                    <tr key={tsiObj.id}>
                                        <td><button onClick={() => {handleDelete(tsiObj.id)}}>Delete</button></td>
                                        <td>{tsiObj.id}</td>
                                        <td>{new Date(tsiObj.timestamp).toLocaleString()}</td>
                                        <td>{tsiObj.open}</td>
                                        <td>{tsiObj.high}</td>
                                        <td>{tsiObj.low}</td>
                                        <td>{tsiObj.close}</td>
                                        <td>{tsiObj.volume}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </>
    )
}

export default TSITable;
