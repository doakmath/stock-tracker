import { useState } from "react"

function SaveAllApi () {

    const [volumeData, setVolumeData] = useState([])
    const [spyVolumeData, setSPYVolumeData] = useState([])
    const [qqqVolumeData, setQQQVolumeData] =useState([])
    const [iwmVolumeData, setIWMVolumeData] = useState([])

    const data = [
        spyVolumeData,
        qqqVolumeData,
        iwmVolumeData,
        volumeData
    ]

    async function saveData (volumeData) {
        for (const items of volumeData) {
          await saveDataOneAtATime(items)
        }
        // alert("Save Successful!")
      }

    async function saveDataOneAtATime(items) {
        // console.log("items tsi", items)
        const url = "http://localhost:8000/stocks/createTsi/"
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(items)
        }
        const response = await fetch(url, options)
        if (!response.ok) {
            throw new Error('Failed to submit tsi data')
        }
    }


    const fetchData = async () => {
        const url = 'https://alpha-vantage.p.rapidapi.com/query?interval=5min&function=TIME_SERIES_INTRADAY&symbol=MSFT&datatype=json&output_size=compact';
        const options = {
            method: 'GET',
            headers: {
                'X-RapidAPI-Key': 'bf0635dbcemshde30513e0b8df0cp129e7djsn9f83ff38a9b3',
                'X-RapidAPI-Host': 'alpha-vantage.p.rapidapi.com'
            }
        }
        try {
            const response = await fetch(url, options);
            const result = await response.json();
            // alert("You successfully made an API call!")
            console.log("RESULT", result)
            const volumeData = Object.entries(result["Time Series (5min)"]).map(([timestamp, data]) => ({
                timestamp,
                open: parseFloat(data["1. open"]),
                high: parseFloat(data["2. high"]),
                low: parseFloat(data["3. low"]),
                close: parseFloat(data["4. close"]),
                volume: parseFloat(data["5. volume"])
            }))
            setVolumeData(volumeData)
            saveData(volumeData)
            fetchQQQData()
        } catch (error) {
            console.error(error)
        }
    }


    // _______________________________fetch the other 3______________________________

    const fetchSPYData = async () => {
        const url = 'https://alpha-vantage.p.rapidapi.com/query?interval=5min&function=TIME_SERIES_INTRADAY&symbol=SPY&datatype=json&output_size=compact';
        const options = {
            method: 'GET',
            headers: {
                'X-RapidAPI-Key': 'bf0635dbcemshde30513e0b8df0cp129e7djsn9f83ff38a9b3',
                'X-RapidAPI-Host': 'alpha-vantage.p.rapidapi.com'
            }
        }
        try {
            const response = await fetch(url, options);
            const result = await response.json();
            // alert("You successfully made an API call!")
            console.log("RESULT SPY", result)
            const spyVolumeData = Object.entries(result["Time Series (5min)"]).map(([timestamp, data]) => ({
                timestamp,
                open: parseFloat(data["1. open"]),
                high: parseFloat(data["2. high"]),
                low: parseFloat(data["3. low"]),
                close: parseFloat(data["4. close"]),
                volume: parseFloat(data["5. volume"])
            }))
            setSPYVolumeData(spyVolumeData)
            saveSPYData(spyVolumeData)
            fetchIWMData()
        } catch (error) {
            console.error(error)
        }
    }


    const fetchQQQData = async () => {
        const url = 'https://alpha-vantage.p.rapidapi.com/query?interval=5min&function=TIME_SERIES_INTRADAY&symbol=QQQ&datatype=json&output_size=compact';
        const options = {
            method: 'GET',
            headers: {
                'X-RapidAPI-Key': 'bf0635dbcemshde30513e0b8df0cp129e7djsn9f83ff38a9b3',
                'X-RapidAPI-Host': 'alpha-vantage.p.rapidapi.com'
            }
        }
        try {
            const response = await fetch(url, options);
            const result = await response.json();
            // alert("You successfully made an API call!")
            console.log("RESULT QQQ", result)
            const qqqVolumeData = Object.entries(result["Time Series (5min)"]).map(([timestamp, data]) => ({
                timestamp,
                open: parseFloat(data["1. open"]),
                high: parseFloat(data["2. high"]),
                low: parseFloat(data["3. low"]),
                close: parseFloat(data["4. close"]),
                volume: parseFloat(data["5. volume"])
            }))
            setQQQVolumeData(qqqVolumeData)
            saveQQQData(qqqVolumeData)
            fetchSPYData()
        } catch (error) {
            console.error(error)
        }
    }

    const fetchIWMData = async () => {
        const url = 'https://alpha-vantage.p.rapidapi.com/query?interval=5min&function=TIME_SERIES_INTRADAY&symbol=IWM&datatype=json&output_size=compact';
        const options = {
            method: 'GET',
            headers: {
                'X-RapidAPI-Key': 'bf0635dbcemshde30513e0b8df0cp129e7djsn9f83ff38a9b3',
                'X-RapidAPI-Host': 'alpha-vantage.p.rapidapi.com'
            }
        }
        try {
            const response = await fetch(url, options);
            const result = await response.json();
            // alert("You successfully made an API call!")
            console.log("RESULT IWM", result)
            const iwmVolumeData = Object.entries(result["Time Series (5min)"]).map(([timestamp, data]) => ({
                timestamp,
                open: parseFloat(data["1. open"]),
                high: parseFloat(data["2. high"]),
                low: parseFloat(data["3. low"]),
                close: parseFloat(data["4. close"]),
                volume: parseFloat(data["5. volume"])
            }))
            setIWMVolumeData(iwmVolumeData)
            saveIWMData(iwmVolumeData)
        } catch (error) {
            console.error(error)
        }
    }


    async function saveSPYData (spyVolumeData) {
        for (const items of spyVolumeData) {
            // console.log('items spy', items)
          await saveSPYDataOneAtATime(items)
        }
      }

    async function saveQQQData (qqqVolumeData) {
      for (const items of qqqVolumeData) {
        // console.log('items qqq', items)
        await saveQQQDataOneAtATime(items)
      }
    }

    async function saveIWMData (iwmVolumeData) {
        for (const items of iwmVolumeData) {
            // console.log('items iwm', items)
            await saveIWMDataOneAtATime(items)
        }
        alert("Save Successful!")
    }

    async function saveSPYDataOneAtATime(items) {
        const url = "http://localhost:8000/stocks/createSPY/"
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(items)
        }
        const response = await fetch(url, options)
        if (!response.ok) {
            throw new Error('Failed to submit SPY data')
        }
    }

    async function saveQQQDataOneAtATime(items) {
        const url = "http://localhost:8000/stocks/createQQQ/"
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(items)
        }
        const response = await fetch(url, options)
        if (!response.ok) {
            throw new Error('Failed to submit QQQ data')
        }
    }

    async function saveIWMDataOneAtATime(items) {
        const url = "http://localhost:8000/stocks/createIWM/"
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(items)
        }
        const response = await fetch(url, options)
        if (!response.ok) {
            throw new Error('Failed to submit IMW data')
        }
    }


    function handleAPICall () {
         fetchData()
        //  fetchQQQData()
        //  fetchSPYData()
        //  fetchIWMData()
    }

// console.log('iwmVolumeData', iwmVolumeData)

    return (
        <>
        <div className='flex justify-content-end' >
            <button onClick={()=>{handleAPICall()}}>Click to fetch and save todays data for all 4 stocks</button>
        </div>
        </>

    )
}
export default SaveAllApi
