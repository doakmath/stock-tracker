
function SearchTable ({listData, dateView, handleDateView, singleDayList, showDate}) {

    return (
        <div>
            <div className='d-flex justify-content-center'>
            <h4>Select Day above on Calendar</h4>
            </div>
            <div className='d-flex justify-content-center'>
            <h4>Search by Time:</h4>
                <input onChange={handleDateView}/>
            <p>Example: 4:55:00 PM</p>
            </div>
            <table className="table table-striped table-custom m-3">
                <thead>
                    <tr>
                    <th>--Date-------------</th>
                        <th>----index----</th>
                        <th className="bg-secondary">----Timestamp----</th>
                        <th className="bg-primary">----Open----</th>
                        <th className="bg-success">----High----</th>
                        <th className="bg-warning">----Low----</th>
                        <th className="bg-info">----Close----</th>
                        <th className="bg-danger">----Volume----</th>
                    </tr>
                </thead>
                <tbody>
                {singleDayList && singleDayList
                    .filter(list => dateView === '' || (new Date(list.timestamp).toLocaleTimeString().includes(dateView) || new Date(showDate).toLocaleTimeString() === dateView))
                    .map(list => (
                    <tr key={list.id}>
                        <td>{new Date(list.timestamp).toLocaleDateString()}</td>
                        <td>{list.id}</td>
                        <td>{new Date(list.timestamp).toLocaleTimeString()}</td>
                        <td>{list.open}</td>
                        <td>{list.high}</td>
                        <td>{list.low}</td>
                        <td>{list.close}</td>
                        <td>{list.volume}</td>
                    </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default SearchTable
