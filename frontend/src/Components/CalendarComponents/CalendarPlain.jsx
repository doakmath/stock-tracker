import React, { useState } from 'react'
import Calendar from 'react-calendar';

function CalendarPlain() {

  const [clickDay, setClickDay] = useState(null)
  const [previousDay, setPreviousDay] = useState(null)

  const handleClickDay = (e) => {
    console.log("e:", e)
    const value = e
    setClickDay(value.toLocaleDateString())
  }

  const handlePreviousDay = (e, clickDay) => {
    setPreviousDay(clickDay)
    handleClickDay(e)
  }

  console.log("clickDay:", clickDay)

  return (
    <>
    <div style={{marginBottom:'6%'}}>
    </div>
    <div>
      <h1>Plain Calendar</h1>
    </div>
    <div>
      <div style={{marginBottom:'2%'}}>
        <Calendar
        onClickDay={(e)=>{handleClickDay(e)}}
        onChange={(e)=>{handlePreviousDay(e, clickDay)}}
        />
      </div>
      {previousDay !== null &&
      <div>
        <h1>Previous Choice: {previousDay}</h1>
      </div>
      }

      {clickDay !== null &&
      <div>
        <h1>Current Choice: {clickDay}</h1>
      </div>
      }
    </div>
    </>
  )
}

export default CalendarPlain;
