import React, { useState, useEffect } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import '../styles/CalendarReact.css'
import SearchTable from './SearchTable'
import CandleStick2 from '../GraphComponents/CandleStick2';
import MultiViewPage from '../GraphComponents/MultiViewPage';
import SaveAllApi from './SaveAllApi';
import StackedBarChart from '../GraphComponents/StackedBarChart';
import Notes from '../NotesComponents/Notes';
import NasaCarousel from '../NASAComponents/NasaCarousel';
import DoggieGame from '../DogsComponents/DoggieGame';
import ZenAPI from '../ZenComponents/ZenAPI';
import TSIGraphInteractive from '../GraphComponents/TSIGraphInteractive';
import TSIDynamicGraph from '../GraphComponents/TSIDynamicGraph'
import AllNotesPage from '../NotesComponents/AllNotesPage'


function MyCalendar() {
  const [selectedDate, setSelectedDate] = useState(new Date())
  const [listData, setListData] = useState([])
  const [showDate, setShowDate] = useState('')
  const [showData, setShowData] = useState({})
  const [singleDayList, setSingleDayList] = useState([])
  const [dateView, setDateView] = useState('')
  const [error, setError] = useState('no data exists')

  const [dataChoice, setDataChoice] = useState('getQQQ')

  const [seeTable, setSeeTable] = useState(false)
  const [seeCandlestick, setSeeCandlestick] = useState(false)
  const [seeAllCandlesticks, setSeeAllCandlesticks] = useState(false)
  const [seeStackedBarCharts, setSeeStackedBarCharts] = useState(false)
  const [seeNasaCarousel, setSeeNasaCarousel] = useState(false)
  const [seeDoggieGame, setSeeDoggieGame] = useState(false)
  const [seeZenAPI, setSeeZenAPI] = useState(false)
  const [seeExtra, setSeeExtra] = useState(false)
  const [seeTSIGraphInteractive, setSeeTSIGraphInteractive] = useState(false)
  const [seeTSIDynamicGraph, setSeeTSIDynamicGraph] = useState(false)
  const [seeAllNotes, setSeeAllNotes] = useState(false)


  const [colorToggle, setColorToggle] = useState(false)

  const [seeQQQ, setSeeQQQ] = useState(false)
  const [seeSPY, setSeeSPY] = useState(false)
  const [seeIWM, setSeeIWM] = useState(false)
  const [seeMSFT, setSeeMSFT] = useState(false)



  const separateGet = () => {
    if (dataChoice.startsWith('get')) {
        let remaining = dataChoice.slice('get'.length).trim();
        return remaining;
    } else {
        return null;
    }
  }

  const fetchData = async () => {
    const url = `http://localhost:8000/stocks/${dataChoice}/`
    const response = await fetch(url)
    try {
      if (response.ok) {
        let dataSelection = separateGet()
         if (dataSelection === 'Tsi'){
          dataSelection = 'tsi'
        }
        const listData = await response.json()
        setListData(listData[dataSelection])
      }
    } catch (error) {
        console.log("error!", error)
    }
  }

  useEffect(() => {
    fetchData()
  }, [dataChoice])

  useEffect(() => {
    handleReset()
  }, [dataChoice])



  const compareDates = (date, checkDate) => {
    const date1 = new Date(date).toLocaleDateString()
    const date2 = new Date(checkDate).toLocaleDateString()
    if (date1 === date2) {
      setShowDate(date1)
      return true
    } else {
      return false
    }

  }

  const handleDateClick = (date) => {
    setSelectedDate(date);
    let dataExists = false
    for(let list of listData) {
      const dateCheck = new Date(list['timestamp'])
      const formattedDate = dateCheck.toLocaleDateString('en-US', { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' });
      const formattedTime = dateCheck.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: true });
      const checkDate = formattedDate + ' ' + formattedTime;
      if (compareDates(date, checkDate)){
        const singleDayList = listData
          .map(list => list)
          .filter(list => new Date(list.timestamp).toLocaleDateString() === new Date(date).toLocaleDateString());
        setSingleDayList(singleDayList)
        setShowData(list)
        dataExists = true
      }
    }
    if (dataExists){
      setError('')
    } else {
      setError("no data exists")
    }
  }

  const handleReset = () => {
    if (dataChoice === 'getQQQ'){
      setSeeQQQ(true)
      setSeeSPY(false)
      setSeeIWM(false)
      setSeeMSFT(false)
    } else if (dataChoice === 'getSPY'){
      setSeeQQQ(false)
      setSeeSPY(true)
      setSeeIWM(false)
      setSeeMSFT(false)
    } else if (dataChoice === 'getIWM') {
      setSeeQQQ(false)
      setSeeSPY(false)
      setSeeIWM(true)
      setSeeMSFT(false)
    } else if ( dataChoice === 'getTsi') {
      setSeeQQQ(false)
      setSeeSPY(false)
      setSeeIWM(false)
      setSeeMSFT(true)
    }
  }

  const handleDateView = (e) => {
    const value = e.target.value
    setDateView(value)
  }

  const handleQQQChoice = () => {
    setDataChoice('getQQQ')
    handleReset()
  }

  const handleSPYChoice = () => {
    setDataChoice('getSPY')
    handleReset()
  }

  const handleIWMChoice = () => {
    setDataChoice('getIWM')
    handleReset()
  }

  const handleMSFTChoice = () => {
    setDataChoice('getTsi')
    handleReset()
  }

  const handleSeeTable = () => {
    if (seeTable === true) {
      setSeeTable(false)
    }
    if (seeTable === false) {
      setSeeTable(true)
    }
  }

  const handleSeeCandlestick = () => {
    if (seeCandlestick === true) {
      setSeeCandlestick(false)
    }
    if (seeCandlestick === false) {
      setSeeCandlestick(true)
    }
  }

  const handleSeeAllCandlesticks = () => {
    if (seeAllCandlesticks === true) {
      setSeeAllCandlesticks(false)
    }
    if (seeAllCandlesticks === false) {
      setSeeAllCandlesticks(true)
    }
  }

  const handleStackedBarCharts = () => {
    if (seeStackedBarCharts === true) {
      setSeeStackedBarCharts(false)
    }
    if (seeStackedBarCharts === false) {
      setSeeStackedBarCharts(true)
    }
  }

  const handleSeeNasaCarousel = () => {
    if (seeNasaCarousel === true) {
      setSeeNasaCarousel(false)
    }
    if (seeNasaCarousel === false) {
      setSeeNasaCarousel(true)
    }
  }

  const handleSeeDoggieGame = () => {
    if (seeDoggieGame === true) {
      setSeeDoggieGame(false)
    }
    if (seeDoggieGame === false) {
      setSeeDoggieGame(true)
    }
  }

  const handleSeeZenAPI = () => {
    if (seeZenAPI === true) {
      setSeeZenAPI(false)
    }
    if (seeZenAPI === false) {
      setSeeZenAPI(true)
    }
  }

  const handleSeeExtra = () => {
    if (seeExtra === true) {
      setSeeExtra(false)
    }
    if (seeExtra === false) {
      setSeeExtra(true)
    }
  }

  const handleSeeTSIGraphInteractive = () => {
    if (seeTSIGraphInteractive === true) {
      setSeeTSIGraphInteractive(false)
    }
    if (seeTSIGraphInteractive === false) {
      setSeeTSIGraphInteractive(true)
    }
  }

  const handleSeeTSIDynamicGraph = () => {
    if (seeTSIDynamicGraph === true) {
      setSeeTSIDynamicGraph(false)
    }
    if (seeTSIDynamicGraph === false) {
      setSeeTSIDynamicGraph(true)
    }
  }

  const handleSeeAllNotes = () => {
    if (seeAllNotes === true) {
      setSeeAllNotes(false)
    }
    if (seeAllNotes === false) {
      setSeeAllNotes(true)
    }
  }

  const handleSeeQQQ = () => {
    if (seeQQQ === true) {
      setSeeQQQ(false)
    }
    if (seeQQQ === false) {
      setSeeQQQ(true)
    }
  }

  const handleSeeSPY = () => {
    if (seeSPY === true) {
      setSeeSPY(false)
    }
    if (seeSPY === false) {
      setSeeSPY(true)
    }
  }

  const handleSeeIWM = () => {
    if (seeIWM === true) {
      setSeeIWM(false)
    }
    if (seeIWM === false) {
      setSeeIWM(true)
    }
  }

  const handleSeeMSFT = () => {
    if (seeMSFT === true) {
      setSeeMSFT(false)
    }
    if (seeMSFT === false) {
      setSeeMSFT(true)
    }
  }


  const tileContentStyle = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
    height: '50%',
  };


  const dotStyle = (color) => ({
    width: '12px',
    height: '12px',
    borderRadius: '100%',
    backgroundColor: color,
    margin: 'auto',
  });

  const toggleColor = (toggler) => {
    if (toggler === true) {
      return {
        // color: 'blue',
        // background: 'green',
        borderRadius: '30%',
      };
    }
    if (toggler === false) {
      return {
        color: 'black'
      }
    }
  }

  const hasDataForDate = (date) => {
    return listData.some(list => new Date(list.timestamp).toLocaleDateString() === date.toLocaleDateString());
  };



// console.log(dataChoice)



  return (
    <>
    <div style={{marginBottom: '6%'}}>
    </div>


    <div className='d-flex justify-content-left' style={{marginBottom: '1%'}}>
      <SaveAllApi />
      <button onClick={()=>{handleSeeExtra()}}>Extras</button>
      {seeExtra === true &&
        <div>
          <button onClick={()=>{handleSeeZenAPI()}}>Inspiration</button>
          <button  style={{marginBottom:'.5%'}} onClick={()=>{handleSeeDoggieGame()}}>Wesley's Favorite Doggie Game</button>
          <button onClick={()=>{handleSeeNasaCarousel()}}>Nasa Images</button>
        </div>
      }
    </div>


    <div>
      <div style={{marginBottom:'1%'}}>
        {seeNasaCarousel === true &&
            <NasaCarousel />
        }

        {seeZenAPI === true &&
            <ZenAPI />
        }

        {seeDoggieGame === true &&
            <DoggieGame />
        }
      </div>
    </div>

    <div>
      <div>
        <button style={toggleColor(seeQQQ)} onClick={()=>{handleQQQChoice()}}>QQQ</button>
        <button style={toggleColor(seeSPY)} onClick={()=>{handleSPYChoice()}}>SPY</button>
        <button style={toggleColor(seeIWM)} onClick={()=>{handleIWMChoice()}}>IWM</button>
        <button style={toggleColor(seeMSFT)} onClick={()=>{handleMSFTChoice(); fetchData()}}>MSFT</button>
      </div>
      <div style={{marginBottom:'1%'}}>
        <button style={toggleColor(seeTable)} onClick={()=>{handleSeeTable()}}>Table</button>
        <button style={toggleColor(seeCandlestick)} onClick={()=>{handleSeeCandlestick()}}>Single Candlestick</button>
        <button style={toggleColor(seeAllCandlesticks)} onClick={handleSeeAllCandlesticks}>All Candlesticks</button>
        <button style={toggleColor(seeStackedBarCharts)} onClick={()=>{handleStackedBarCharts()}}>Stacked BarCharts</button>
        <button style={toggleColor(seeTSIGraphInteractive)} onClick={handleSeeTSIGraphInteractive}>Split View/Comparisons</button>
        <button style={toggleColor(seeTSIDynamicGraph)} onClick={handleSeeTSIDynamicGraph}>Dynamic Line Graph</button>
      </div>
      <div>
        <div style={{ maxWidth: '100%' }}>
          <div className='d-flex' style={{marginBottom: '1%'}}>
            <Calendar
              onChange={setSelectedDate}
              onClickDay={handleDateClick}
              tileContent={({ date }) => (
                <div className='flex-d flex-container' style={tileContentStyle} >
                  {hasDataForDate(date) && <div style={dotStyle('green')}></div>}
                </div>
              )}
            />
          {error === 'no data exists' && <h3>{error} {selectedDate.toLocaleDateString()}</h3>}
          <div className='d-flex container align-items-center'>
            <h1 style={{marginRight: '5%'}}>{(dataChoice && separateGet())}</h1>
            <h1>{(selectedDate).toLocaleDateString()}</h1>
            {/* {dataChoice &&
              <div>
              <CandleStick2 dataChoice={dataChoice} selectedDate={selectedDate}/>
              </div>
            } */}
            </div>
          </div>

          <div>
            <div>
              <button onClick={()=>{handleSeeAllNotes()}}>See All Notes</button>
            </div>
            {seeAllNotes === true &&
            <div >
              <Notes />
              <AllNotesPage />
            </div>
            }
          </div>
        </div>

      <div>
        <div className="d-flex justify-content-center align-items-center">
        <h1>
          {error && <div>{error} for {selectedDate.toLocaleDateString()}</div>}
        </h1>
        </div>
      </div>

      </div>
      <div style={{marginBottom:'1%'}}>
        {seeTable === true &&
          <div>
            <SearchTable listData={listData} dateView={dateView} handleDateView={handleDateView} singleDayList={singleDayList} showDate={showDate}/>
          </div>
        }

        {seeCandlestick === true &&
          <div>
            <CandleStick2 dataChoice={dataChoice} selectedDate={selectedDate}/>
          </div>
        }

        {seeAllCandlesticks === true &&
          <div>
            <MultiViewPage />
          </div>
        }

        {seeStackedBarCharts === true &&
          <div>
            <StackedBarChart />
          </div>
        }

        {seeTSIGraphInteractive === true &&
          <div>
            <TSIGraphInteractive dataChoice={dataChoice}/>
          </div>
        }

        {seeTSIDynamicGraph === true &&
          <div>
            <TSIDynamicGraph dataChoice={dataChoice} selectedDate={selectedDate}/>
          </div>
        }
      </div>
    </div>
    </>
  )
}
export default MyCalendar;
