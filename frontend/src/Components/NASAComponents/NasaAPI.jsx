import { Link } from 'react-router-dom'
import {useState, useEffect } from 'react'
// import NASA from 'Components/images'


function NasaAPI () {

    const[nasa, setNasa] = useState([1,1,1])

    const fetchNasaData = async () => {
        const fetchOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const url = 'https://api.nasa.gov/planetary/apod?api_key=BcZGe9R2NLUcKNNQqQ3mQhlLYHHEl0StHVfec0VD';
        const response = await fetch(url, fetchOptions);
        try {
            if (response.ok) {
                const data = await response.json();
                setNasa(data)
                saveNasa(data)
            } else {
                throw new Error('Failed to fetch data');
            }
        } catch (error) {
            console.error("Error retrieving NASA data:", error);
        }
        alert("You just submitted an NASAAPI call successfully!")
    };

    useEffect(()=>{fetchNasaData()},[])

    async function saveNasa(nasa) {
        // console.log("nasa----------------------", nasa)
        // console.log("type of nasa", typeof nasa)
        try {
          const url = 'http://localhost:8000/pictures/createNasa'
          const fetchOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(nasa)
          }
          const response = await fetch(url, fetchOptions)
          console.log("Response", response)

          if (!response.ok) {
            throw new Error('Failed to submit data-Nasa');
          }
          console.log('Nasa submitted successfully:', nasa);
        } catch (error) {
          console.error('Error submitting Nasa:', error);
        }
      }






    return(
        <>
        <div style={{marginBottom: "6%"}}>
        </div>
        <div>
            <div>
                <h1>{nasa.title}</h1>
            </div>
            <div>
                <h3>{nasa.date}</h3>
            </div>
            <div>
                <h5>{nasa.explanation}</h5>
            </div>
            <div>
                Copyright {nasa.copyright}
            </div>
            <img src={nasa.hdurl} alt="NASA daily" height="850"></img>

            {/* <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {nasaList.map((image, index) => (
                    <img key={index} src={image.url} alt={`${index}`} height={500} width={500} style={{ margin: '10px' }} />
                    ))}
                </div> */}

        </div>
                {/* <button onClick={handleClick}>Click for NASA Pic of the Day</button> */}
        </>
    )
}

export default NasaAPI
