import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Carousel from 'react-bootstrap/Carousel'
import './NasaCarousel.css'

function NasaCarousel () {

    const [nasaPictures, setNasaPictures] = useState([])
    const[nasa, setNasa] = useState([1,1,1])

    useEffect(() => {
        getPictures()
    }, [])

    const getPictures = async () => {
        const url = 'http://localhost:8000/pictures/listNasa';
        try {
            const response = await fetch(url);
            const result = await response.json()
            const pictureList = result.nasa.map(nasa => nasa.hdurl)
            setNasaPictures(pictureList)
        } catch (error) {
            console.error(error);
        }
    }

    const fetchNasaData = async () => {
        const fetchOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const url = 'https://api.nasa.gov/planetary/apod?api_key=BcZGe9R2NLUcKNNQqQ3mQhlLYHHEl0StHVfec0VD';
        const response = await fetch(url, fetchOptions);
        try {
            if (response.ok) {
                const data = await response.json();
                setNasa(data)
                saveNasa(data)
            } else {
                throw new Error('Failed to fetch data');
            }
        } catch (error) {
            console.error("Error retrieving NASA data:", error);
        }
        // alert("You just submitted an NASAAPI call successfully!")
    };

    useEffect(()=>{fetchNasaData()},[])

    async function saveNasa(nasa) {
        // console.log("nasa----------------------", nasa)
        // console.log("type of nasa", typeof nasa)
        try {
          const url = 'http://localhost:8000/pictures/createNasa'
          const fetchOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(nasa)
          }
          const response = await fetch(url, fetchOptions)
          console.log("Response", response)

          if (!response.ok) {
            throw new Error('Failed to submit data-Nasa');
          }
          console.log('Nasa submitted successfully:', nasa);
        } catch (error) {
          console.error('Error submitting Nasa:', error);
        }
      }

    return (
        <>
        {/* <div className="black-background"style={{marginBottom: '10%'}}>
        </div> */}
        <div className="d-flex justify-content-center" style={{marginBottom:'1%'}}>
            {/* <h5>nasa images</h5> */}
        </div>
        <div className="d-flex justify-content-center">
            <div className="carousel-container" style={{ width: '75%'}}>
                <Carousel width="100">
                    {nasaPictures && nasaPictures.map((nasa, index)=>{
                        return(
                            <Carousel.Item key={index}>
                                {/* <p>{nasa}</p> */}
                                <img
                                    className="d-block w-100"
                                    style={{ height: '600px', marginBottom:'1%', borderRadius: '50%'}}
                                    src={nasa}
                                    alt="Nasa slide"
                                />
                            </Carousel.Item>
                        )
                        })}
                </Carousel>
            </div>
        </div>
        <div style={{marginBottom: '60%'}}>
        </div>
            </>
    )
}

export default NasaCarousel
