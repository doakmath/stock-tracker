import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function NasaHouse () {

    const [nasa, setNasa] = useState([])


    const fetchData = async () => {
        const url = "http://localhost:8000/pictures/listNasa"
        const response = await fetch(url);
        try {
            if (response.ok) {
            const data = await response.json()
            setNasa(data.nasa)
            }
        } catch (error) {
            console.error("Error retrieving fetchData-nasaList", error)
        }
    }

    useEffect(() => {
        fetchData()
    }, []);


    async function deleteNasa(id) {
        const url = `http://localhost:8000/pictures/deleteNasa/${id}`
        const fetchConfig = {method: "DELETE"}
        const response = await fetch(url, fetchConfig)
        try {
            if (response.ok) {
                fetchData()
            }
        } catch (error) {
            console.error("deletion error, perhaps it doesn't exist", error)
        }
    }

console.log("nasa list", nasa)

    return (
        <>
        <div style={{marginBottom: '6%'}}>
        </div>
        <div>
            <h1>Previous Nasa Pics of the Day</h1>
        </div>
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        {nasa.map((item, index) => (
            <div key={index} style={{ border: '1px solid #ccc', borderRadius: '5px', padding: '10px', margin: '10px', marginBottom: '1px', width: 'calc(33.33% - 22px)' }}>
                <h2 style={{ fontSize: '1.5rem', fontWeight: 'bold' }}>{item.title}</h2>
                <p style={{ fontSize: '1rem', marginBottom: '5px' }}>{item.date}</p>
                <img src={item.hdurl} alt={item.title} style={{ width: '100%', height: 'auto' }} />
                <p>{item.copyright}</p>
                <p>{item.explanation}</p>
                <button onClick={() => deleteNasa(item.id)}>Delete</button>
            </div>
            ))}
        </div>
        </>
    )
}

export default NasaHouse
