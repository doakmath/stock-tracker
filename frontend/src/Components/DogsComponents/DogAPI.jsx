import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
// import './DogAPI.css'

function DogAPI () {

  const [dogs, setDogs] = useState([])

  const fetchData = async () => {
    const url = "https://dog.ceo/api/breeds/image/random/2"
    const response = await fetch(url);
    try {
      if (response.ok) {
        const data = await response.json()
        setDogs(data["message"])
        saveDog(data["message"][0])
      }
    } catch (error) {
        console.error("Error retrieving sales: fetchData", error)
    }
  }

  useEffect(() => {
    fetchData()
  }, []);

  async function saveDog(dog) {
    try {
      const url = 'http://localhost:8000/pictures/createPicture'
      const fetchOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(dog)
      }
      const response = await fetch(url, fetchOptions)
      // console.log("Response", response)

      if (!response.ok) {
        throw new Error('Failed to submit data');
      }
      console.log('Dog submitted successfully:', dog);
    } catch (error) {
      console.error('Error submitting dog:', error);
    }
    // fetchData()
  }

  async function saveDog2(dog) {
    try {
      const url = 'http://localhost:8000/pictures/createFav'
      const fetchOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(dog)
      }
      const response = await fetch(url, fetchOptions)
      console.log("Response2222222222", response)

      if (!response.ok) {
        throw new Error('Failed to submit data');
      }
      console.log('Dog submitted successfully:', dog);
    } catch (error) {
      console.error('Error submitting dog:', error);
    }
    fetchData()
  }

  async function saveDogs (dogs) {
    for (const dog of dogs) {
      await saveDog(dog);
      await saveDog2(dog); // XXX Added to save as favorite
    }
  }

  return (
    <>
      <div>
        <div>
          {/* <Link to="/"><button>HOME</button></Link>
          <Link to="/pictures"><button>PICTURES</button></Link>
          <Link to="/picturehouse"><button>all the pictures</button></Link>
          <Link to="/dogs"><button>DOGGIES!</button></Link>
          <Link to="/dogfavs"><button>Favorite Doggies</button></Link>
          <Link to="/doghouse"><button>DogHouse</button></Link>
          <Link to="/nasa"><button>NASA Daily Pic</button></Link>
          <Link to="/nasahouse"><button>NASA Previous Pics</button></Link>
          <Link to="/zen"><button>MOMENT OF ZEN</button></Link> */}
        </div>
      </div>
      <div>
        <h1>``</h1>
      </div>
      <div className="container">
        <div>
              <Link to="/dogfavs">
                <button
                  style={{
                    padding: '15px 30px 30px', // Add more padding on the bottom
                    fontSize: '18px', // Keep button size
                    backgroundColor: 'green', // Keep button color
                    color: 'white', // Keep text color
                    border: 'none', // Remove border
                    borderRadius: '50%', // Very round edges
                    fontFamily: 'Arial, sans-serif', // Keep font family
                    fontWeight: 'bold', // Keep font weight
                    textTransform: 'uppercase' // Keep text transformation
                  }}
                >
                    Click to see ALL THE DOGGIES in the DOGHOUSE!!!
                    <Link to="/doghouse">
                      <button
                        style={{
                          padding: '35px 30px 20px', // Add more padding on the bottom
                          fontSize: '18px', // Keep button size
                          backgroundColor: 'pink', // Keep button color
                          color: 'white', // Keep text color
                          border: 'none', // Remove border
                          borderRadius: '50px', // Very round edges
                          fontFamily: 'Arial, sans-serif', // Keep font family
                          fontWeight: 'bold', // Keep font weight
                          textTransform: 'uppercase' // Keep text transformation
                        }}
                      >
                        Click to see your FAVORITE DOGGIES!!!
                      </button>
                    </Link>
                </button>
              </Link>
                          <button
                            onClick={() => saveDogs(dogs)}
                            style={{
                              padding: '30px 30px 30px', // Add more padding on the bottom
                              fontSize: '18px', // Keep button size
                              backgroundColor: 'blue', // Keep button color
                              color: 'white', // Keep text color
                              border: 'none', // Remove border
                              borderRadius: '100%', // Very round edges
                              fontFamily: 'Arial, sans-serif', // Keep font family
                              fontWeight: 'bold', // Keep font weight
                              textTransform: 'uppercase' // Keep text transformation
                            }}
                          >
                            Click to REFRESH!!!
                              </button>
        </div>
        <table>
          <thead>
            <tr>
              {dogs.map((dog, index) => (
                <th key={index}><strong>{index + 1}</strong></th>
              ))}
            </tr>
          </thead>
          <tbody>
            <tr>
              {dogs.map((dog, index) => (
                <td key={index}>
                  <img src={dog} alt="doggie" height="300" style={{ marginRight: '10px', marginBottom: '10px' }} />
                  {/* XXX Added button to save dog as favorite */}
                </td>
              ))}
            </tr>
            <tr>
              {dogs.map((dog, index) => (
                <td key={index}>
                  <button
                    onClick={() => saveDog2(dog)}
                    style={{
                      padding: '10px 20px', // Adjust padding
                      backgroundColor: '#c7e7c7', // Pastel green background color
                      color: '#ff9933', // Light orange font color
                      border: 'none', // Remove border
                      borderRadius: '20px', // Rounded edges
                      fontFamily: 'Arial, sans-serif', // Keep font family
                      fontWeight: 'bold', // Keep font weight
                      textTransform: 'uppercase' // Keep text transformation
                    }}
                  >
                    Pick Me!  WooF!
                  </button>
                </td>
              ))}
            </tr>
          </tbody>
        </table>
      </div>
    </>
  )
}

export default DogAPI













// function DogAPI () {

//   const [dog, setDog] = useState([]);

//   const fetchData = async () => {
//     try {
//       const response = await fetch('https://dog.ceo/api/breeds/image/random/5');
//       const data = await response.json();
//       const newDogs = data.message;
//       setDog(newDogs);
//       await saveDogs(newDogs);
//     } catch (error) {
//       console.error('Error fetching dogs:', error);
//     }
//   };


//   try {
//     const response = await fetch('http://your-api-endpoint.com/pictures/', {
//       method: 'POST',
//       headers: { 'Content-Type': 'application/json' },
//       body: JSON.stringify(formData)
//     });

//     if (!response.ok) {
//       throw new Error('Failed to submit data');
//     }

//     console.log('Dog submitted successfully:', dog);
//   } catch (error) {
//     console.error('Error submitting dog:', error);
//     alert('Failed to submit dog:', dog);
//   }
// };

// useEffect(() => {
//   fetchData();
// }, []);

// const saveDog = async (dog) => {
//   const formData = { image: dog };

// const saveDogs = async (dogs) => {
//   for (const dog of dogs) {
//     await saveDog(dog);
//   }
// };

// return (
//   <>
//       <div>
//         <Link to="/"><button>HOME</button></Link>
//         <Link to="/pictures"><button>PICTURES</button></Link>
//         <Link to="/dogs"><button>DOGGIES!</button></Link>
//       </div>
//       <div>
//         <h1>This is a page for DOGGIES!</h1>
//       </div>
//       <div>
//         <button onClick={fetchData}>Click for more DOGGIES!!!</button>
//       </div>
//       <div className="container">
//         <div className="image-container">
//           {dog && dog.map((doggie, index) => (
//             <img key={index} src={doggie} alt="doggie" height="300" style={{ marginRight: '10px', marginBottom: '10px' }} />
//           ))}
//         </div>
//       </div>
//     </>
//   );
// }






//PREVIOUS CODE----------------------------
// return (
//   <>
//     <div>
//       <Link to="/"><button>HOME</button></Link>
//       <Link to="/pictures"><button>PICTURES</button></Link>
//       <Link to="/dogs"><button>DOGGIES!</button></Link>
//     </div>
//     <div>
//       <h1>This is a page for DOGGIES!</h1>
//     </div>
//       <div>
//           <button onClick={ handleSubmitDogs(dog) }>Click for more DOGGIES!!!</button>
//       </div>
//     <div className="container">
//       <div className="image-container">
//           {dog.map((doggie, index) => (
//             <img key={index} src={doggie} alt="doggie" height="300" style={{ marginRight: '10px', marginBottom: '10px' }} />
//             ))}
//       </div>
//     </div>
//   </>
// );


//PREVIOUS CODE BEFORE REFACTOR--------------------
//   const [formData, setFormData] = useState({
//     image: ''
//   });

// const handleSubmit = async (event, dog) => {
//     // event.preventDefault();
//     setFormData(dog)
//     try {
//         const response = await fetch('http://localhost:8000/pictures/createPicture', {
//             method: 'POST',
//             headers: {
//                 'Content-Type': 'application/json'
//             },
//             body: JSON.stringify(formData)
//         });

//         if (!response.ok) {
//             throw new Error('Failed to submit data');
//         }

//         // Clear form data
//         setFormData({
//             image: ''
//         });

//         alert('Data submitted successfully');
//     } catch (error) {
//         console.error('Error:', error);
//         alert('Failed to submit data');
//     }
//     getDogs()
// };


// const handleSubmitDogs = (dog) => {
//   dog.forEach(dog => {
//       handleSubmit(dog);
//   });
// }



//   const [dog, setDog] = useState([1,1,1])

//   const getDogs = async () => {
//       const url = 'https://dog.ceo/api/breeds/image/random/5';
//       const options = {
//           method: 'GET'
//       }
//       try {
//           const response = await fetch(url, options);
//           const result = await response.json()
//           console.log("RESULT", result);
//           setDog(result["message"])

//       } catch (error) {
//           console.error(error);
//       }
//   }

//   useEffect(() => {
//       getDogs()
//   }, [])

// console.log("dog------------------", dog)
