import React from 'react';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';


const DoggieGame = () => {
    const [doggieCounter, setDoggieCounter] = useState(0)
    const [dogs, setDogs] = useState([])
    const [breed1, setBreed1] = useState('')
    const [breed2, setBreed2] = useState('')
    const [change, setChange] = useState(false)
    const [allDogs, setAllDogs] = useState([])
    const [wesley, setWesley] = useState(false)


    useEffect(() => {
      fetchAllDogsData()
    }, [])

    const fetchAllDogsData = async () => {
        const url = "http://localhost:8000/pictures/listFav"
        const response = await fetch(url);
        try {
            if (response.ok) {
            const data = await response.json()
            setAllDogs(data.fav)
          }
        } catch (error) {
          console.error("Error retrieving fetchData", error)
        }
      }
      // console.log("allDogs", allDogs)

    const generateRandomNumber = () => {
      return (Math.random() * 10)
    }

    const handleClick = () => {
        setDoggieCounter(doggieCounter + 1)
    }

    const handleReset = () => {
        setDoggieCounter(0)
    }

    const handleResetBreeds = () => {
        setBreed1('')
        setBreed2('')
    }

    const handleBreed1 = (url) => {
        const extractedBreed = extractBreedFromImageUrl(url)
        setBreed1(extractedBreed)
    }

    const handleBreed2 = (url) => {
        const extractedBreed = extractBreedFromImageUrl(url)
        setBreed2(extractedBreed)
    }

    const extractBreedFromImageUrl = (url) => {
        const parts = url.split('/');
        // Find the index of the part containing 'breeds'
        const breedsIndex = parts.findIndex(part => part === 'breeds');
        // Extract the breed name from the next part after 'breeds'
        if (breedsIndex !== -1 && parts.length > breedsIndex + 1) {
            return parts[breedsIndex + 1];
        } else {
            return 'Breed not found';
        }
    }

    const fetchData = async () => {
        if (doggieCounter < 6) {
            const url = "https://dog.ceo/api/breeds/image/random/2"
            const response = await fetch(url);
            try {
            if (response.ok) {
                const data = await response.json()
                console.log("---------------data from api call--------------------", data)
                setDogs(data["message"])
                // saveFirstDog(dogs[0])
                // saveDog(data["message"][0])
                // alert("You made an API call!")
            }
            } catch (error) {
                console.error("Error retrieving sales: fetchData", error)
            }
        } else {
            const url = "http://localhost:8000/pictures/listFav"
            const response = await fetch(url)
            try {
                if (response.ok){
                    const data = await response.json()
                    console.log("---------data from local database---------", data)
                    const dataAsList = []
                    for (let i=0; i < 2; i++) {
                        dataAsList.push(data.fav[i])
                    }

                    // for (let obj of data['fav']) {
                    //     dataAsList.push(obj['image'])
                    // }

                    console.log("dataAsList", dataAsList)
                    setDogs(dataAsList)
                }
            } catch (error) {
                console.log("Error retrieving Favs")
            }
        }
    }

    async function deleteDog(dog) {
        console.log("dog", dog)
        const url = `http://localhost:8000/pictures/deleteFav/${dog.id}`
        const fetchConfig = {method: "DELETE"}
        const response = await fetch(url, fetchConfig)
        try {
            if (response.ok) {
                if (doggieCounter < 13){
                    fetchData()
                    handleClick()
                }
            }
        } catch (error) {
            console.error("deletion error, perhaps it doesn't exist", error)
        }
    }


    async function saveDog(dog) {
        try {
            const url = 'http://localhost:8000/pictures/createFav'
            const fetchOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(dog)
          }
          const response = await fetch(url, fetchOptions)
          console.log("Response", response)
        //   alert("You just saved a Doggie")
          fetchData()
          handleResetBreeds()
          handleClick()
          if (!response.ok) {
            throw new Error('Failed to submit data');
          }
          console.log('Dog NOT submitted successfully:', dog);
        } catch (error) {
          console.error('Error submitting dog:', error);
        }
        // fetchData()
    }

    async function saveFirstDog(dog) {
        try {
            const url = 'http://localhost:8000/pictures/createFav'
            const fetchOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(dog)
          }
          const response = await fetch(url, fetchOptions)
          console.log("Response", response)
        //   alert("You just saved a Doggie")
          if (!response.ok) {
            throw new Error('Failed to submit data');
          }
          console.log('Dog NOT submitted successfully:', dog);
        } catch (error) {
          console.error('Error submitting dog:', error);
        }
    }



const handleChange = () => {
  if (change === false) {
    setChange(true)
  } else if (change === true) {
    setChange(false)
  }
}

const handleWesley = () => {
  if (wesley === false) {
    setWesley(true)
  } else if (wesley === true) {
    setWesley(false)
  }
}


console.log('change', change)
    // console.log("------------dogs0----------", dogs.length > 0 ? dogs[0] : null);
    // console.log("------------dogs1----------", dogs.length > 1 ? dogs[1] : null);
// console.log("dogs", dogs)


  return (
    <>
    <div style={{marginBottom:'6%'}}>
    </div>
    <h1 className="d-flex justify-content-center">Wesley's Favorite Doggie Game</h1>
    <button onClick={()=>{handleChange()}}>Current Favorite Doggie!</button>
    <button onClick={()=>{handleWesley()}}>A word from Wesley</button>
    {wesley === true &&
      <div>
        <p>(get an API call from a Random Quote API; find image of corgi?  cartoon corgi?)</p>
        <p>hi there...welcome to Wesley's DoggieGame!  To play click Play DoggieGame below.  In ROUND 1 you CHOOSE your favorite of the two.</p>
        <p>In ROUND 2 you compare all the chosen doggies AND the Current Favorite.  Set loose the one you don't want to keep until you find your </p>
        <p>FAVORITEST DOGGIE!</p>
      </div>
      }
    {change === true &&
      <div className='d-flex justify-content-center'>
        {allDogs[0].id}
        <img src={allDogs[0].image}></img>
      </div>}
    {/* <h2>Clicks {doggieCounter}</h2>
    <button onClick={handleClick}>Counter Button</button>
    <button onClick={handleReset}>Reset Counter</button> */}
    <div className="doggie-game-container">
      <div className="button-container d-flex justify-content-center">
        {/* <Link to='/DogHouse'><button>Browse Doggies!</button></Link> */}
      </div>
      <div className="centered-button-container d-flex justify-content-center">
        {doggieCounter === 0 && <button onClick ={() =>{fetchData(); handleClick()}}className="centered-button">Play Doggie Game!</button>}
        {doggieCounter === 13 && <button onClick ={() =>{handleReset()}}className="centered-button">Replay</button>}
      </div>
      {doggieCounter < 7 && doggieCounter !== 0 &&
        <>
        <h1 className="d-flex justify-content-center text-success">Round 1</h1>
        <h3 className="d-flex justify-content-center text-primary"><button style={{
                            padding: '10px 20px', // Adjust padding
                            backgroundColor: '#c7e7c7', // Pastel green background color
                            color: '#ff9933', // Light orange font color
                            border: 'none', // Remove border
                            borderRadius: '20px', // Rounded edges
                            fontFamily: 'Arial, sans-serif', // Keep font family
                            // fontWeight: 'bold', // Keep font weight
                            textTransform: 'uppercase' // Keep text transformation
                        }}>pick your favorite</button></h3>
        </>
        }
      {doggieCounter >= 7 && doggieCounter <= 12 &&
        <>
        <h3 className="d-flex justify-content-center text-danger">Round 2</h3>
        <h3 className="d-flex justify-content-center text-warning"><button style={{
                                padding: '10px 20px', // Adjust padding
                                backgroundColor: 'red', // Pastel green background color
                                color: '#ff9933', // Light orange font color
                                border: 'none', // Remove border
                                borderRadius: '20px', // Rounded edges
                                fontFamily: 'Arial, sans-serif', // Keep font family
                                fontWeight: 'bold', // Keep font weight
                                textTransform: 'uppercase' // Keep text transformation
                            }}>select one to get rid of...</button></h3>
        </>
      }
      {doggieCounter === 13 &&
      <>
      <h1 className="d-flex justify-content-center">Your Favoritest Doggie!!!</h1>
      <div className="d-flex justify-content-center">
      <img src={dogs[0].image} alt="favorite" ></img>
      </div>
      </>
        }
      <div className="d-flex justify-content-center">
        {doggieCounter < 13 &&
      <table>
          <thead>
            <tr>
              {/* {dogs.map((dog, index) => (
                <th key={index}><strong>{index}</strong></th>
              ))} */}
            </tr>
          </thead>
          <tbody>

            <tr>
              {dogs.map((dog, index) => (
                <td key={index}>

                    {doggieCounter < 7 && dog &&
                        <img src={dog} alt="doggie" height="300" style={{ marginRight: '10px', marginBottom: '10px' }} />
                    }

                    {doggieCounter >= 7 && dog &&
                        <img src={dog.image} alt="doggie" height="300" style={{ marginRight: '10px', marginBottom: '10px' }} />
                    }

                </td>
              ))}
            </tr>
            <tr>
            {dogs.map((dog, index) => (
                // {console.log("dog in for loop onClick", dog)}
                <td key={index}>
                    {doggieCounter < 7 &&
                    <button
                        onClick={() => {
                            saveDog(dog);
                        }}
                        style={{
                            padding: '10px 20px', // Adjust padding
                            backgroundColor: '#c7e7c7', // Pastel green background color
                            color: '#ff9933', // Light orange font color
                            border: 'none', // Remove border
                            borderRadius: '20px', // Rounded edges
                            fontFamily: 'Arial, sans-serif', // Keep font family
                            fontWeight: 'bold', // Keep font weight
                            textTransform: 'uppercase' // Keep text transformation
                        }}
                    >
                    Round 1: Pick Me!
                    </button>}
                    {doggieCounter >= 7 && doggieCounter < 13 &&
                        <button
                            onClick={() => {
                                if (doggieCounter >= 7 && doggieCounter < 13) {
                                deleteDog(dog);
                                }
                            }}
                            style={{
                                padding: '10px 20px', // Adjust padding
                                backgroundColor: 'red', // Pastel green background color
                                color: '#ff9933', // Light orange font color
                                border: 'none', // Remove border
                                borderRadius: '20px', // Rounded edges
                                fontFamily: 'Arial, sans-serif', // Keep font family
                                fontWeight: 'bold', // Keep font weight
                                textTransform: 'uppercase' // Keep text transformation
                            }}
                            >
                            Round 2: Set Me Loose?
                            </button>
                    }
                </td>
                ))}
            </tr>
            <tr>
                <td>
                {breed1 && <div>{breed1}</div>}
                    {doggieCounter > 0 &&  doggieCounter <7 &&
                      <div>
                        <button style={{
                        padding: '10px 20px', // Adjust padding
                        backgroundColor: '#c7e7c7', // Pastel green background color
                        color: '#ff9933', // Light orange font color
                        border: 'none', // Remove border
                        borderRadius: '20px', // Rounded edges
                        fontFamily: 'Arial, sans-serif', // Keep font family
                        // fontWeight: 'bold', // Keep font weight
                        // textTransform: 'uppercase' // Keep text transformation
                    }} onClick={() => {handleBreed1(dogs[0])}}>Psst...wanna know what kind of Doggie I am??</button>
                      </div>
                      }



                </td>
                <td>
                {breed2 && <div>{breed2}</div>}
                    {doggieCounter > 0 && doggieCounter <7 &&
                    <div>
                      <button style={{
                      padding: '10px 20px', // Adjust padding
                      backgroundColor: '#c7e7c7', // Pastel green background color
                      color: '#ff9933', // Light orange font color
                      border: 'none', // Remove border
                      borderRadius: '20px', // Rounded edges
                      fontFamily: 'Arial, sans-serif', // Keep font family
                      // fontWeight: 'bold', // Keep font weight
                      // textTransform: 'uppercase' // Keep text transformation
                  }} onClick={() => {handleBreed2(dogs[1])}}>Psst...wanna know what kind of Doggie I am??</button>
                      </div>
                      }
                </td>
            </tr>
          </tbody>
        </table>
}
        </div>
      <div className ='d-flex justify-content-center'>
      <div className="image-container d-flex">
      </div>
      </div>
    </div>
    <div style={{marginBottom: '50%'}}></div>
    </>
  );
};

export default DoggieGame;
