import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';


function DogFavs () {

    const [favs, setFavs] = useState([])

    const fetchData = async () => {
        const url = "http://localhost:8000/pictures/listFav"
        const response = await fetch(url);
        try {
            if (response.ok) {
            const data = await response.json()
            setFavs(data.fav)
            }
        } catch (error) {
            console.error("Error retrieving fetchData", error)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])


    async function deletePicture(id) {
        const url = `http://localhost:8000/pictures/deleteFav/${id}`
        const fetchConfig = {method: "DELETE"}
        const response = await fetch(url, fetchConfig)
        try {
            if (response.ok) {
                fetchData()
            }
        } catch (error) {
            console.error("deletion error, perhaps it doesn't exist", error)
        }
    }


    return (
        <>
        <div style={{marginBottom: '120px'}}>
        {/* <Link to="/"><button>HOME</button></Link>
        <Link to="/pictures"><button>PICTURES</button></Link>
        <Link to="/picturehouse"><button>all the pictures</button></Link>
        <Link to="/dogs"><button>DOGGIES!</button></Link>
        <Link to="/dogfavs"><button>Favorite Doggies</button></Link>
        <Link to="/doghouse"><button>DogHouse</button></Link>
        <Link to="/nasa"><button>NASA Daily Pic</button></Link>
        <Link to="/nasahouse"><button>NASA Previous Pics</button></Link>
        <Link to="/zen"><button>MOMENT OF ZEN</button></Link> */}
        {/* <h1>`</h1> */}
      </div>
      <Link to="/doghouse"><button style={{ backgroundColor: 'green', color: 'white', borderRadius: '50%', width: '90px', height: '81px', marginTop: '5px', fontSize: '12px', padding: '5px' }}>DogHouse</button></Link>
      <Link to="/dogs"><button style={{ backgroundColor: 'green', color: 'white', borderRadius: '50%', width: '90px', height: '78px', marginTop: '5px', fontSize: '12px', padding: '5px' }}>back to DOGGIES</button></Link>
      <div>
        <button style={{ backgroundColor: 'red', color: 'white', borderRadius: '50%', width: '40px', height: '40px', marginTop: '5px' }}></button>hit red button for delete
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {favs.map((fav, index) => (
                <div key={index} style={{ margin: '10px', textAlign: 'center' }}>
                <img src={fav.image} alt={`Favorite Dog ${index}`} style={{ width: '200px', height: '200px', borderRadius: '50%' }} />
                <button onClick={() => deletePicture(fav.id)} style={{ backgroundColor: 'red', color: 'white', borderRadius: '50%', width: '40px', height: '40px', marginTop: '5px' }}>{fav.id}</button>
                </div>
            ))}
            </div>
        </div>
        </>
    )
}

export default DogFavs
