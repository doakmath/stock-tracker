import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function DogHouse () {

    const [pictures, setPictures] = useState([])


    const fetchData = async () => {
        const url = "http://localhost:8000/pictures/listPictures/"
        const response = await fetch(url);
        try {
            if (response.ok) {
            const data = await response.json()
            setPictures(data.pictures)
            }
        } catch (error) {
            console.error("Error retrieving fetchData", error)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    async function saveFav(picture) {
        try {
          const url = 'http://localhost:8000/pictures/createFav'
          const fetchOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(picture.image)
          }
          const response = await fetch(url, fetchOptions)
          console.log("Response", response)
          alert("Doggie was saved!  Check out FAVS to see!")
          if (!response.ok) {
            throw new Error('Failed to submit data')
          }
          console.log('Fav Added submitted successfully:', picture)
        } catch (error) {
          console.error('Error submitting dog:', error)
        }
        // fetchData()
      }


    async function deletePicture(id) {
        const url = `http://localhost:8000/pictures/deletePicture/${id}`
        const fetchConfig = {method: "DELETE"}
        const response = await fetch(url, fetchConfig)
        try {
            if (response.ok) {
                fetchData()
            }
        } catch (error) {
            console.error("deletion error, perhaps it doesn't exist", error)
        }
    }

    async function handleDeleteAll() {
        const url = `http://localhost:8000/pictures/deleteAllPictures`
        const fetchConfig = {method: "DELETE"}
        const response = await fetch(url, fetchConfig)
        try {
            if (response.ok) {
                fetchData()
            }
        } catch (error) {
            console.error("deletion error, perhaps it doesn't exist", error)
        }
    }

console.log("dogs list", pictures)

    return (
        <>
        <div style = {{marginBottom: '70px'}}>
            {/* <Link to="/"><button>HOME</button></Link>
            <Link to="/pictures"><button>PICTURES</button></Link>
            <Link to="/picturehouse"><button>all the pictures</button></Link>
            <Link to="/dogs"><button>DOGGIES!</button></Link>
            <Link to="/dogfavs"><button>Favorite Doggies</button></Link>
            <Link to="/doghouse"><button>DogHouse</button></Link>
            <Link to="/nasa"><button>NASA Daily Pic</button></Link>
            <Link to="/nasahouse"><button>NASA Previous Pics</button></Link>
            <Link to="/zen"><button>MOMENT OF ZEN</button></Link> */}
        </div>
        <h1>The DogHouse</h1>
        <Link to="/dogfavs"><button style={{ backgroundColor: 'pink', color: 'black', borderRadius: '50%', width: '80px', height: '40px', marginTop: '5px', fontSize: '12px', padding: '5px' }}>See FAVS</button></Link>
        <Link to="/dogs"><button style={{ backgroundColor: 'green', color: 'black', borderRadius: '50%', width: '80px', height: '60px', marginTop: '5px', fontSize: '12px', padding: '5px' }}>Back to DOGGIES</button></Link>
        {/* <button onClick={() => {handleDeleteAll()}} style={{ backgroundColor: 'red', color: 'black', borderRadius: '50%', width: '40px', height: '40px', marginTop: '5px' }}>X</button>DELETE ALL Doggies */}
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {pictures.map((picture, index) => (
                <div key={index} style={{ margin: '5px', textAlign: 'center' }}>
                <>
                <button
                    className="delete-button"
                    onClick={() => deletePicture(picture.id)}
                    style={{
                        backgroundColor: 'red',
                        color: 'black',
                        borderRadius: '50%',
                        width: '40px',
                        height: '50px',
                        marginBottom: '20px',
                        marginTop: '5px',
                        fontSize: '12px',
                        padding: '5px',
                        textAlign: 'center',
                        border: 'none',
                        cursor: 'pointer'
                    }}
                >
                    X {picture.id}
                </button>
                        <button
                            onClick={() => saveFav(picture)}
                            style={{
                                backgroundColor: 'pink',
                                color: 'black',
                                borderRadius: '50%',
                                width: '40px',
                                height: '80px',
                                marginBottom: '20px',
                                marginTop: '5px',
                                fontSize: '12px',
                                padding: '5px',
                                textAlign: 'center',
                                border: 'none',
                                cursor: 'pointer'
                            }}
                        >
                            Add to FAVS
                        </button>
                <img
                    src={picture.image}
                    alt={`${index}`}
                    style={{ width: '215px', height: '224px' }}
                />
                </>
            </div>
        ))}
    </div>
    </>
    )
}

export default DogHouse
