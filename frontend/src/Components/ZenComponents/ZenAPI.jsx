import { Link } from 'react-router-dom'
import {useState, useEffect } from 'react'


function ZenAPI () {

    const[text, setText] = useState('')
    const[author, setAuthor] = useState('')
    const[prettypics, setPrettypics] = useState([])
    const [quotes, setQuotes] = useState({})
    const [quoteClick, setQuoteClick] = useState(0)


    useEffect(()=>{handleGetQuote()},[])


    const fetchData = async () => {
    const url = 'https://quotes-inspirational-quotes-motivational-quotes.p.rapidapi.com/quote?token=ipworld.info';
    const options = {
        method: 'GET',
        headers: {
            'X-RapidAPI-Key': 'bf0635dbcemshde30513e0b8df0cp129e7djsn9f83ff38a9b3',
            'X-RapidAPI-Host': 'quotes-inspirational-quotes-motivational-quotes.p.rapidapi.com'
        }
    };
    try {
        const response = await fetch(url, options);
        const result = await response.json();
        setText(result['text'])
        setAuthor(result['author'])
        saveQuote(result)
        setQuotes(result)
        // console.log(result);
        // alert("api call")
    } catch (error) {
        console.error(error);
    }
    }

    const fetchQuotesData = async () => {
        const url = 'http://localhost:8000/pictures/listQuotes';
        try {
            const response = await fetch(url);
            const result = await response.json();
            setQuotes(result['quotes'])
            console.log('result', result)
            const randomQuotesNumber = Math.floor((Math.random() * quotes.length))
            console.log('randomQuotesNumber', randomQuotesNumber)
            setText(result['quotes'][randomQuotesNumber]['text'])
            setAuthor(result.quotes[randomQuotesNumber].author)
            console.log("from database result:",result);
        } catch (error) {
            console.error(error);
        }
        }
        console.log('quotes', quotes)

    const fetchPrettypicData = async () => {
        const url = "http://localhost:8000/pictures/listPrettypic"
        const response = await fetch(url);
        try {
            if (response.ok) {
            const data = await response.json()
            setPrettypics(data.prettypic)
            }
        } catch (error) {
            console.error("Error retrieving fetchData-nasaList", error)
        }
    }

    async function saveQuote(quote) {
        console.log("quote-------", quote)
        const url = "http://localhost:8000/pictures/createQuote"
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(quote)
        }
        const response = await fetch(url, options)
        console.log("Response", response)
        console.log("I just got made!")
        if (!response.ok) {
            throw new Error('Failed to submit tsi data')
        }
    }

    const handleGetQuote = () => {
        fetchData()
        handleQuoteClick()
        fetchPrettypicData()
    }

    const handleGetAllQuotes = () => {
        fetchQuotesData()
        fetchPrettypicData()
        handleQuoteClick()
    }

    const handleQuoteClick = () => {
        let plusOne = quoteClick + 1
        setQuoteClick(plusOne)
    }

    const randomPrettyPicNumber = Math.floor(Math.random() * prettypics.length);
    // const randomQuotesNumber = () => {
    //     return Math.floor(Math.random() * quotes.length);
    //   };
    const randomNumber = Math.floor(Math.random() * 10) + 1
    console.log('------------random number-----------------------', randomNumber)

console.log('text', text)
console.log('quoteClick', quoteClick)

    return (
        <>
            <div style={{ marginBottom: '6%' }}>
            </div>
            <div className="d-flex justify-content-center" style={{ marginBottom: '3%' }}>
                {quoteClick === 1 &&
                    <h1>Quote of the Day</h1>
                }
            </div>

            <div className="d-flex justify-content-center" style={{ marginBottom: '4%' }}>
                {text && (
                    <div style={{ position: 'relative', width: '40vw', height: '70vh' }}>
                        <div className="card" style={{ width: '100%', height: '100%', padding: '3%' }}>
                            <img src={prettypics[randomPrettyPicNumber].image} alt="beautiful" style={{ width: '100%', height: '100%', objectFit: 'cover' }}></img>
                        </div>
                        <div style={{ position: 'absolute', top: '35%', left: '50%', transform: 'translate(-50%, -50%)', textAlign: 'center', zIndex: 1 }}>
                            <h5 style={{ color: 'rgba(250, 255, 255, 0.9)' }}>{text}</h5>
                            <p style={{ color: 'rgba(25, 250, 250, 0.9)' }}>{author}</p>
                        </div>
                    </div>
                )}
            </div>

            <div>
            {quoteClick > 0 &&
                <>
                <div className="d-flex justify-content-center" style={{ marginBottom: '5%' }}>
                    <button onClick={handleGetAllQuotes}>Another Quote</button>
                </div>
                <div className="d-flex justify-content-center" style={{ marginBottom: '5%' }}>
                    <button onClick={handleGetQuote}>Quote of the Day</button>
                </div>
                </>
            }
            </div>
            <div style={{marginBottom:'15%'}}>
            </div>
        </>
    );
}

export default ZenAPI
