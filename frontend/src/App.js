import './App.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Home from './Components/HomeComponents/Home'
import PictureAPI from './Components/PrettyPicturesComponents/PictureAPI'
import DogAPI from './Components/DogsComponents/DogAPI'
import DogHouse from './Components/DogsComponents/DogHouse'
import ZenAPI from './Components/ZenComponents/ZenAPI'
import NasaAPI from './Components/NASAComponents/NasaAPI'
import NasaHouse from './Components/NASAComponents/NasaHouse'
import PictureHouse from './Components/PrettyPicturesComponents/PictureHouse'
import DogFavs from './Components/DogsComponents/DogFavs'
import Nav from './Components/Nav'
import Circles from './Components/CSSFactory/Circles'
import Maslow from './Components/CSSFactory/Maslow'
import TSITable from './Components/GraphComponents/TSITable'
import CSSFactory from './Components/CSSFactory/CSSFactory'
import TSIGraphs from './Components/GraphComponents/TSIGraphs'
import TSIGraphInteractive from './Components/GraphComponents/TSIGraphInteractive'
import CalendarPlain from './Components/CalendarComponents/CalendarPlain'
import CalendarReact from './Components/CalendarComponents/CalendarReact'
import TSIDynamicGraph from './Components/GraphComponents/TSIDynamicGraph'
import DoggieGame from './Components/DogsComponents/DoggieGame'
import Compare3Stocks from './Components/GraphComponents/Compare3Stocks'
import CandleStickGraph from './Components/GraphComponents/CandlestickGraph'
import StackedBarChart from './Components/GraphComponents/StackedBarChart'
import CandleStick2 from './Components/GraphComponents/CandleStick2'
import MultiViewPage from './Components/GraphComponents/MultiViewPage'
import NasaCarousel from './Components/NASAComponents/NasaCarousel'
import ViewAllNotes from './Components/NotesComponents/ViewAllNotes'
import QQQNotes from './Components/NotesComponents/QQQNotes'
import SPYNotes from './Components/NotesComponents/SPYNotes'
import IWMNotes from './Components/NotesComponents/IWMNotes'
import MSFTNotes from './Components/NotesComponents/MSFTNotes'
import Notes from './Components/NotesComponents/Notes'
import AllNotesPage from './Components/NotesComponents/AllNotesPage'

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/pictures" element={<PictureAPI />} />
        <Route path="/picturehouse" element={<PictureHouse />} />
        <Route path="/dogs" element={<DogAPI />} />
        <Route path="/doghouse" element={<DogHouse />} />
        <Route path="/dogfavs" element={<DogFavs />} />
        <Route path="/nasa" element={<NasaAPI />} />
        <Route path="/nasahouse" element={<NasaHouse />} />
        <Route path="/NasaCarousel" element={<NasaCarousel />} />
        <Route path="/zen" element={<ZenAPI />} />
        <Route path="/circles" element={<Circles />} />
        <Route path="/maslow" element={<Maslow />} />
        <Route path="/TSItable" element={<TSITable />} />
        <Route path="/TSIGraphs" element={<TSIGraphs />} />
        <Route path="/TSIGraphInteractive" element={<TSIGraphInteractive />} />
        <Route path="TSIDynamicGraph" element={<TSIDynamicGraph />} />
        <Route path="/Compare3Stocks" element={<Compare3Stocks />} />
        <Route path="/CandleStickGraph" element={<CandleStickGraph />} />
        <Route path="/StackedBarChart" element={<StackedBarChart />} />
        <Route path="/CandleStick2" element={<CandleStick2 />} />
        <Route path="/MultiViewPage" element={<MultiViewPage />} />
        <Route path="/CSSFactory" element={<CSSFactory />} />
        <Route path="/calendar" element={<CalendarPlain />} />
        <Route path="/calendarReact" element={<CalendarReact />} />
        <Route path="/DoggieGame" element={<DoggieGame />} />
        <Route path="/ViewAllNotes" element={<ViewAllNotes />} />
        <Route path="/QQQNotes" element={<QQQNotes />} />
        <Route path="/SPYNotes" element={<SPYNotes />} />
        <Route path="/IWMNotes" element={<IWMNotes />} />
        <Route path="/MSFTNotes" element={<MSFTNotes />} />
        <Route path="/Notes" element={<Notes />} />
        <Route path="/AllNotesPage" element={<AllNotesPage />} />
      </Routes>
      </div>
    </BrowserRouter>
  )
}

export default App;
