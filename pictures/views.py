from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from .models import Picture, Nasa, PrettyPics, Fav, Quotes
from django.http import JsonResponse
from .common.model_encoder import ModelEncoder
from django.core import serializers


# Create your views here.

# -------------------------------ModelEncoders--------------------------------------

class PictureEncoder(ModelEncoder):
    model = Picture
    properties = [
        "id",
        "image"
    ]

class FavEncoder(ModelEncoder):
    model = Fav
    properties = [
        "id",
        "image"
    ]

class NasaEncoder(ModelEncoder):
    model = Nasa
    properties = [
        "id",
        "copyright",
        "date",
        "explanation",
        "hdurl",
        "media_type",
        "service_version",
        "title",
        "url"
    ]

class PrettyPicEncoder(ModelEncoder):
    model = PrettyPics
    properties = [
        "id",
        "image",
        "by",
        "source",
        "diffrentSizes"
    ]

class QuoteEncoder(ModelEncoder):
    model = Quotes
    properties = [
        "author",
        "text"
    ]


    # ------------------------------DOGGIES---------------------------------------

@require_http_methods(["POST"])
def create_picture(request):
    if request.method == "POST":
        content = json.loads(request.body)
        picture = Picture(image=content)
        picture.save()
        return JsonResponse(
            picture,
            encoder=PictureEncoder,
            safe=False
        )
    else:
        print("Error in the POST-picture")

@require_http_methods(["GET"])
def get_all_pictures(request):
    if request.method == "GET":
        pictures = Picture.objects.all()
        return JsonResponse(
            {"pictures": pictures},
            encoder=PictureEncoder
        )

@require_http_methods(["DELETE"])
def delete_picture(request, pk):
    if request.method == "DELETE":
        picture = Picture.objects.get(id=pk)
        picture.delete()
        return JsonResponse(
            picture,
            encoder=PictureEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def delete_all_pictures(request):
    if request.method == "DELETE":
        picture = Picture.objects.all()
        picture.delete()
        return JsonResponse(
            picture,
            encoder=PictureEncoder,
            safe=False
        )


    # -----------------------------------DOGGIE FAVS---------------------------------------


@require_http_methods(["POST"])
def create_fav(request):
    if request.method == "POST":
        content = json.loads(request.body)
        fav = Fav(image=content)
        fav.save()
        return JsonResponse(
            fav,
            encoder=FavEncoder,
            safe=False
        )
    else:
        print("Error in the POST-Nasa")


@require_http_methods(["GET"])
def list_fav(request):
    if request.method == "GET":
        fav = Fav.objects.all()
        return JsonResponse(
            {"fav": fav},
            encoder=FavEncoder
        )


@require_http_methods(["DELETE"])
def delete_fav(request, pk):
    if request.method == "DELETE":
        fav = Fav.objects.get(id=pk)
        fav.delete()
        return JsonResponse(
            fav,
            encoder=FavEncoder,
            safe=False
        )



    # ---------------------------------NASA-----------------------------------------


# def get():
#     nasa_queryset = Nasa.objects.all()
#     nasa_data = serializers.serialize('json', nasa_queryset)
#     return json.loads(nasa_data)

@require_http_methods(["POST"])
def create_Nasa(request):
    try:
        # nasa_images = get()
        # print("-------------------nasa_images--------------------------", nasa_images)
        content = json.loads(request.body)
        if Nasa.objects.filter(explanation = content['explanation']):
            return JsonResponse(
                {"message": "This one already exists"}
            )
        # for image in nasa_images['fields']:
            # print("------------image---------------", image)
            # if image['explanation'] == content['explanation']:
                # print("This one already exists!!!!!!!!!!!!!!!!!!!!!!!!")
                # return JsonResponse({"message": "This one already exists"})
        # if Nasa.objects.exists()
        nasa = Nasa.objects.create(**content)
        print("I just got created!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        return JsonResponse({'message:': 'created'})
    except:
        return JsonResponse({'status': 'error'})



@require_http_methods(["GET"])
def list_Nasa(request):
    if request.method == "GET":
        nasa = Nasa.objects.all()
        return JsonResponse(
            {"nasa": nasa},
            encoder=NasaEncoder
        )


@require_http_methods(["DELETE"])
def delete_Nasa(request, pk):
    if request.method == "DELETE":
        nasa = Nasa.objects.get(id=pk)
        nasa.delete()
        return JsonResponse(
            nasa,
            encoder=NasaEncoder,
            safe=False
        )



# -------------------------------------PRETTY PICS--------------------------------


@require_http_methods(["POST"])
def create_prettypic(request):
    try:
        content = json.loads(request.body)
        if PrettyPics.objects.filter(image=content['image']).exists():
            return JsonResponse({"message": "This one already exists"})
        prettypic = PrettyPics.objects.create(**content)
        return JsonResponse({"message": "Object created successfully"})
    except Exception as e:
        print("Error:", str(e))
        return JsonResponse({'status': 'error'})


@require_http_methods(["GET"])
def list_prettypic(request):
    if request.method == "GET":
        prettypic = PrettyPics.objects.all()
        return JsonResponse(
            {"prettypic": prettypic},
            encoder=PrettyPicEncoder
        )

@require_http_methods(["DELETE"])
def delete_prettypic(request, pk):
    if request.method == "DELETE":
        prettypic = PrettyPics.objects.get(id=pk)
        prettypic.delete()
        return JsonResponse(
            prettypic,
            encoder=PrettyPicEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def delete_all_prettypics(request):
    if request.method == "DELETE":
        prettypic = PrettyPics.objects.all()
        prettypic.delete()
        return JsonResponse(
            prettypic,
            encoder=PrettyPicEncoder,
            safe=False
        )


# ---------------------------------------QUOTES--------------------------------------

@require_http_methods(["POST"])
def create_quote(request):
    try:
        content = json.loads(request.body)
        if Quotes.objects.filter(text=content['text']).exists():
            return JsonResponse({"message": "This one already exists"})
        quote = Quotes.objects.create(**content)
        return JsonResponse({"message": "Object created successfully"})
    except Exception as e:
        print("Error:", str(e))
        return JsonResponse({'status': 'error'})


@require_http_methods(["GET"])
def list_quotes(request):
    if request.method == "GET":
        quotes = Quotes.objects.all()
        return JsonResponse(
            {"quotes": quotes},
            encoder=QuoteEncoder
        )

@require_http_methods(["DELETE"])
def delete_quote(request, pk):
    if request.method == "DELETE":
        quote = Quotes.objects.get(id=pk)
        quote.delete()
        return JsonResponse(
            quote,
            encoder=QuoteEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def delete_all_quotes(request):
    if request.method == "DELETE":
        quotes = Quotes.objects.all()
        quotes.delete()
        return JsonResponse(
            quotes,
            encoder=QuoteEncoder,
            safe=False
        )
