from django.urls import path
from .views import (
    create_picture,
    get_all_pictures,
    delete_picture,
    delete_all_pictures,
    create_Nasa,
    list_Nasa,
    delete_Nasa,
    create_prettypic,
    list_prettypic,
    delete_prettypic,
    delete_all_prettypics,
    create_fav,
    list_fav,
    delete_fav,
    create_quote,
    list_quotes,
    delete_quote,
    delete_all_quotes
    )


urlpatterns = [
    path("createPicture", create_picture, name="create_picture"),
    path("listPictures/", get_all_pictures, name="get_all_pictures"),
    path("deletePicture/<int:pk>", delete_picture, name="delete_picture"),
    path("deleteAllPictures", delete_all_pictures, name="delete_all_pictures"),
    path("createNasa", create_Nasa, name="create_Nasa"),
    path("listNasa", list_Nasa, name="list_Nasa"),
    path("deleteNasa/<int:pk>", delete_Nasa, name="delete_Nasa"),
    path("createPrettypic", create_prettypic, name="create_prettypic"),
    path("listPrettypic", list_prettypic, name="list_prettypic"),
    path("deletePrettypic/<int:pk>", delete_prettypic, name="delete_prettypic"),
    path("deleteAllPrettypics", delete_all_prettypics, name="delete_all_prettypics"),
    path("createFav", create_fav, name="create_fav"),
    path("listFav", list_fav, name="list_fav"),
    path("deleteFav/<int:pk>", delete_fav, name="delete_fav"),
    path("createQuote", create_quote, name="create_quote"),
    path("listQuotes", list_quotes, name="list_quotes"),
    path("deleteQuote/<int:pk>", delete_quote, name="delete_quote"),
    path("deleteAllQuotes", delete_all_quotes, name="delete_all_quotes")
]
