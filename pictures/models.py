from django.db import models

# Create your models here.

class Picture(models.Model):
    image = models.TextField()

class Fav(models.Model):
    image = models.TextField()

class Nasa(models.Model):
    copyright = models.CharField(max_length=200, null=True)
    date = models.CharField(max_length=200, null=True)
    explanation = models.TextField(null=True)
    hdurl = models.CharField(max_length=200, null=True)
    media_type = models.CharField(max_length=200, null=True)
    service_version = models.CharField(max_length=200, null=True)
    title = models.CharField(max_length=200, null=True)
    url = models.CharField(max_length=200, null=True)

class PrettyPics(models.Model):
    image = models.TextField(null=True)
    by = models.CharField(max_length=200, null=True)
    download = models.TextField(null=True)
    source = models.CharField(max_length=200, null=True)
    diffrentSizes = models.TextField(null=True)
    pic_id = models.TextField(null=True)

    def set_diffrentSizes(self, diffrentSizes):
        self.diffrentSizes = ','.join(diffrentSizes)

    def get_diffrentSizes(self):
        return self.diffrentSizes.split(',')


class Quotes(models.Model):
    author = models.CharField(max_length=200)
    text = models.TextField()
